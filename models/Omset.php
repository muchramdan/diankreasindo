<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "omset".
 *
 * @property int $id_omset
 * @property string $tanggal
 * @property string $no_op
 * @property string $no_kw
 * @property string $customer
 * @property string $pembelian
 * @property string $saldo_awal
 * @property string $dp_cash
 * @property string $dp_bank
 * @property string $pelunasan
 * @property string $pelunasan_bank
 * @property string $bg_mundur
 * @property string $potongan
 * @property string $saldo_akhir
 * @property string $marketing
 */
class Omset extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'omset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tanggal', 'no_op', 'saldo_awal','jenis_pengerjaan','marketing','customer'], 'required'],
            [['id_omset'], 'integer'],
            [['jenis_pengerjaan','tanggal', 'no_op', 'no_kw','customer', 'pembelian', 'saldo_awal', 'dp_cash', 'dp_bank', 'pelunasan', 'pelunasan_bank', 'bg_mundur', 'potongan', 'saldo_akhir'], 'string', 'max' => 50],
            [['id_omset'], 'unique'],
            [['no_op'], 'autonumber', 'format'=>'SA.'.date('Y-m-d').'.?'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_omset' => 'Id Omset',
            'jenis_pengerjaan' => 'Jenis Pengerjaan',
            'tanggal' => 'Tanggal',
            'no_op' => 'No. Op',
            'no_kw' => 'No. Kw',
            'customer' => 'Customer',
            'pembelian' => 'Pembelian',
            'saldo_awal' => 'Saldo Awal',
            'dp_cash' => 'Dp Cash',
            'dp_bank' => 'Dp Bank',
            'pelunasan' => 'Pelunasan',
            'pelunasan_bank' => 'Pelunasan Bank',
            'bg_mundur' => 'Bg Mundur',
            'potongan' => 'Potongan',
            'saldo_akhir' => 'Saldo Akhir',
            'marketing' => 'Marketing'
        ];
    }
}
