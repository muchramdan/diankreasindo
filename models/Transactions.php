<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property string $trans_code
 * @property string $trans_date
 * @property integer $type_id
 * @property string $remarks
 *
 * @property TransactionDetails[] $transactionDetails
 * @property TransactionTypes $type
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_code', 'type_id','customer','alamat','no_hp','jenis_produk','pengerjaan','color'], 'required'],
            [['trans_date','nama_bahan','jumlah','harga','total'], 'safe'],
            [['type_id'], 'integer'],
            [['trans_code', 'remarks'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trans_code' => 'Transaction Code',
            'trans_date' => 'Transaction Date',
            'type_id' => 'Transaction Type',
            'remarks' => 'Remarks',
            'customer'=>'Customer',
            'alamat'=> 'Alamat',
            'no_hp'=>'No HP/ Telp',
            'jenis_produk'=> 'Jenis Produk',
            'pengerjaan'=>'Pengerjaan',
            'color' => 'Warna Rangka/Atap',
            'nama_bahan' => 'Nama Bahan',
            'jumlah' => 'Jumlah',
            'harga' => 'Harga',
            'total' => 'Total'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionDetails()
    {
        return $this->hasMany(TransactionDetails::className(), ['trans_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransactionTypes::className(), ['id' => 'type_id']);
    }
}
