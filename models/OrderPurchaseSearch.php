<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrderPurchase;

/**
 * OrderPurchaseSearch represents the model behind the search form of `app\models\OrderPurchase`.
 */
class OrderPurchaseSearch extends OrderPurchase
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_order_purchase'], 'integer'],
            [['op_no_order', 'op_customer_nama', 'op_customer_alamat', 'op_customer_lokasi_ukur', 'op_customer_telp', 'op_tanggal_op', 'op_customer_daerah', 'op_customer_hp', 'op_customer_janji_pasang', 'op_data_ukur_t', 'op_data_ukur_l', 'op_data_ukur_set', 'op_data_ukur_arah', 'op_data_gambar', 'op_cp_produk_set', 'op_cp_produk_luas', 'op_cp_produk_harga', 'op_cp_produk_jumlah', 'op_cp_warna_set', 'op_cp_warna_luas', 'op_cp_warna_harga', 'op_cp_warna_jumlah', 'op_cp_dudukan_set', 'op_cp_dudukan_luas', 'op_cp_dudukan_harga', 'op_cp_dudukan_jumlah', 'op_jumlah', 'op_uang_muka', 'op_sisa'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderPurchase::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_order_purchase' => $this->id_order_purchase,
        ]);

        $query->andFilterWhere(['like', 'op_no_order', $this->op_no_order])
            ->andFilterWhere(['like', 'op_customer_nama', $this->op_customer_nama])
            ->andFilterWhere(['like', 'op_customer_alamat', $this->op_customer_alamat])
            ->andFilterWhere(['like', 'op_customer_lokasi_ukur', $this->op_customer_lokasi_ukur])
            ->andFilterWhere(['like', 'op_customer_telp', $this->op_customer_telp])
            ->andFilterWhere(['like', 'op_tanggal_op', $this->op_tanggal_op])
            ->andFilterWhere(['like', 'op_customer_daerah', $this->op_customer_daerah])
            ->andFilterWhere(['like', 'op_customer_hp', $this->op_customer_hp])
            ->andFilterWhere(['like', 'op_customer_janji_pasang', $this->op_customer_janji_pasang])
            ->andFilterWhere(['like', 'op_data_ukur_t', $this->op_data_ukur_t])
            ->andFilterWhere(['like', 'op_data_ukur_l', $this->op_data_ukur_l])
            ->andFilterWhere(['like', 'op_data_ukur_set', $this->op_data_ukur_set])
            ->andFilterWhere(['like', 'op_data_ukur_arah', $this->op_data_ukur_arah])
            ->andFilterWhere(['like', 'op_data_gambar', $this->op_data_gambar])
            ->andFilterWhere(['like', 'op_cp_produk_set', $this->op_cp_produk_set])
            ->andFilterWhere(['like', 'op_cp_produk_luas', $this->op_cp_produk_luas])
            ->andFilterWhere(['like', 'op_cp_produk_harga', $this->op_cp_produk_harga])
            ->andFilterWhere(['like', 'op_cp_produk_jumlah', $this->op_cp_produk_jumlah])
            ->andFilterWhere(['like', 'op_cp_warna_set', $this->op_cp_warna_set])
            ->andFilterWhere(['like', 'op_cp_warna_luas', $this->op_cp_warna_luas])
            ->andFilterWhere(['like', 'op_cp_warna_harga', $this->op_cp_warna_harga])
            ->andFilterWhere(['like', 'op_cp_warna_jumlah', $this->op_cp_warna_jumlah])
            ->andFilterWhere(['like', 'op_cp_dudukan_set', $this->op_cp_dudukan_set])
            ->andFilterWhere(['like', 'op_cp_dudukan_luas', $this->op_cp_dudukan_luas])
            ->andFilterWhere(['like', 'op_cp_dudukan_harga', $this->op_cp_dudukan_harga])
            ->andFilterWhere(['like', 'op_cp_dudukan_jumlah', $this->op_cp_dudukan_jumlah])
            ->andFilterWhere(['like', 'op_jumlah', $this->op_jumlah])
            ->andFilterWhere(['like', 'op_uang_muka', $this->op_uang_muka])
            ->andFilterWhere(['like', 'op_sisa', $this->op_sisa]);

        return $dataProvider;
    }
}
