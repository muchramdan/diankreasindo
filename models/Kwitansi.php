<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kwitansi".
 *
 * @property int $id_kwitansi
 * @property int $id_order_purchase
 */
class Kwitansi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kwitansi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_order_purchase'], 'required'],
            [['id_order_purchase','no_kw'], 'integer'],
            [['id_order_purchase','no_kw'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kwitansi' => 'Id Kwitansi',
            'id_order_purchase' => 'Id Order Purchase',
            'no_kw' => ' No Kw'
        ];
    }
}
