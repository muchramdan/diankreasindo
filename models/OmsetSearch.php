<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Omset;

/**
 * OmsetSearch represents the model behind the search form of `app\models\Omset`.
 */
class OmsetSearch extends Omset
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_omset'], 'integer'],
            [['tanggal', 'no_op', 'no_kw', 'pembelian', 'saldo_awal', 'dp_cash', 'dp_bank', 'pelunasan', 'pelunasan_bank', 'bg_mundur', 'potongan', 'saldo_akhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Omset::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_omset' => $this->id_omset,
        ]);

        $query->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'no_op', $this->no_op])
            ->andFilterWhere(['like', 'no_kw', $this->no_kw])
            ->andFilterWhere(['like', 'pembelian', $this->pembelian])
            ->andFilterWhere(['like', 'saldo_awal', $this->saldo_awal])
            ->andFilterWhere(['like', 'dp_cash', $this->dp_cash])
            ->andFilterWhere(['like', 'dp_bank', $this->dp_bank])
            ->andFilterWhere(['like', 'pelunasan', $this->pelunasan])
            ->andFilterWhere(['like', 'pelunasan_bank', $this->pelunasan_bank])
            ->andFilterWhere(['like', 'bg_mundur', $this->bg_mundur])
            ->andFilterWhere(['like', 'potongan', $this->potongan])
            ->andFilterWhere(['like', 'saldo_akhir', $this->saldo_akhir]);

        return $dataProvider;
    }
}
