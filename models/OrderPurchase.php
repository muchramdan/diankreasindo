<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_purchase".
 *
 * @property int $id_order_purchase
 * @property string $op_no_order
 * @property string $op_customer_nama
 * @property string $op_customer_alamat
 * @property string $op_customer_lokasi_ukur
 * @property string $op_customer_telp
 * @property string $op_tanggal_op
 * @property string $op_customer_daerah
 * @property string $op_customer_hp
 * @property string $op_customer_janji_pasang
 * @property string $op_data_ukur_t
 * @property string $op_data_ukur_l
 * @property string $op_data_ukur_set
 * @property string $op_data_ukur_arah
 * @property string $op_data_gambar
 * @property string $op_cp_produk_set
 * @property string $op_cp_produk_luas
 * @property string $op_cp_produk_harga
 * @property string $op_cp_produk_jumlah
 * @property string $op_cp_warna_set
 * @property string $op_cp_warna_luas
 * @property string $op_cp_warna_harga
 * @property string $op_cp_warna_jumlah
 * @property string $op_cp_dudukan_set
 * @property string $op_cp_dudukan_luas
 * @property string $op_cp_dudukan_harga
 * @property string $op_cp_dudukan_jumlah
 * @property string $op_jumlah
 * @property string $op_uang_muka
 * @property string $op_sisa
 */
class OrderPurchase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_purchase';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['op_no_order','op_customer_nama', 'op_customer_alamat', 'op_customer_lokasi_ukur', 'op_customer_telp', 'op_tanggal_op', 'op_customer_daerah', 'op_customer_hp'], 'required'],
            [['op_no_order','op_customer_nama', 'op_customer_alamat', 'op_customer_lokasi_ukur', 'op_customer_telp', 'op_tanggal_op', 'op_customer_daerah', 'op_customer_hp', 'op_customer_janji_pasang', 'op_data_ukur_t', 'op_data_ukur_l', 'op_data_ukur_set', 'op_data_ukur_arah', 'op_data_gambar', 'op_cp_produk_set', 'op_cp_produk_luas', 'op_cp_produk_harga', 'op_cp_produk_jumlah', 'op_cp_warna_set', 'op_cp_warna_luas', 'op_cp_warna_harga', 'op_cp_warna_jumlah', 'op_cp_dudukan_set', 'op_cp_dudukan_luas', 'op_cp_dudukan_harga', 'op_cp_dudukan_jumlah', 'op_jumlah', 'op_uang_muka', 'op_sisa'], 'string', 'max' => 100],
            [['op_no_order'], 'autonumber', 'format'=>function(){
                return $this->type . '-' . date('Ymd') . '-?';
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_order_purchase' => 'Id Order Purchase',
            'op_no_order' => 'Order',
            'op_customer_nama' => 'Customer Nama',
            'op_customer_alamat' => 'Customer Alamat',
            'op_customer_lokasi_ukur' => 'Customer Lokasi Ukur',
            'op_customer_telp' => 'Customer Telp',
            'op_tanggal_op' => 'Tanggal Op',
            'op_customer_daerah' => 'Customer Daerah',
            'op_customer_hp' => 'Customer Hp',
            'op_customer_janji_pasang' => 'Customer Janji Pasang',
            'op_data_ukur_t' => 'Data Ukur T',
            'op_data_ukur_l' => 'Data Ukur L',
            'op_data_ukur_set' => 'Data Ukur Set',
            'op_data_ukur_arah' => 'Data Ukur Arah',
            'op_data_gambar' => 'Data Gambar',
            'op_cp_produk_set' => 'Produk Set',
            'op_cp_produk_luas' => 'Produk Luas',
            'op_cp_produk_harga' => 'Produk Harga',
            'op_cp_produk_jumlah' => 'Produk Jumlah',
            'op_cp_warna_set' => 'Warna Set',
            'op_cp_warna_luas' => 'Warna Luas',
            'op_cp_warna_harga' => 'Warna Harga',
            'op_cp_warna_jumlah' => 'Warna Jumlah',
            'op_cp_dudukan_set' => 'Dudukan Set',
            'op_cp_dudukan_luas' => 'Dudukan Luas',
            'op_cp_dudukan_harga' => 'Dudukan Harga',
            'op_cp_dudukan_jumlah' => 'Dudukan Jumlah',
            'op_jumlah' => 'Jumlah',
            'op_uang_muka' => 'Uang Muka',
            'op_sisa' => 'Sisa',
        ];
    }
    public static function generateKode_Urut() {

        $_d = date("ymd");
        $_left = $_d;
        $_first = "0001";
        $_len = strlen($_left);
        $no = $_left . $_first;
        $last_po = OrderPurchase::find()
        ->where(["left(id_order_purchase, " . $_len . ")" => $_left])
        ->orderBy(["id_order_purchase" => SORT_DESC])
        ->one();
        if ($last_po != null) {
            $_no = substr($last_po->id_order_purchase, $_len);
            $_no++;
            $_no = substr("0000", strlen($_no)) . $_no;
            $no = $_left . $_no;
        }
        return $no;

    }


}
