<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kwitansi;

/**
 * KwitansiSearch represents the model behind the search form of `app\models\Kwitansi`.
 */
class KwitansiSearch extends Kwitansi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kwitansi', 'id_order_purchase'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kwitansi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kwitansi' => $this->id_kwitansi,
            'id_order_purchase' => $this->id_order_purchase,
        ]);

        return $dataProvider;
    }
}
