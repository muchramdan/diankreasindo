-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2020 at 10:46 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assetmgt_full`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_code`
--

CREATE TABLE `account_code` (
  `id_account_code` int(11) NOT NULL,
  `id_account_code_parent` int(11) NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `account_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_code`
--

INSERT INTO `account_code` (`id_account_code`, `id_account_code_parent`, `account_code`, `account_name`) VALUES
(1, 0, '1', 'AKTIVA'),
(11, 1, '1.1', 'AKTIVA TETAP'),
(12, 1, '1.2', 'AKTIVA BERGERAK');

-- --------------------------------------------------------

--
-- Table structure for table `app_field_config`
--

CREATE TABLE `app_field_config` (
  `id_app_field_config` int(11) NOT NULL,
  `classname` varchar(250) NOT NULL,
  `varian_group` varchar(150) NOT NULL,
  `fieldname` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `no_order` int(11) NOT NULL,
  `is_visible` int(1) NOT NULL,
  `is_required` int(1) NOT NULL,
  `is_unique` int(11) DEFAULT 0,
  `is_safe` int(11) DEFAULT 0,
  `type_field` int(11) DEFAULT 0,
  `max_field` int(4) NOT NULL,
  `default_value` varchar(250) DEFAULT NULL,
  `pattern` varchar(250) DEFAULT NULL,
  `image_extensions` varchar(250) NOT NULL,
  `image_max_size` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_field_config`
--

INSERT INTO `app_field_config` (`id_app_field_config`, `classname`, `varian_group`, `fieldname`, `label`, `no_order`, `is_visible`, `is_required`, `is_unique`, `is_safe`, `type_field`, `max_field`, `default_value`, `pattern`, `image_extensions`, `image_max_size`) VALUES
(1, 'asset_master', '', 'asset_name', 'Nama Barang', 2, 1, 1, 0, 0, 1, 250, NULL, NULL, '', ''),
(2, 'asset_master', '', 'asset_code', 'Kode Barang', 1, 1, 1, 0, 0, 1, 40, NULL, NULL, '', ''),
(3, 'asset_master', '', 'id_type_asset1', 'Jenis Aset', 3, 1, 1, 0, 0, 2, 12, NULL, NULL, '', ''),
(6, 'asset_master', '', 'id_type_asset2', 'Type Asset 2', 0, 0, 0, 0, 0, 2, 12, NULL, NULL, '', ''),
(7, 'asset_master', '', 'attribute1', 'TEST', 0, 0, 0, 0, 0, 2, 12, NULL, NULL, '', ''),
(20, 'type_asset_item1', '', 'type_asset_item', 'Embuh', 0, 1, 1, 0, 0, 1, 0, NULL, NULL, '', ''),
(21, 'type_asset_item1', '', 'description', 'description', 0, 1, 0, 0, 0, 1, 0, NULL, NULL, '', ''),
(22, 'type_asset_item1', '', 'is_active', 'is active', 0, 1, 1, 0, 0, 2, 0, NULL, NULL, '', ''),
(23, 'kelurahan', '', 'id_kecamatan', 'Kecamatan', 0, 1, 1, 0, 0, 2, 0, NULL, NULL, '', ''),
(24, 'kelurahan', '', 'nama_kelurahan', 'nama kelurahan', 0, 1, 1, 0, 0, 1, 0, NULL, NULL, '', ''),
(25, 'kelurahan', '', 'kodepos', 'kodepos', 0, 0, 0, 0, 0, 2, 0, NULL, NULL, '', ''),
(26, 'asset_item_location', '', 'id_asset_master', 'id asset master', 1, 1, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(27, 'asset_item_location', '', 'latitude', 'latitude', 2, 1, 1, 0, 0, 1, 60, NULL, NULL, '', ''),
(28, 'asset_item_location', '', 'longitude', 'longitude', 3, 1, 1, 0, 0, 1, 60, NULL, NULL, '', ''),
(29, 'asset_item_location', '', 'address', 'address', 4, 1, 1, 0, 0, 1, 250, NULL, NULL, '', ''),
(30, 'asset_item_location', '', 'desa', 'desa', 5, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(31, 'asset_item_location', '', 'kecamatan', 'kecamatan', 6, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(32, 'asset_item_location', '', 'kabupaten', 'kabupaten', 7, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(33, 'asset_item_location', '', 'provinsi', 'provinsi', 8, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(34, 'asset_item_location', '', 'kodepos', 'kodepos', 9, 1, 1, 0, 0, 1, 250, NULL, NULL, '', ''),
(35, 'asset_item_location', '', 'id_kabupaten', 'id kabupaten', 10, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(36, 'asset_item_location', '', 'id_propinsi', 'id propinsi', 11, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(37, 'asset_item_location', '', 'id_kecamatan', 'id kecamatan', 12, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(38, 'asset_item_location', '', 'id_kelurahan', 'id kelurahan', 13, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(39, 'asset_item_location', '', 'batas_utara', 'batas utara', 14, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(40, 'asset_item_location', '', 'batas_selatan', 'batas selatan', 15, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(41, 'asset_item_location', '', 'batas_timur', 'batas timur', 16, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(42, 'asset_item_location', '', 'batas_barat', 'batas barat', 17, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(43, 'asset_item_location', '', 'luas', 'luas', 18, 1, 1, 0, 0, 4, 18, NULL, NULL, '', ''),
(44, 'asset_item_location', '', 'keterangan1', 'keterangan1', 19, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(45, 'asset_item_location', '', 'keterangan2', 'keterangan2', 20, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(46, 'asset_item_location', '', 'keterangan3', 'keterangan3', 21, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(47, 'asset_item', '', 'id_asset_master', 'id asset master', 7, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(48, 'asset_item', '', 'number1', 'Nomor Registrasi', 2, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(49, 'asset_item', '', 'number2', 'number2', 3, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(50, 'asset_item', '', 'number3', 'number3', 4, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(51, 'asset_item', '', 'picture1', 'Gambar', 5, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(52, 'asset_item', '', 'picture2', 'picture2', 6, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(53, 'asset_item', '', 'picture3', 'picture3', 7, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(54, 'asset_item', '', 'picture4', 'picture4', 8, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(55, 'asset_item', '', 'picture5', 'picture5', 9, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(56, 'asset_item', '', 'caption_picture1', 'caption picture1', 10, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(57, 'asset_item', '', 'caption_picture2', 'caption picture2', 11, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(58, 'asset_item', '', 'caption_picture3', 'caption picture3', 12, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(59, 'asset_item', '', 'caption_picture4', 'caption picture4', 13, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(60, 'asset_item', '', 'caption_picture5', 'caption picture5', 14, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(61, 'asset_item', '', 'id_asset_received', 'id asset received', 15, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(62, 'asset_item', '', 'id_asset_item_location', 'id asset item location', 16, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(63, 'asset_item', '', 'id_type_asset_item1', 'id type asset item1', 17, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(64, 'asset_item', '', 'id_type_asset_item2', 'id type asset item2', 18, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(65, 'asset_item', '', 'id_type_asset_item3', 'id type asset item3', 19, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(66, 'asset_item', '', 'id_type_asset_item4', 'id type asset item4', 20, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(67, 'asset_item', '', 'id_type_asset_item5', 'id type asset item5', 21, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(68, 'asset_item', '', 'file1', 'file1', 22, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(69, 'asset_item', '', 'file2', 'file2', 23, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(70, 'asset_item', '', 'file3', 'file3', 24, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(71, 'asset_item', '', 'label1', 'SPAN TOWER', 1, 1, 1, 0, 0, 1, 250, NULL, NULL, '', ''),
(72, 'asset_item', '', 'label2', 'label2', 26, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(73, 'asset_item', '', 'label3', 'label3', 27, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(74, 'asset_item', '', 'label4', 'label4', 28, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(75, 'asset_item', '', 'label5', 'Keterangan', 29, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(76, 'asset_item', 'id_asset_master#10', 'id_asset_master', 'id pertama', 1, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(77, 'asset_item', 'id_asset_master#10', 'id_asset_item_parent', 'id asset item parent', 2, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(78, 'asset_item', 'id_asset_master#10', 'number1', 'number1', 3, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(79, 'asset_item', 'id_asset_master#10', 'number2', 'number2', 4, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(80, 'asset_item', 'id_asset_master#10', 'number3', 'number3', 5, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(81, 'asset_item', 'id_asset_master#10', 'picture1', 'picture1', 6, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(82, 'asset_item', 'id_asset_master#10', 'picture2', 'picture2', 7, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(83, 'asset_item', 'id_asset_master#10', 'picture3', 'picture3', 8, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(84, 'asset_item', 'id_asset_master#10', 'picture4', 'picture4', 9, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(85, 'asset_item', 'id_asset_master#10', 'picture5', 'picture5', 10, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(86, 'asset_item', 'id_asset_master#10', 'caption_picture1', 'caption picture1', 11, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(87, 'asset_item', 'id_asset_master#10', 'caption_picture2', 'caption picture2', 12, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(88, 'asset_item', 'id_asset_master#10', 'caption_picture3', 'caption picture3', 13, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(89, 'asset_item', 'id_asset_master#10', 'caption_picture4', 'caption picture4', 14, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(90, 'asset_item', 'id_asset_master#10', 'caption_picture5', 'caption picture5', 15, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(91, 'asset_item', 'id_asset_master#10', 'id_asset_received', 'id asset received', 16, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(92, 'asset_item', 'id_asset_master#10', 'id_asset_item_location', 'Jenis Tanaman', 17, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(93, 'asset_item', 'id_asset_master#10', 'id_type_asset_item1', 'Jenis Tanaman', 18, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(94, 'asset_item', 'id_asset_master#10', 'id_type_asset_item2', 'id type asset item2', 19, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(95, 'asset_item', 'id_asset_master#10', 'id_type_asset_item3', 'Jenis Tanaman', 20, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(96, 'asset_item', 'id_asset_master#10', 'id_type_asset_item4', 'id type asset item4', 21, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(97, 'asset_item', 'id_asset_master#10', 'id_type_asset_item5', 'id type asset item5', 22, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(98, 'asset_item', 'id_asset_master#10', 'file1', 'file1', 23, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(99, 'asset_item', 'id_asset_master#10', 'file2', 'file2', 24, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(100, 'asset_item', 'id_asset_master#10', 'file3', 'file3', 25, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(101, 'asset_item', 'id_asset_master#10', 'label1', 'BESAR', 26, 1, 0, 0, 0, 1, 8, NULL, NULL, '', ''),
(102, 'asset_item', 'id_asset_master#10', 'label2', 'SEDANG', 27, 1, 0, 0, 0, 1, 8, NULL, NULL, '', ''),
(103, 'asset_item', 'id_asset_master#10', 'label3', 'KECIL', 28, 1, 0, 0, 0, 1, 8, NULL, NULL, '', ''),
(104, 'asset_item', 'id_asset_master#10', 'label4', 'JUMLAH', 29, 1, 0, 0, 0, 1, 8, NULL, NULL, '', ''),
(105, 'asset_item', 'id_asset_master#10', 'label5', 'label5', 30, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(106, 'asset_item', 'id_asset_master#11', 'id_asset_master', 'id asset master', 1, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(107, 'asset_item', 'id_asset_master#11', 'id_asset_item_parent', 'id asset item parent', 2, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(108, 'asset_item', 'id_asset_master#11', 'number1', 'number1', 3, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(109, 'asset_item', 'id_asset_master#11', 'number2', 'number2', 4, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(110, 'asset_item', 'id_asset_master#11', 'number3', 'number3', 5, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(111, 'asset_item', 'id_asset_master#11', 'picture1', 'picture1', 6, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(112, 'asset_item', 'id_asset_master#11', 'picture2', 'picture2', 7, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(113, 'asset_item', 'id_asset_master#11', 'picture3', 'picture3', 8, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(114, 'asset_item', 'id_asset_master#11', 'picture4', 'picture4', 9, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(115, 'asset_item', 'id_asset_master#11', 'picture5', 'picture5', 10, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(116, 'asset_item', 'id_asset_master#11', 'caption_picture1', 'caption picture1', 11, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(117, 'asset_item', 'id_asset_master#11', 'caption_picture2', 'caption picture2', 12, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(118, 'asset_item', 'id_asset_master#11', 'caption_picture3', 'caption picture3', 13, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(119, 'asset_item', 'id_asset_master#11', 'caption_picture4', 'caption picture4', 14, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(120, 'asset_item', 'id_asset_master#11', 'caption_picture5', 'caption picture5', 15, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(121, 'asset_item', 'id_asset_master#11', 'id_asset_received', 'id asset received', 16, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(122, 'asset_item', 'id_asset_master#11', 'id_asset_item_location', 'id asset item location', 17, 0, 0, 0, 0, 2, 20, NULL, NULL, '', ''),
(123, 'asset_item', 'id_asset_master#11', 'id_type_asset_item1', 'id type asset item1', 18, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(124, 'asset_item', 'id_asset_master#11', 'id_type_asset_item2', 'id type asset item2', 19, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(125, 'asset_item', 'id_asset_master#11', 'id_type_asset_item3', 'id type asset item3', 20, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(126, 'asset_item', 'id_asset_master#11', 'id_type_asset_item4', 'id type asset item4', 21, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(127, 'asset_item', 'id_asset_master#11', 'id_type_asset_item5', 'id type asset item5', 22, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(128, 'asset_item', 'id_asset_master#11', 'file1', 'file1', 23, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(129, 'asset_item', 'id_asset_master#11', 'file2', 'file2', 24, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(130, 'asset_item', 'id_asset_master#11', 'file3', 'file3', 25, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(131, 'asset_item', 'id_asset_master#11', 'label1', 'Pemilik', 26, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(132, 'asset_item', 'id_asset_master#11', 'label2', 'Alamat', 27, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(133, 'asset_item', 'id_asset_master#11', 'label3', 'Luas Tanah', 28, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(134, 'asset_item', 'id_asset_master#11', 'label4', 'Posisi Tanah', 29, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(135, 'asset_item', 'id_asset_master#11', 'label5', 'label5', 30, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(166, 'asset_item', 'id_asset_master#12', 'id_asset_master', 'id asset master', 1, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(167, 'asset_item', 'id_asset_master#12', 'id_asset_item_parent', 'id asset item parent', 2, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(168, 'asset_item', 'id_asset_master#12', 'number1', 'number1', 3, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(169, 'asset_item', 'id_asset_master#12', 'number2', 'number2', 4, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(170, 'asset_item', 'id_asset_master#12', 'number3', 'number3', 5, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(171, 'asset_item', 'id_asset_master#12', 'picture1', 'picture1', 6, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(172, 'asset_item', 'id_asset_master#12', 'picture2', 'picture2', 7, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(173, 'asset_item', 'id_asset_master#12', 'picture3', 'picture3', 8, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(174, 'asset_item', 'id_asset_master#12', 'picture4', 'picture4', 9, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(175, 'asset_item', 'id_asset_master#12', 'picture5', 'picture5', 10, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(176, 'asset_item', 'id_asset_master#12', 'caption_picture1', 'caption picture1', 11, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(177, 'asset_item', 'id_asset_master#12', 'caption_picture2', 'caption picture2', 12, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(178, 'asset_item', 'id_asset_master#12', 'caption_picture3', 'caption picture3', 13, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(179, 'asset_item', 'id_asset_master#12', 'caption_picture4', 'caption picture4', 14, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(180, 'asset_item', 'id_asset_master#12', 'caption_picture5', 'caption picture5', 15, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(181, 'asset_item', 'id_asset_master#12', 'id_asset_received', 'id asset received', 16, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(182, 'asset_item', 'id_asset_master#12', 'id_asset_item_location', 'id asset item location', 17, 0, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(183, 'asset_item', 'id_asset_master#12', 'id_type_asset_item1', 'Kondisi Bangunan', 58, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(184, 'asset_item', 'id_asset_master#12', 'id_type_asset_item2', 'Konstruksi', 59, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(185, 'asset_item', 'id_asset_master#12', 'id_type_asset_item3', 'id type asset item3', 20, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(186, 'asset_item', 'id_asset_master#12', 'id_type_asset_item4', 'id type asset item4', 21, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(187, 'asset_item', 'id_asset_master#12', 'id_type_asset_item5', 'id type asset item5', 22, 0, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(188, 'asset_item', 'id_asset_master#12', 'file1', 'file1', 23, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(189, 'asset_item', 'id_asset_master#12', 'file2', 'file2', 24, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(190, 'asset_item', 'id_asset_master#12', 'file3', 'file3', 25, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(191, 'asset_item', 'id_asset_master#12', 'label1', 'Jenis Bangunan', 26, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(192, 'asset_item', 'id_asset_master#12', 'label2', 'Pemilik Bangunan', 27, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(193, 'asset_item', 'id_asset_master#12', 'label3', 'Luas Bangunan', 28, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(194, 'asset_item', 'id_asset_master#12', 'label4', 'Pondasi', 29, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(195, 'asset_item', 'id_asset_master#12', 'label5', 'Kerangka', 30, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(196, 'asset_item', 'id_asset_master#12', 'label6', 'Dinding', 31, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(197, 'asset_item', 'id_asset_master#12', 'label7', 'Kusen', 32, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(198, 'asset_item', 'id_asset_master#12', 'label8', 'Kuda-kuda', 33, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(199, 'asset_item', 'id_asset_master#12', 'label9', 'Rangka Atap', 34, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(200, 'asset_item', 'id_asset_master#12', 'label10', 'Daun Pintu', 35, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(201, 'asset_item', 'id_asset_master#12', 'label11', 'Daun Jendela', 36, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(202, 'asset_item', 'id_asset_master#12', 'label12', 'Rangka Langit-Langit', 37, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(203, 'asset_item', 'id_asset_master#12', 'label13', 'Langit-Langit', 38, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(204, 'asset_item', 'id_asset_master#12', 'label14', 'Lantai', 39, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(205, 'asset_item', 'id_asset_master#12', 'label15', 'Teras', 40, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(206, 'asset_item', 'id_asset_master#12', 'label16', 'label16', 41, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(207, 'asset_item', 'id_asset_master#12', 'label17', 'label17', 42, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(208, 'asset_item', 'id_asset_master#12', 'label18', 'label18', 43, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(209, 'asset_item', 'id_asset_master#12', 'label19', 'label19', 44, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(210, 'asset_item', 'id_asset_master#12', 'label20', 'label20', 45, 0, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(211, 'asset_item', 'id_asset_master#14', 'id_asset_master', 'id asset master', 1, 1, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(212, 'asset_item', 'id_asset_master#14', 'id_asset_item_parent', 'id asset item parent', 2, 1, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(213, 'asset_item', 'id_asset_master#14', 'number1', 'number1', 3, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(214, 'asset_item', 'id_asset_master#14', 'number2', 'number2', 4, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(215, 'asset_item', 'id_asset_master#14', 'number3', 'number3', 5, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(216, 'asset_item', 'id_asset_master#14', 'picture1', 'picture1', 6, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(217, 'asset_item', 'id_asset_master#14', 'picture2', 'picture2', 7, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(218, 'asset_item', 'id_asset_master#14', 'picture3', 'picture3', 8, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(219, 'asset_item', 'id_asset_master#14', 'picture4', 'picture4', 9, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(220, 'asset_item', 'id_asset_master#14', 'picture5', 'picture5', 10, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(221, 'asset_item', 'id_asset_master#14', 'caption_picture1', 'caption picture1', 11, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(222, 'asset_item', 'id_asset_master#14', 'caption_picture2', 'caption picture2', 12, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(223, 'asset_item', 'id_asset_master#14', 'caption_picture3', 'caption picture3', 13, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(224, 'asset_item', 'id_asset_master#14', 'caption_picture4', 'caption picture4', 14, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(225, 'asset_item', 'id_asset_master#14', 'caption_picture5', 'caption picture5', 15, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(226, 'asset_item', 'id_asset_master#14', 'id_asset_received', 'id asset received', 16, 1, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(227, 'asset_item', 'id_asset_master#14', 'id_asset_item_location', 'id asset item location', 17, 1, 1, 0, 0, 2, 20, NULL, NULL, '', ''),
(228, 'asset_item', 'id_asset_master#14', 'id_type_asset_item1', 'id type asset item1', 18, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(229, 'asset_item', 'id_asset_master#14', 'id_type_asset_item2', 'id type asset item2', 19, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(230, 'asset_item', 'id_asset_master#14', 'id_type_asset_item3', 'id type asset item3', 20, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(231, 'asset_item', 'id_asset_master#14', 'id_type_asset_item4', 'id type asset item4', 21, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(232, 'asset_item', 'id_asset_master#14', 'id_type_asset_item5', 'id type asset item5', 22, 1, 0, 0, 0, 2, 11, NULL, NULL, '', ''),
(233, 'asset_item', 'id_asset_master#14', 'file1', 'file1', 23, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(234, 'asset_item', 'id_asset_master#14', 'file2', 'file2', 24, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(235, 'asset_item', 'id_asset_master#14', 'file3', 'file3', 25, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(236, 'asset_item', 'id_asset_master#14', 'label1', 'label1', 26, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(237, 'asset_item', 'id_asset_master#14', 'label2', 'label2', 27, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(238, 'asset_item', 'id_asset_master#14', 'label3', 'label3', 28, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(239, 'asset_item', 'id_asset_master#14', 'label4', 'label4', 29, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(240, 'asset_item', 'id_asset_master#14', 'label5', 'label5', 30, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(241, 'asset_item', 'id_asset_master#14', 'label6', 'label6', 31, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(242, 'asset_item', 'id_asset_master#14', 'label7', 'label7', 32, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(243, 'asset_item', 'id_asset_master#14', 'label8', 'label8', 33, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(244, 'asset_item', 'id_asset_master#14', 'label9', 'label9', 34, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(245, 'asset_item', 'id_asset_master#14', 'label10', 'label10', 35, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(246, 'asset_item', 'id_asset_master#14', 'label11', 'label11', 36, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(247, 'asset_item', 'id_asset_master#14', 'label12', 'label12', 37, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(248, 'asset_item', 'id_asset_master#14', 'label13', 'label13', 38, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(249, 'asset_item', 'id_asset_master#14', 'label14', 'label14', 39, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(250, 'asset_item', 'id_asset_master#14', 'label15', 'label15', 40, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(251, 'asset_item', 'id_asset_master#14', 'label16', 'label16', 41, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(252, 'asset_item', 'id_asset_master#14', 'label17', 'label17', 42, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(253, 'asset_item', 'id_asset_master#14', 'label18', 'label18', 43, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(254, 'asset_item', 'id_asset_master#14', 'label19', 'label19', 44, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(255, 'asset_item', 'id_asset_master#14', 'label20', 'label20', 45, 1, 0, 0, 0, 1, 250, NULL, NULL, '', ''),
(256, 'asset_master', '', 'id_account_code', 'Account Name', 0, 0, 0, 0, 0, 2, 12, '', '', '0', '0'),
(257, 'asset_master', '', 'id_mst_accrual', 'Method', 0, 0, 0, 0, 0, 2, 12, '', '', '0', '0'),
(258, 'asset_master', '', 'account_umur_economic_age', 'Account Umur Economic Age', 0, 0, 0, 1, 1, 2, 12, '', '', '0', '0'),
(259, 'asset_master', '', 'account_residual_value', 'Account Residual Value', 0, 0, 0, 1, 1, 2, 12, '', '', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `app_setting`
--

CREATE TABLE `app_setting` (
  `id_app_setting` int(11) NOT NULL,
  `setting_name` varchar(250) NOT NULL,
  `is_image` int(1) NOT NULL,
  `value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_setting`
--

INSERT INTO `app_setting` (`id_app_setting`, `setting_name`, `is_image`, `value`) VALUES
(1, 'APP-NAME', 0, 'Inventarisasi Tanah Kementrian Pekerjaan Umum Dan Perumahan Rakyat Yang Dikelola Oleh Perum Jasa Tirta I'),
(2, 'APP-NAME-SINGKAT', 0, 'INVENTARISASI TANAH'),
(3, 'APP-NAME-SINGKATAN', 0, 'InTands'),
(4, 'Logo', 1, 'Logo.png'),
(5, 'Icon', 1, 'Icon.jpg'),
(6, 'ADDRESS', 0, 'Alamat Perusahaan'),
(7, 'Copyright', 0, 'Copyright {TAHUN} PUPR. All Right Reserved'),
(8, 'MAIN-BACKGROUND', 1, 'MAIN-BACKGROUND.jpeg'),
(100, 'ID-MAIN-ASSET', 0, 'LzloRzRtWjh0S3d3ZitTMko0UENYQT09');

-- --------------------------------------------------------

--
-- Table structure for table `app_vocabulary`
--

CREATE TABLE `app_vocabulary` (
  `id_app_vocabulary` bigint(20) NOT NULL,
  `master_vocab` varchar(150) NOT NULL,
  `vocab_lang1` varchar(250) NOT NULL,
  `vocab_lang2` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_vocabulary`
--

INSERT INTO `app_vocabulary` (`id_app_vocabulary`, `master_vocab`, `vocab_lang1`, `vocab_lang2`) VALUES
(1, 'Type Asset Item 1', 'Kode Aset', ''),
(2, 'Type Asset Item 2', 'Status SIMAK', NULL),
(3, 'Data Aset', 'Data Aset Tanah', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_code`
--

CREATE TABLE `asset_code` (
  `id_asset_code` bigint(20) NOT NULL,
  `id_parent_asset_code` bigint(20) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_item`
--

CREATE TABLE `asset_item` (
  `id_asset_item` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `id_asset_item_parent` bigint(20) NOT NULL,
  `number1` varchar(250) DEFAULT NULL,
  `number2` varchar(250) DEFAULT NULL,
  `number3` varchar(250) DEFAULT NULL,
  `picture1` varchar(250) DEFAULT NULL,
  `picture2` varchar(250) DEFAULT NULL,
  `picture3` varchar(250) DEFAULT NULL,
  `picture4` varchar(250) DEFAULT NULL,
  `picture5` varchar(250) DEFAULT NULL,
  `caption_picture1` varchar(250) DEFAULT NULL,
  `caption_picture2` varchar(250) DEFAULT NULL,
  `caption_picture3` varchar(250) DEFAULT NULL,
  `caption_picture4` varchar(250) DEFAULT NULL,
  `caption_picture5` varchar(250) DEFAULT NULL,
  `id_asset_received` bigint(20) NOT NULL,
  `id_asset_item_location` bigint(20) NOT NULL,
  `id_type_asset_item1` int(11) DEFAULT NULL,
  `id_type_asset_item2` int(11) DEFAULT NULL,
  `id_type_asset_item3` int(11) DEFAULT NULL,
  `id_type_asset_item4` int(11) DEFAULT NULL,
  `id_type_asset_item5` int(11) DEFAULT NULL,
  `file1` varchar(250) DEFAULT NULL,
  `file2` varchar(250) DEFAULT NULL,
  `file3` varchar(250) DEFAULT NULL,
  `label1` varchar(250) DEFAULT NULL,
  `label2` varchar(250) DEFAULT NULL,
  `label3` varchar(250) DEFAULT NULL,
  `label4` varchar(250) DEFAULT NULL,
  `label5` varchar(250) DEFAULT NULL,
  `label6` varchar(250) DEFAULT NULL,
  `label7` varchar(250) DEFAULT NULL,
  `label8` varchar(250) DEFAULT NULL,
  `label9` varchar(250) DEFAULT NULL,
  `label10` varchar(250) DEFAULT NULL,
  `label11` varchar(250) DEFAULT NULL,
  `label12` varchar(250) DEFAULT NULL,
  `label13` varchar(250) DEFAULT NULL,
  `label14` varchar(250) DEFAULT NULL,
  `label15` varchar(250) DEFAULT NULL,
  `label16` varchar(250) DEFAULT NULL,
  `label17` varchar(250) DEFAULT NULL,
  `label18` varchar(250) DEFAULT NULL,
  `label19` varchar(250) DEFAULT NULL,
  `label20` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `asset_item`
--

INSERT INTO `asset_item` (`id_asset_item`, `id_asset_master`, `id_asset_item_parent`, `number1`, `number2`, `number3`, `picture1`, `picture2`, `picture3`, `picture4`, `picture5`, `caption_picture1`, `caption_picture2`, `caption_picture3`, `caption_picture4`, `caption_picture5`, `id_asset_received`, `id_asset_item_location`, `id_type_asset_item1`, `id_type_asset_item2`, `id_type_asset_item3`, `id_type_asset_item4`, `id_type_asset_item5`, `file1`, `file2`, `file3`, `label1`, `label2`, `label3`, `label4`, `label5`, `label6`, `label7`, `label8`, `label9`, `label10`, `label11`, `label12`, `label13`, `label14`, `label15`, `label16`, `label17`, `label18`, `label19`, `label20`) VALUES
(101, 1, 0, '23', '1', NULL, 'Screenshot_8.png', '', '', '', '', 'Bis Pertama di Dunia', 'Cap2', 'Cap3', 'Caplok Sukses', 'Cap5', 1, 1, 10, 100, NULL, NULL, NULL, 'file_asset_1.pdf', NULL, NULL, 'T052 - T051', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', ''),
(102, 1, 0, '888', '', NULL, 'Screenshot_8.png', '', '', '', '', 'Cap1', 'Cap2', 'Cap3', '', '', 2, 2, 10, 100, NULL, NULL, NULL, 'file_asset_2.pdf', NULL, NULL, 'T054 - T053', NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', ''),
(111, 10, 101, 'wewav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 1000, NULL, NULL, NULL, NULL, NULL, 'T123', '4', '5', '6', 'Sdddd', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', ''),
(113, 11, 102, 'ss', 's', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ddda', NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', ''),
(118, 12, 101, 'ss', 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 100, NULL, NULL, NULL, NULL, NULL, NULL, 'sssssss', NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', ''),
(119, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, 'as', NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', ''),
(121, 11, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'T123', 'Sorek Bergembira', 'Luas Arena Gembira', 'posisi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 10, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 1000, NULL, NULL, NULL, NULL, NULL, '11', '2', '3', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 10, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 1001, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 11, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Suparman', 'Jalan Raya Arcamanik', '3', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 12, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fff', 'fff', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 10, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 1000, NULL, NULL, NULL, NULL, NULL, '11', '2', '34', '32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 11, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '56', 'Alamat', '89', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 11, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 's', 'Super Sekali', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_condition_log`
--

CREATE TABLE `asset_item_condition_log` (
  `id_asset_item_condition_log` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `id_mst_status_condition` int(11) NOT NULL,
  `condition_log_date` date NOT NULL,
  `condition_log_datetime` datetime NOT NULL,
  `condition_log_notes` text DEFAULT NULL,
  `reported_by` varchar(250) DEFAULT NULL,
  `reported_user_id` int(11) DEFAULT NULL,
  `reported_ip_address` varchar(250) DEFAULT NULL,
  `photo1` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_deletion`
--

CREATE TABLE `asset_item_deletion` (
  `id_asset_item_deletion` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `status_deletion` set('destroy','acquisition','grant') NOT NULL,
  `execution_date` date NOT NULL,
  `execution_month` int(2) NOT NULL,
  `execution_year` int(4) NOT NULL,
  `execution_id_user` int(11) DEFAULT NULL,
  `execution_user` varchar(250) DEFAULT NULL,
  `income` double(20,2) DEFAULT 0.00,
  `id_mst_status_condition` int(11) NOT NULL,
  `condition_when_deletion` varchar(250) DEFAULT NULL,
  `acquisition_by` varchar(250) DEFAULT NULL,
  `grant_to` varchar(250) DEFAULT NULL,
  `photo1` varchar(250) NOT NULL,
  `photo2` varchar(250) NOT NULL,
  `notes` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_item_deletion`
--

INSERT INTO `asset_item_deletion` (`id_asset_item_deletion`, `id_asset_item`, `status_deletion`, `execution_date`, `execution_month`, `execution_year`, `execution_id_user`, `execution_user`, `income`, `id_mst_status_condition`, `condition_when_deletion`, `acquisition_by`, `grant_to`, `photo1`, `photo2`, `notes`) VALUES
(1, 10, 'destroy', '0000-00-00', 1, 1, 1, '', NULL, 1, '', '', '', '1', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_distribution_current`
--

CREATE TABLE `asset_item_distribution_current` (
  `id_asset_item_distribution_current` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `distribute_to` set('EMPLOYEE','DEPARTEMEN','NOT SET') NOT NULL DEFAULT 'NOT SET',
  `id_pegawai` int(11) DEFAULT NULL,
  `id_departement` bigint(20) DEFAULT NULL,
  `id_asset_item_location` bigint(20) DEFAULT NULL,
  `status` set('ACTIVE','IN-ACTIVE') NOT NULL,
  `start_date` date DEFAULT NULL,
  `start_month` int(2) DEFAULT NULL,
  `start_year` int(4) DEFAULT NULL,
  `duration` varchar(5) DEFAULT NULL,
  `handover_by` varchar(250) DEFAULT NULL,
  `handover_condition_notes` varchar(250) DEFAULT NULL,
  `id_mst_status_condition` int(11) NOT NULL,
  `handover_photos1` varchar(250) DEFAULT NULL,
  `handover_photos2` varchar(250) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_item_distribution_current`
--

INSERT INTO `asset_item_distribution_current` (`id_asset_item_distribution_current`, `id_asset_item`, `distribute_to`, `id_pegawai`, `id_departement`, `id_asset_item_location`, `status`, `start_date`, `start_month`, `start_year`, `duration`, `handover_by`, `handover_condition_notes`, `id_mst_status_condition`, `handover_photos1`, `handover_photos2`, `notes`) VALUES
(1, 1, 'DEPARTEMEN', 1, NULL, NULL, 'ACTIVE', NULL, NULL, NULL, '', '', '', 0, '', '', ''),
(2, 1, 'EMPLOYEE', 1, 1, 1, 'ACTIVE', '0000-00-00', 1, 1, '1', '1', '1', 1, '1', '1', '1'),
(3, 10, 'EMPLOYEE', 1, 1, 1, 'ACTIVE', '0000-00-00', 1, 1, '1', '1', '1', 1, '1', '1', '1'),
(4, 1, 'EMPLOYEE', 1, 1, 1, 'ACTIVE', '0000-00-00', 1, 1, '1', '1', '1', 1, '1', '1', '1'),
(5, 10, 'EMPLOYEE', 1, 1, 1, 'ACTIVE', '0000-00-00', 1, 1, '1', '1', '1', 1, '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_distribution_log`
--

CREATE TABLE `asset_item_distribution_log` (
  `id_asset_item_distribution_log` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `distribute_to` set('USER','DEPARTEMEN','NOT SET') NOT NULL DEFAULT 'NOT SET',
  `id_pegawai` int(11) DEFAULT NULL,
  `from_id_pegawai` int(11) NOT NULL,
  `id_departement` bigint(20) DEFAULT NULL,
  `id_asset_item_location` bigint(20) DEFAULT NULL,
  `status` set('ACTIVE','IN-ACTIVE') NOT NULL,
  `start_date` date DEFAULT NULL,
  `start_month` int(2) DEFAULT NULL,
  `start_year` int(4) DEFAULT NULL,
  `status_distribution` int(1) NOT NULL,
  `end_date` date DEFAULT NULL,
  `end_month` int(2) DEFAULT NULL,
  `end_year` int(4) DEFAULT NULL,
  `duration` varchar(5) DEFAULT NULL,
  `handover_by` varchar(250) DEFAULT NULL,
  `handover_condition_notes` varchar(250) DEFAULT NULL,
  `id_mst_status_condition` int(11) NOT NULL,
  `handover_photos1` varchar(250) DEFAULT NULL,
  `handover_photos2` varchar(250) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_item_distribution_log`
--

INSERT INTO `asset_item_distribution_log` (`id_asset_item_distribution_log`, `id_asset_item`, `distribute_to`, `id_pegawai`, `from_id_pegawai`, `id_departement`, `id_asset_item_location`, `status`, `start_date`, `start_month`, `start_year`, `status_distribution`, `end_date`, `end_month`, `end_year`, `duration`, `handover_by`, `handover_condition_notes`, `id_mst_status_condition`, `handover_photos1`, `handover_photos2`, `notes`) VALUES
(1, 10, '', 1, 1, 1, 1, 'ACTIVE', '0000-00-00', 1, 1, 1, '0000-00-00', 1, 1, '1', '1', '1', 1, '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_incident`
--

CREATE TABLE `asset_item_incident` (
  `id_asset_item_incident` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `incident_date` date NOT NULL,
  `incident_datetime` datetime NOT NULL,
  `incident_notes` text DEFAULT NULL,
  `reported_by` varchar(250) DEFAULT NULL,
  `reported_user_id` int(11) DEFAULT NULL,
  `reported_ip_address` varchar(250) DEFAULT NULL,
  `photo1` varchar(225) DEFAULT NULL,
  `photo2` int(11) DEFAULT NULL,
  `photo3` int(11) DEFAULT NULL,
  `photo4` int(11) DEFAULT NULL,
  `photo5` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_item_incident`
--

INSERT INTO `asset_item_incident` (`id_asset_item_incident`, `id_asset_item`, `incident_date`, `incident_datetime`, `incident_notes`, `reported_by`, `reported_user_id`, `reported_ip_address`, `photo1`, `photo2`, `photo3`, `photo4`, `photo5`) VALUES
(37, 10, '2020-01-23', '0000-00-00 00:00:00', 'Ramdan', '', NULL, NULL, 'asset_item_incident_photo1_37.jpg', NULL, NULL, NULL, NULL),
(40, 12, '2020-01-23', '0000-00-00 00:00:00', '', '', NULL, NULL, 'asset_item_incident_photo1_40.jpg', NULL, NULL, NULL, NULL),
(41, 10, '2020-01-25', '0000-00-00 00:00:00', 'RADAMNADANDN', '', NULL, NULL, 'asset_item_incident_photo1_41.jpg', NULL, NULL, NULL, NULL),
(42, 12, '2020-01-30', '0000-00-00 00:00:00', '', '', NULL, NULL, 'asset_item_incident_photo1_42.jpg', NULL, NULL, NULL, NULL),
(43, 1, '2020-01-23', '0000-00-00 00:00:00', '', 'Ramdan', NULL, NULL, 'asset_item_incident_photo1_43.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_location`
--

CREATE TABLE `asset_item_location` (
  `id_asset_item_location` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `latitude` varchar(60) NOT NULL,
  `longitude` varchar(60) NOT NULL,
  `address` varchar(250) NOT NULL,
  `desa` varchar(250) DEFAULT NULL,
  `kecamatan` varchar(250) DEFAULT NULL,
  `kabupaten` varchar(250) DEFAULT NULL,
  `provinsi` varchar(250) DEFAULT NULL,
  `kodepos` varchar(250) NOT NULL,
  `id_kabupaten` int(11) DEFAULT NULL,
  `id_propinsi` int(11) DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL,
  `id_kelurahan` int(11) DEFAULT NULL,
  `batas_utara` varchar(250) DEFAULT NULL,
  `batas_selatan` varchar(250) DEFAULT NULL,
  `batas_timur` varchar(250) DEFAULT NULL,
  `batas_barat` varchar(250) DEFAULT NULL,
  `luas` double(18,3) NOT NULL,
  `keterangan1` varchar(250) DEFAULT NULL,
  `keterangan2` varchar(250) DEFAULT NULL,
  `keterangan3` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Ini khusus untuk aset diam seperti tanah, gedung';

--
-- Dumping data for table `asset_item_location`
--

INSERT INTO `asset_item_location` (`id_asset_item_location`, `id_asset_master`, `latitude`, `longitude`, `address`, `desa`, `kecamatan`, `kabupaten`, `provinsi`, `kodepos`, `id_kabupaten`, `id_propinsi`, `id_kecamatan`, `id_kelurahan`, `batas_utara`, `batas_selatan`, `batas_timur`, `batas_barat`, `luas`, `keterangan1`, `keterangan2`, `keterangan3`) VALUES
(1, 1, '-7.975293', '110.923327', 'Ds Baturetno, Baturetno, Kb Wonogiri', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 'Gn. Wd. Wonogiri', '', '', '', 132000.000, '', NULL, NULL),
(2, 1, '-7.936381', '110.90587', 'Ds Pulung, Baturetno, Wonogiri', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 'Gn. Wd. Wonogiri', '', '', '', 0.000, 'Bukti Milik saya', NULL, NULL),
(3, 2, 'ds', 's', 's', NULL, NULL, NULL, NULL, 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, NULL, NULL),
(4, 1, '123', '3', 'Banjarnegara', NULL, NULL, NULL, NULL, '3123', 1113, 36, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, NULL, NULL),
(5, 1, 'd', 'd', 'df', NULL, NULL, NULL, NULL, '', 1101, 11, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, NULL, NULL),
(6, 1, '89,821', '90sa', 'Bandung raya', NULL, NULL, NULL, NULL, '', 1116, NULL, NULL, NULL, 'Utara Jaya', 'Selatan Paling Ujung', 'Timur ', 'barat', 0.000, NULL, NULL, NULL),
(7, 1, '54', '56', 'df', NULL, NULL, NULL, NULL, '', 1102, 13, NULL, NULL, '', '', '', '', 0.000, NULL, NULL, NULL),
(8, 1, 'south', 'east', '', NULL, NULL, NULL, NULL, '', 1111, NULL, NULL, NULL, 'Embuh', 'Nyoh', 'Batas Timur', 'Batas Barat', 0.000, NULL, NULL, NULL),
(9, 1, 'd', 'd', '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', 0.000, NULL, NULL, NULL),
(10, 3, 'Ini Baru Loh', 'Ini lebih Baru', '', NULL, NULL, NULL, NULL, '', 1115, NULL, NULL, NULL, 'u', 's', 't', 'b', 0.000, NULL, NULL, NULL),
(11, 1, 's', 't', '', NULL, NULL, NULL, NULL, '', 1111, NULL, NULL, NULL, 'u', 's', 't', 'b', 0.000, '', NULL, NULL),
(12, 1, 'd', '12', '', NULL, NULL, NULL, NULL, '', 1116, NULL, NULL, NULL, 'u', 's', 'Timur ', 'barat', 0.000, 'Bukti Milik', NULL, NULL),
(13, 1, '12.33', '43.1232', '', NULL, NULL, NULL, NULL, '', 1101, NULL, 1, 1, '', '', '', '', 0.000, 'YA', NULL, NULL),
(14, 1, '123', '231', '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', 12.000, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_maintenance`
--

CREATE TABLE `asset_item_maintenance` (
  `id_asset_item_maintenance` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `id_asset_master_criteria_maintenance` bigint(20) DEFAULT NULL,
  `last_primer_value` double(20,4) DEFAULT NULL,
  `maintenance_date` date NOT NULL,
  `id_vendor` int(11) DEFAULT NULL,
  `carried_to_vendor_by` varchar(250) DEFAULT NULL,
  `estimated_day` int(11) NOT NULL,
  `status_maintenance` int(11) NOT NULL,
  `maintenance_finish_date` date NOT NULL,
  `maintenance_cost` double(16,2) DEFAULT NULL,
  `received_date` date DEFAULT NULL,
  `received_user` varchar(250) DEFAULT NULL,
  `maintenance_info` varchar(250) NOT NULL,
  `sparepart_changes_info` varchar(250) DEFAULT NULL,
  `last_condition_report` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_item_maintenance`
--

INSERT INTO `asset_item_maintenance` (`id_asset_item_maintenance`, `id_asset_item`, `id_asset_master_criteria_maintenance`, `last_primer_value`, `maintenance_date`, `id_vendor`, `carried_to_vendor_by`, `estimated_day`, `status_maintenance`, `maintenance_finish_date`, `maintenance_cost`, `received_date`, `received_user`, `maintenance_info`, `sparepart_changes_info`, `last_condition_report`) VALUES
(1, 1, 1, NULL, '2020-02-20', NULL, '', 2, 2, '2020-02-26', NULL, NULL, '', '2', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_repair`
--

CREATE TABLE `asset_item_repair` (
  `id_asset_item_repair` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `id_asset_item_incident` bigint(20) DEFAULT NULL,
  `repair_date` date NOT NULL,
  `id_vendor` int(11) DEFAULT NULL,
  `carried_to_vendor_by` varchar(250) DEFAULT NULL,
  `estimated_day` int(11) NOT NULL,
  `status_repair` int(11) NOT NULL,
  `repair_finish_date` date NOT NULL,
  `repair_cost` double(16,2) DEFAULT NULL,
  `received_date` date DEFAULT NULL,
  `received_user` varchar(250) DEFAULT NULL,
  `repair_info` varchar(250) NOT NULL,
  `sparepart_changes_info` varchar(250) DEFAULT NULL,
  `last_condition_report` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_item_repair`
--

INSERT INTO `asset_item_repair` (`id_asset_item_repair`, `id_asset_item`, `id_asset_item_incident`, `repair_date`, `id_vendor`, `carried_to_vendor_by`, `estimated_day`, `status_repair`, `repair_finish_date`, `repair_cost`, `received_date`, `received_user`, `repair_info`, `sparepart_changes_info`, `last_condition_report`) VALUES
(1, 11, 12, '0000-00-00', 12, '12', 12, 12, '0000-00-00', NULL, NULL, '', '12', '', ''),
(2, 10, 12, '0000-00-00', 1, '1', 1, 1, '0000-00-00', NULL, '0000-00-00', '1', '1', '1', '1'),
(3, 10, 12, '0000-00-00', 12, '12', 12, 12, '0000-00-00', 12.00, '0000-00-00', '12', '12', '12', '12'),
(4, 10, NULL, '2020-02-19', NULL, '', 12, 12, '2020-02-26', NULL, '2020-02-20', '', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_tracking_device`
--

CREATE TABLE `asset_item_tracking_device` (
  `id_asset_item_tracking_device` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `id_device` bigint(20) NOT NULL,
  `installed_date` date DEFAULT NULL,
  `installed_by` varchar(150) DEFAULT NULL,
  `status_active` int(1) NOT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_tracking_device_log`
--

CREATE TABLE `asset_item_tracking_device_log` (
  `id_asset_item_tracking_device_log` bigint(20) NOT NULL,
  `id_asset_item_tracking_device` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `id_device` bigint(20) NOT NULL,
  `installed_date` date DEFAULT NULL,
  `installed_by` varchar(150) DEFAULT NULL,
  `status_active` int(1) NOT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_item_tracking_log`
--

CREATE TABLE `asset_item_tracking_log` (
  `id_asset_item_tracking_log` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `id_device_tracking` bigint(20) NOT NULL,
  `log_date` date NOT NULL,
  `log_datetime` datetime NOT NULL,
  `device_logtime` datetime DEFAULT NULL,
  `longitude` varchar(250) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `full_message` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_master`
--

CREATE TABLE `asset_master` (
  `id_asset_master` bigint(20) NOT NULL,
  `asset_name` varchar(250) NOT NULL,
  `id_asset_code` bigint(20) DEFAULT NULL,
  `asset_code` varchar(150) NOT NULL,
  `id_account_code` int(11) DEFAULT NULL,
  `id_mst_accrual` int(11) DEFAULT NULL,
  `id_type_asset1` int(11) DEFAULT NULL,
  `id_type_asset2` int(11) DEFAULT NULL,
  `id_type_asset3` int(11) DEFAULT NULL,
  `id_type_asset4` int(11) DEFAULT NULL,
  `id_type_asset5` int(11) DEFAULT NULL,
  `attribute1` varchar(250) DEFAULT NULL,
  `attribute2` varchar(250) DEFAULT NULL,
  `attribute3` varchar(250) DEFAULT NULL,
  `attribute4` varchar(250) DEFAULT NULL,
  `attribute5` varchar(250) DEFAULT NULL,
  `account_umur_economic_age` bigint(20) DEFAULT NULL,
  `account_residual_value` double(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_master`
--

INSERT INTO `asset_master` (`id_asset_master`, `asset_name`, `id_asset_code`, `asset_code`, `id_account_code`, `id_mst_accrual`, `id_type_asset1`, `id_type_asset2`, `id_type_asset3`, `id_type_asset4`, `id_type_asset5`, `attribute1`, `attribute2`, `attribute3`, `attribute4`, `attribute5`, `account_umur_economic_age`, `account_residual_value`) VALUES
(11, 'Meja Belajar', 2, '2.01.03.08.004', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Proyektor', 3, '2.01.02.01.006', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '12', NULL, 'Ramdan', 11, NULL, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL),
(24, '12', NULL, 'Ramdan1', 11, 2, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'RR12', NULL, 'RR12', 12, 2, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'RR12', NULL, 'RR12', 12, 3, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'TR1', NULL, 'TR1', 11, 3, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, 1, 1.00);

-- --------------------------------------------------------

--
-- Table structure for table `asset_master_criteria_maintenance`
--

CREATE TABLE `asset_master_criteria_maintenance` (
  `id_asset_master_criteria_maintenance` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `criteria` varchar(200) NOT NULL,
  `type_criteria` int(11) NOT NULL,
  `periodic_value` double NOT NULL,
  `metric` int(11) NOT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_master_criteria_maintenance`
--

INSERT INTO `asset_master_criteria_maintenance` (`id_asset_master_criteria_maintenance`, `id_asset_master`, `criteria`, `type_criteria`, `periodic_value`, `metric`, `notes`) VALUES
(1, 11, 'a', 1, 1, 1, ''),
(2, 10, '1', 1, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `asset_master_field_config`
--

CREATE TABLE `asset_master_field_config` (
  `id_asset_master_field_config` int(11) NOT NULL,
  `fieldname` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `is_visible` int(1) NOT NULL,
  `is_required` int(1) NOT NULL,
  `type_field` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_master_field_config`
--

INSERT INTO `asset_master_field_config` (`id_asset_master_field_config`, `fieldname`, `label`, `is_visible`, `is_required`, `type_field`) VALUES
(1, 'ss', 'ss', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `asset_master_location`
--

CREATE TABLE `asset_master_location` (
  `id_asset_master_location` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `latitude` varchar(60) NOT NULL,
  `longitude` varchar(60) NOT NULL,
  `address` varchar(250) NOT NULL,
  `desa` varchar(250) DEFAULT NULL,
  `kecamatan` varchar(250) DEFAULT NULL,
  `kabupaten` varchar(250) DEFAULT NULL,
  `provinsi` varchar(250) DEFAULT NULL,
  `kodepos` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Ini khusus untuk aset diam seperti tanah, gedung';

-- --------------------------------------------------------

--
-- Table structure for table `asset_master_map_year`
--

CREATE TABLE `asset_master_map_year` (
  `id_asset_master_map_year` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `year` int(4) NOT NULL,
  `current_count` bigint(20) NOT NULL,
  `total_need` bigint(20) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_user` int(11) DEFAULT NULL,
  `updated_ip_address` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_master_request`
--

CREATE TABLE `asset_master_request` (
  `id_asset_master_request` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `request_date` date NOT NULL,
  `request_datetime` datetime NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `request_notes` text DEFAULT NULL,
  `requested_by` varchar(250) DEFAULT NULL,
  `requested_user_id` int(11) DEFAULT NULL,
  `requested_ip_address` varchar(250) DEFAULT NULL,
  `approved_status` int(2) NOT NULL,
  `approved_id_user` int(11) DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `approved_datetime` datetime DEFAULT NULL,
  `approved_ipaddress` varchar(64) DEFAULT NULL,
  `approved_notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_master_request`
--

INSERT INTO `asset_master_request` (`id_asset_master_request`, `id_asset_master`, `request_date`, `request_datetime`, `quantity`, `request_notes`, `requested_by`, `requested_user_id`, `requested_ip_address`, `approved_status`, `approved_id_user`, `approved_date`, `approved_datetime`, `approved_ipaddress`, `approved_notes`) VALUES
(6, 10, '2020-01-22', '0000-00-00 00:00:00', NULL, '1234566', 'asdasd23', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(7, 10, '2020-02-19', '0000-00-00 00:00:00', NULL, 'qwe', 'qwe', NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL),
(8, 10, '2020-02-19', '0000-00-00 00:00:00', NULL, 'qwe', 'qwe', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL),
(9, 10, '2020-02-19', '0000-00-00 00:00:00', NULL, 'qweasdasda', 'qwe', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(10, 12, '2020-02-18', '0000-00-00 00:00:00', NULL, 'Perlu Cat', 'admin', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_master_structure`
--

CREATE TABLE `asset_master_structure` (
  `id_asset_master_structure` bigint(20) NOT NULL,
  `id_asset_master_parent` bigint(20) NOT NULL,
  `id_asset_master_child` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_master_structure`
--

INSERT INTO `asset_master_structure` (`id_asset_master_structure`, `id_asset_master_parent`, `id_asset_master_child`) VALUES
(110, 1, 10),
(111, 1, 11),
(112, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `asset_received`
--

CREATE TABLE `asset_received` (
  `id_asset_received` bigint(20) NOT NULL,
  `id_asset_master` bigint(20) NOT NULL,
  `number1` varchar(150) DEFAULT NULL,
  `number2` varchar(150) DEFAULT NULL,
  `number3` varchar(150) DEFAULT NULL,
  `received_date` date NOT NULL,
  `received_year` int(4) NOT NULL,
  `price_received` double NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `id_status_received` int(11) NOT NULL,
  `notes1` varchar(250) DEFAULT NULL,
  `notes2` varchar(250) DEFAULT NULL,
  `notes3` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_received`
--

INSERT INTO `asset_received` (`id_asset_received`, `id_asset_master`, `number1`, `number2`, `number3`, `received_date`, `received_year`, `price_received`, `quantity`, `id_status_received`, `notes1`, `notes2`, `notes3`) VALUES
(29, 1, '1', '1', '1', '2020-01-29', 2019, 19545454, 1000, 2, NULL, NULL, NULL),
(30, 1, '1', '1', '1', '2020-01-29', 2019, 1500000, 1, 1, NULL, NULL, NULL),
(31, 1, '1', '1', '1', '2020-01-30', 2019, 205000, 1, 1, NULL, NULL, NULL),
(32, 10, '1', '1', '1', '2020-01-29', 2019, 450000, 1, 1, NULL, NULL, NULL),
(35, 1, '1', '1', '1', '2020-01-30', 2018, 2018, 1, 2, NULL, NULL, NULL),
(36, 12, '1', '1', '1', '2020-01-30', 2018, 2019, 1, 1, NULL, NULL, NULL),
(37, 1, '1', '1', '1', '2020-01-30', 2018, 2019, 1, 1, NULL, NULL, NULL),
(38, 1, '1', '1', '', '2020-02-14', 2019, 344200000, 1, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_received_to_item`
--

CREATE TABLE `asset_received_to_item` (
  `id_asset_received_to_item` bigint(20) NOT NULL,
  `id_asset_received` bigint(20) NOT NULL,
  `id_asset_item` bigint(20) NOT NULL,
  `created_user` bigint(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip_address` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_received_to_item`
--

INSERT INTO `asset_received_to_item` (`id_asset_received_to_item`, `id_asset_received`, `id_asset_item`, `created_user`, `created_date`, `created_ip_address`) VALUES
(1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1542014079),
('admin', '36', 1565786619),
('admin', '37', 1565787233),
('admin', '7', 1552641965),
('member', '13', 1563241503),
('member', '14', 1547712959),
('member', '15', 1547712959),
('member', '16', 1563241503),
('member', '18', 1547712959),
('member', '32', 1565784712),
('member', '33', 1565785037),
('member', '38', 1565820068);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/account-code/*', 2, NULL, NULL, NULL, 1581404623, 1581404623),
('/admin/assignment/*', 2, NULL, NULL, NULL, 1552641503, 1552641503),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1552641510, 1552641510),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1552641510, 1552641510),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1552641510, 1552641510),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1552641510, 1552641510),
('/admin/default/*', 2, NULL, NULL, NULL, 1552641513, 1552641513),
('/admin/default/index', 2, NULL, NULL, NULL, 1552641513, 1552641513),
('/admin/permission/*', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/create', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/index', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/update', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/permission/view', 2, NULL, NULL, NULL, 1552641517, 1552641517),
('/admin/role/*', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/assign', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/create', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/delete', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/index', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/remove', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/update', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/role/view', 2, NULL, NULL, NULL, 1552641520, 1552641520),
('/admin/route/*', 2, NULL, NULL, NULL, 1552641523, 1552641523),
('/admin/route/assign', 2, NULL, NULL, NULL, 1552641523, 1552641523),
('/admin/route/create', 2, NULL, NULL, NULL, 1552641523, 1552641523),
('/admin/route/index', 2, NULL, NULL, NULL, 1552641523, 1552641523),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1552641523, 1552641523),
('/admin/route/remove', 2, NULL, NULL, NULL, 1552641523, 1552641523),
('/admin/rule/*', 2, NULL, NULL, NULL, 1552641527, 1552641527),
('/admin/rule/create', 2, NULL, NULL, NULL, 1552641527, 1552641527),
('/admin/rule/delete', 2, NULL, NULL, NULL, 1552641527, 1552641527),
('/admin/rule/index', 2, NULL, NULL, NULL, 1552641527, 1552641527),
('/admin/rule/update', 2, NULL, NULL, NULL, 1552641527, 1552641527),
('/admin/rule/view', 2, NULL, NULL, NULL, 1552641527, 1552641527),
('/admin/user/delete', 2, NULL, NULL, NULL, 1552641538, 1552641538),
('/admin/user/index', 2, NULL, NULL, NULL, 1552641538, 1552641538),
('/admin/user/view', 2, NULL, NULL, NULL, 1552641538, 1552641538),
('/app-field-config/*', 2, NULL, NULL, NULL, 1573554003, 1573554003),
('/app-field-config/create', 2, NULL, NULL, NULL, 1573554003, 1573554003),
('/app-field-config/delete', 2, NULL, NULL, NULL, 1573554003, 1573554003),
('/app-field-config/index', 2, NULL, NULL, NULL, 1573554003, 1573554003),
('/app-field-config/update', 2, NULL, NULL, NULL, 1573554003, 1573554003),
('/app-field-config/view', 2, NULL, NULL, NULL, 1573554003, 1573554003),
('/app-setting/*', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/app-setting/create', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/app-setting/delete', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/app-setting/index', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/app-setting/update', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/app-setting/view', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/app-vocabulary/*', 2, NULL, NULL, NULL, 1574669422, 1574669422),
('/app-vocabulary/create', 2, NULL, NULL, NULL, 1574669422, 1574669422),
('/app-vocabulary/delete', 2, NULL, NULL, NULL, 1574669422, 1574669422),
('/app-vocabulary/index', 2, NULL, NULL, NULL, 1574669422, 1574669422),
('/app-vocabulary/update', 2, NULL, NULL, NULL, 1574669422, 1574669422),
('/app-vocabulary/view', 2, NULL, NULL, NULL, 1574669422, 1574669422),
('/asset_item_tracking_log/*', 2, NULL, NULL, NULL, 1581694051, 1581694051),
('/asset-code/*', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/asset-code/create', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/asset-code/delete', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/asset-code/index', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/asset-code/update', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/asset-code/view', 2, NULL, NULL, NULL, 1571279315, 1571279315),
('/asset-in-asset/*', 2, NULL, NULL, NULL, NULL, NULL),
('/asset-item-app/*', 2, NULL, NULL, NULL, 1574924630, 1574924630),
('/asset-item-app/create', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/delete', 2, NULL, NULL, NULL, 1574924630, 1574924630),
('/asset-item-app/download-file', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/gen-pdf', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/index', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/kecamatan', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/kelurahan', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/list', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/list-search', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/update', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/update-asset', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/upload-file', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/upload-file-search', 2, NULL, NULL, NULL, 1574924630, 1574924630),
('/asset-item-app/upload-file1', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/upload-image', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/view', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-app/view-detail', 2, NULL, NULL, NULL, 1574924629, 1574924629),
('/asset-item-condition-log/*', 2, NULL, NULL, NULL, 1580200127, 1580200127),
('/asset-item-deletion/*', 2, NULL, NULL, NULL, 1581404692, 1581404692),
('/asset-item-distribution-current/*', 2, NULL, NULL, NULL, 1581404498, 1581404498),
('/asset-item-distribution-log/*', 2, NULL, NULL, NULL, 1581404488, 1581404488),
('/asset-item-incident/*', 2, NULL, NULL, NULL, 1580179269, 1580179269),
('/asset-item-location/*', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item-location/create', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item-location/delete', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item-location/index', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item-location/kecamatan', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item-location/kelurahan', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item-location/lists', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item-location/update', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item-location/view', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item-location/view-map', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item-maintenance/*', 2, NULL, NULL, NULL, 1581396528, 1581396528),
('/asset-item-repair/*', 2, NULL, NULL, NULL, 1581318273, 1581318273),
('/asset-item-tracking-device-log/*', 2, NULL, NULL, NULL, 1581694228, 1581694228),
('/asset-item-tracking-device/*', 2, NULL, NULL, NULL, 1581694209, 1581694209),
('/asset-item-tracking-log/*', 2, NULL, NULL, NULL, 1581694241, 1581694241),
('/asset-item/*', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item/create', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item/delete', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item/download-file', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/gen-pdf', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/index', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item/kecamatan', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/kelurahan', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/list', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/list-search', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/update', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item/update-asset', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/upload-file', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/upload-file-search', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/upload-file1', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/upload-image', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-item/view', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-item/view-detail', 2, NULL, NULL, NULL, 1573609747, 1573609747),
('/asset-master-criteria-maintenance/*', 2, NULL, NULL, NULL, 1581327152, 1581327152),
('/asset-master-field-config/*', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-field-config/create', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-field-config/delete', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-field-config/index', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-field-config/update', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-field-config/view', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-location/*', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-master-location/create', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-master-location/delete', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-master-location/index', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-location/update', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-master-location/view', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master-map-year/*', 2, NULL, NULL, NULL, 1580264212, 1580264212),
('/asset-master-request/*', 2, NULL, NULL, NULL, 1580264200, 1580264200),
('/asset-master-structure/*', 2, NULL, NULL, NULL, 1542013422, 1548749426),
('/asset-master/*', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master/create', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master/delete', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master/index', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master/update', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-master/view', 2, NULL, NULL, NULL, 1571279316, 1571279316),
('/asset-received-to-item/*', 2, NULL, NULL, NULL, 1580120450, 1580120450),
('/asset-received/*', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-received/create', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-received/delete', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-received/index', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-received/update', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/asset-received/view', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/AssetItemTrackingLogController/*', 2, NULL, NULL, NULL, 1581675548, 1581675548),
('/conditional-asset-item/*', 2, NULL, NULL, NULL, 1580189147, 1580189147),
('/cpanel-leftmenu/*', 2, NULL, NULL, NULL, 1552641574, 1552641574),
('/cpanel-leftmenu/create', 2, NULL, NULL, NULL, 1552641574, 1552641574),
('/cpanel-leftmenu/delete', 2, NULL, NULL, NULL, 1552641574, 1552641574),
('/cpanel-leftmenu/index', 2, NULL, NULL, NULL, 1552641574, 1552641574),
('/cpanel-leftmenu/update', 2, NULL, NULL, NULL, 1552641574, 1552641574),
('/cpanel-leftmenu/view', 2, NULL, NULL, NULL, 1552641574, 1552641574),
('/dashboard/*', 2, NULL, NULL, NULL, 1552641577, 1552641577),
('/dashboard/index', 2, NULL, NULL, NULL, 1569294058, 1569294058),
('/dashboard/main', 2, NULL, NULL, NULL, 1552641577, 1552641577),
('/Departement/*', 2, NULL, NULL, NULL, 1581399282, 1581399282),
('/gii/*', 2, NULL, NULL, NULL, 1552641560, 1552641560),
('/gridview/export/download', 2, NULL, NULL, NULL, 1580379760, 1580379760),
('/hrm-pegawai/*', 2, NULL, NULL, NULL, 1552641579, 1552641579),
('/hrm-pegawai/create', 2, NULL, NULL, NULL, 1552641579, 1552641579),
('/hrm-pegawai/delete', 2, NULL, NULL, NULL, 1552641579, 1552641579),
('/hrm-pegawai/index', 2, NULL, NULL, NULL, 1552641579, 1552641579),
('/hrm-pegawai/update', 2, NULL, NULL, NULL, 1552641579, 1552641579),
('/hrm-pegawai/view', 2, NULL, NULL, NULL, 1552641579, 1552641579),
('/kabupaten/*', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kabupaten/create', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kabupaten/delete', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kabupaten/index', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kabupaten/update', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kabupaten/view', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kamus-petunjuk/*', 2, NULL, NULL, NULL, 1565760399, 1565760399),
('/kamus-petunjuk/create', 2, NULL, NULL, NULL, 1565760399, 1565760399),
('/kamus-petunjuk/delete', 2, NULL, NULL, NULL, 1565760399, 1565760399),
('/kamus-petunjuk/index', 2, NULL, NULL, NULL, 1565760399, 1565760399),
('/kamus-petunjuk/update', 2, NULL, NULL, NULL, 1565760399, 1565760399),
('/kamus-petunjuk/view', 2, NULL, NULL, NULL, 1565760399, 1565760399),
('/kecamatan/*', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kecamatan/create', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kecamatan/delete', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kecamatan/index', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kecamatan/update', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kecamatan/view', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kelurahan/*', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kelurahan/create', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kelurahan/delete', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kelurahan/index', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kelurahan/update', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/kelurahan/view', 2, NULL, NULL, NULL, 1571279317, 1571279317),
('/laporan/*', 2, NULL, NULL, NULL, 1552641588, 1552641588),
('/laporan/bulanan', 2, NULL, NULL, NULL, 1552641588, 1552641588),
('/laporan/captcha', 2, NULL, NULL, NULL, 1552641588, 1552641588),
('/laporan/error', 2, NULL, NULL, NULL, 1552641588, 1552641588),
('/laporan/harian', 2, NULL, NULL, NULL, 1552641588, 1552641588),
('/laporan/scan', 2, NULL, NULL, NULL, 1552641588, 1552641588),
('/map-maker/*', 2, NULL, NULL, NULL, 1575340767, 1575340767),
('/market-place/*', 2, NULL, NULL, NULL, 1566904898, 1566904898),
('/market-place/create', 2, NULL, NULL, NULL, 1566904898, 1566904898),
('/market-place/delete', 2, NULL, NULL, NULL, 1566904898, 1566904898),
('/market-place/index', 2, NULL, NULL, NULL, 1566904898, 1566904898),
('/market-place/update', 2, NULL, NULL, NULL, 1566904898, 1566904898),
('/market-place/view', 2, NULL, NULL, NULL, 1566904898, 1566904898),
('/mst-accrual/*', 2, NULL, NULL, NULL, 1582615168, 1582615168),
('/mst-status-condition/*', 2, NULL, NULL, NULL, 1580200145, 1580200145),
('/mst-status-received/*', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/mst-status-received/create', 2, NULL, NULL, NULL, 1571279305, 1571279305),
('/mst-status-received/delete', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/mst-status-received/index', 2, NULL, NULL, NULL, 1571279305, 1571279305),
('/mst-status-received/update', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/mst-status-received/view', 2, NULL, NULL, NULL, 1571279305, 1571279305),
('/perusahaan/*', 2, NULL, NULL, NULL, 1552641592, 1552641592),
('/perusahaan/create', 2, NULL, NULL, NULL, 1552641592, 1552641592),
('/perusahaan/delete', 2, NULL, NULL, NULL, 1552641592, 1552641592),
('/perusahaan/index', 2, NULL, NULL, NULL, 1552641592, 1552641592),
('/perusahaan/update', 2, NULL, NULL, NULL, 1552641592, 1552641592),
('/perusahaan/view', 2, NULL, NULL, NULL, 1552641592, 1552641592),
('/product-marketplace/*', 2, NULL, NULL, NULL, 1566904715, 1566904715),
('/product-marketplace/create', 2, NULL, NULL, NULL, 1566904715, 1566904715),
('/product-marketplace/delete', 2, NULL, NULL, NULL, 1566904715, 1566904715),
('/product-marketplace/index', 2, NULL, NULL, NULL, 1566904715, 1566904715),
('/product-marketplace/update', 2, NULL, NULL, NULL, 1566904715, 1566904715),
('/product-marketplace/view', 2, NULL, NULL, NULL, 1566904715, 1566904715),
('/propinsi/*', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/propinsi/create', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/propinsi/delete', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/propinsi/index', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/propinsi/update', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/propinsi/view', 2, NULL, NULL, NULL, 1571279306, 1571279306),
('/request-pick/*', 2, NULL, NULL, NULL, 1567478656, 1567478656),
('/request-pick/create', 2, NULL, NULL, NULL, 1567478656, 1567478656),
('/request-pick/delete', 2, NULL, NULL, NULL, 1567478656, 1567478656),
('/request-pick/index', 2, NULL, NULL, NULL, 1567478656, 1567478656),
('/request-pick/update', 2, NULL, NULL, NULL, 1567478656, 1567478656),
('/request-pick/view', 2, NULL, NULL, NULL, 1567478656, 1567478656),
('/sensor/*', 2, NULL, NULL, NULL, 1567474278, 1567474278),
('/sensor/create', 2, NULL, NULL, NULL, 1567474277, 1567474277),
('/sensor/delete', 2, NULL, NULL, NULL, 1567474277, 1567474277),
('/sensor/index', 2, NULL, NULL, NULL, 1567474277, 1567474277),
('/sensor/update', 2, NULL, NULL, NULL, 1567474277, 1567474277),
('/sensor/view', 2, NULL, NULL, NULL, 1567474277, 1567474277),
('/site/*', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/about', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/captcha', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/contact', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/error', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/index', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/login', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/logout', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/site/scan', 2, NULL, NULL, NULL, 1552641595, 1552641595),
('/supplier/*', 2, NULL, NULL, NULL, 1563246527, 1563246527),
('/supplier/change-password', 2, NULL, NULL, NULL, 1565820642, 1565820642),
('/supplier/create', 2, NULL, NULL, NULL, 1563246527, 1563246527),
('/supplier/delete', 2, NULL, NULL, NULL, 1563246527, 1563246527),
('/supplier/generate-user', 2, NULL, NULL, NULL, 1565760430, 1565760430),
('/supplier/index', 2, NULL, NULL, NULL, 1563246527, 1563246527),
('/supplier/update', 2, NULL, NULL, NULL, 1563246527, 1563246527),
('/supplier/userlist', 2, NULL, NULL, NULL, 1565760430, 1565760430),
('/supplier/view', 2, NULL, NULL, NULL, 1563246527, 1563246527),
('/tester/*', 2, NULL, NULL, NULL, 1569291049, 1569291049),
('/tester/create', 2, NULL, NULL, NULL, 1569291048, 1569291048),
('/tester/delete', 2, NULL, NULL, NULL, 1569291048, 1569291048),
('/tester/editable', 2, NULL, NULL, NULL, 1569291048, 1569291048),
('/tester/index', 2, NULL, NULL, NULL, 1569291048, 1569291048),
('/tester/update', 2, NULL, NULL, NULL, 1569291048, 1569291048),
('/tester/view', 2, NULL, NULL, NULL, 1569291048, 1569291048),
('/type-asset-item1/*', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item1/create', 2, NULL, NULL, NULL, 1573609753, 1573609753),
('/type-asset-item1/delete', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item1/index', 2, NULL, NULL, NULL, 1573609753, 1573609753),
('/type-asset-item1/update', 2, NULL, NULL, NULL, 1573609753, 1573609753),
('/type-asset-item1/view', 2, NULL, NULL, NULL, 1573609753, 1573609753),
('/type-asset-item2/*', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item2/create', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item2/delete', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item2/index', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item2/update', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item2/view', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item3/*', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item3/create', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item3/delete', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item3/index', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item3/update', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item3/view', 2, NULL, NULL, NULL, 1573609754, 1573609754),
('/type-asset-item4/*', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item4/create', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item4/delete', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item4/index', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item4/update', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item4/view', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item5/*', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item5/create', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item5/delete', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item5/index', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item5/update', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset-item5/view', 2, NULL, NULL, NULL, 1573609755, 1573609755),
('/type-asset1/*', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset1/create', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset1/delete', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset1/index', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset1/update', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset1/view', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset2/*', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset2/create', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset2/delete', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset2/index', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset2/update', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset2/view', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset3/*', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset3/create', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset3/delete', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset3/index', 2, NULL, NULL, NULL, 1571279296, 1571279296),
('/type-asset3/update', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset3/view', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset4/*', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset4/create', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset4/delete', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset4/index', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset4/update', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset4/view', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset5/*', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset5/create', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset5/delete', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset5/index', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset5/update', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-asset5/view', 2, NULL, NULL, NULL, 1571279297, 1571279297),
('/type-of-vendor/*', 2, NULL, NULL, NULL, 1581319433, 1581319433),
('/user-perusahaan/*', 2, NULL, NULL, NULL, 1552641605, 1552641605),
('/user-perusahaan/create', 2, NULL, NULL, NULL, 1552641604, 1552641604),
('/user-perusahaan/delete', 2, NULL, NULL, NULL, 1552641605, 1552641605),
('/user-perusahaan/index', 2, NULL, NULL, NULL, 1552641604, 1552641604),
('/user-perusahaan/update', 2, NULL, NULL, NULL, 1552641605, 1552641605),
('/user-perusahaan/view', 2, NULL, NULL, NULL, 1552641604, 1552641604),
('/user/*', 2, NULL, NULL, NULL, 1552641600, 1552641600),
('/user/create', 2, NULL, NULL, NULL, 1552641600, 1552641600),
('/user/delete', 2, NULL, NULL, NULL, 1552641600, 1552641600),
('/user/index', 2, NULL, NULL, NULL, 1552641600, 1552641600),
('/user/update', 2, NULL, NULL, NULL, 1552641600, 1552641600),
('/user/view', 2, NULL, NULL, NULL, 1552641600, 1552641600),
('/vendor/*', 2, NULL, NULL, NULL, 1581319336, 1581319336),
('abs-absence/create', 2, 'Create a log', NULL, NULL, 1547712959, 1547712959),
('abs-absence/index', 2, 'Create a index', NULL, NULL, 1547712959, 1547712959),
('abs-absence/view', 2, 'View a log', NULL, NULL, 1547712959, 1547712959),
('admin', 1, 'Application Admin', NULL, NULL, 1542013792, 1552641743),
('cpanel-leftmenu/create', 2, 'Create a menu', NULL, NULL, 1547712959, 1547712959),
('cpanel-leftmenu/delete', 2, 'delete a menu', NULL, NULL, 1547712959, 1547712959),
('cpanel-leftmenu/index', 2, 'Create a index', NULL, NULL, 1547712959, 1547712959),
('cpanel-leftmenu/update', 2, 'Update a menu', NULL, NULL, 1547713493, 1547713493),
('cpanel-leftmenu/view', 2, 'View a menu', NULL, NULL, 1547712959, 1547712959),
('grievance-list-request/index', 2, 'View Grievance List', NULL, NULL, 1563228150, 1563228150),
('hrm-pegawai/create', 2, 'Create Pegawai', NULL, NULL, NULL, NULL),
('hrm-pegawai/delete', 2, 'Delete Pegawai', NULL, NULL, NULL, NULL),
('hrm-pegawai/index', 2, 'Display index', NULL, NULL, NULL, NULL),
('hrm-pegawai/update', 2, 'Update Pegawai', NULL, NULL, NULL, NULL),
('hrm-pegawai/view', 2, 'view Pegawai', NULL, NULL, 1547712959, 1547712959),
('kartu-rfid/create', 2, 'Create a Kartu RFID', NULL, NULL, 1547716430, 1547716430),
('kartu-rfid/delete', 2, 'delete a Kartu RFID', NULL, NULL, 1547716430, 1547716430),
('kartu-rfid/index', 2, 'Index of Kartu RFID', NULL, NULL, 1547716430, 1547716430),
('kartu-rfid/update', 2, 'Update a Kartu RFID', NULL, NULL, 1547716430, 1547716430),
('kartu-rfid/view', 2, 'View a Kartu RFID', NULL, NULL, 1547716430, 1547716430),
('member', 1, 'Member or supplier', NULL, NULL, 1563240747, 1563240747),
('user/create', 2, 'Create a user', NULL, NULL, 1542013422, 1542013422),
('user/delete', 2, 'Delete a user', NULL, NULL, 1542013422, 1548749079),
('user/index', 2, 'Create a index', NULL, NULL, 1542013422, 1548749389),
('user/update', 2, 'Update a user', NULL, NULL, 1542013422, 1542013422),
('user/view', 2, 'View a user', NULL, NULL, 1542013422, 1542013422);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', '/account-code/*'),
('admin', '/admin/assignment/*'),
('admin', '/admin/assignment/assign'),
('admin', '/admin/assignment/index'),
('admin', '/admin/assignment/revoke'),
('admin', '/admin/assignment/view'),
('admin', '/admin/default/*'),
('admin', '/admin/default/index'),
('admin', '/admin/permission/*'),
('admin', '/admin/permission/assign'),
('admin', '/admin/permission/create'),
('admin', '/admin/permission/delete'),
('admin', '/admin/permission/index'),
('admin', '/admin/permission/remove'),
('admin', '/admin/permission/update'),
('admin', '/admin/permission/view'),
('admin', '/admin/role/*'),
('admin', '/admin/role/assign'),
('admin', '/admin/role/create'),
('admin', '/admin/role/delete'),
('admin', '/admin/role/index'),
('admin', '/admin/role/remove'),
('admin', '/admin/role/update'),
('admin', '/admin/role/view'),
('admin', '/admin/route/*'),
('admin', '/admin/route/assign'),
('admin', '/admin/route/create'),
('admin', '/admin/route/index'),
('admin', '/admin/route/refresh'),
('admin', '/admin/route/remove'),
('admin', '/admin/rule/*'),
('admin', '/admin/rule/create'),
('admin', '/admin/rule/delete'),
('admin', '/admin/rule/index'),
('admin', '/admin/rule/update'),
('admin', '/admin/rule/view'),
('admin', '/admin/user/delete'),
('admin', '/admin/user/index'),
('admin', '/admin/user/view'),
('admin', '/app-field-config/*'),
('admin', '/app-field-config/create'),
('admin', '/app-field-config/delete'),
('admin', '/app-field-config/index'),
('admin', '/app-field-config/update'),
('admin', '/app-field-config/view'),
('admin', '/app-setting/*'),
('admin', '/app-setting/create'),
('admin', '/app-setting/delete'),
('admin', '/app-setting/index'),
('admin', '/app-setting/update'),
('admin', '/app-setting/view'),
('admin', '/app-vocabulary/*'),
('admin', '/app-vocabulary/create'),
('admin', '/app-vocabulary/delete'),
('admin', '/app-vocabulary/index'),
('admin', '/app-vocabulary/update'),
('admin', '/app-vocabulary/view'),
('admin', '/asset_item_tracking_log/*'),
('admin', '/asset-code/*'),
('admin', '/asset-code/create'),
('admin', '/asset-code/delete'),
('admin', '/asset-code/index'),
('admin', '/asset-code/update'),
('admin', '/asset-code/view'),
('admin', '/asset-in-asset/*'),
('admin', '/asset-item-app/*'),
('admin', '/asset-item-app/create'),
('admin', '/asset-item-app/delete'),
('admin', '/asset-item-app/download-file'),
('admin', '/asset-item-app/gen-pdf'),
('admin', '/asset-item-app/index'),
('admin', '/asset-item-app/kecamatan'),
('admin', '/asset-item-app/kelurahan'),
('admin', '/asset-item-app/list'),
('admin', '/asset-item-app/list-search'),
('admin', '/asset-item-app/update'),
('admin', '/asset-item-app/update-asset'),
('admin', '/asset-item-app/upload-file'),
('admin', '/asset-item-app/upload-file-search'),
('admin', '/asset-item-app/upload-file1'),
('admin', '/asset-item-app/upload-image'),
('admin', '/asset-item-app/view'),
('admin', '/asset-item-app/view-detail'),
('admin', '/asset-item-condition-log/*'),
('admin', '/asset-item-deletion/*'),
('admin', '/asset-item-distribution-current/*'),
('admin', '/asset-item-distribution-log/*'),
('admin', '/asset-item-incident/*'),
('admin', '/asset-item-location/*'),
('admin', '/asset-item-location/create'),
('admin', '/asset-item-location/delete'),
('admin', '/asset-item-location/index'),
('admin', '/asset-item-location/kecamatan'),
('admin', '/asset-item-location/kelurahan'),
('admin', '/asset-item-location/lists'),
('admin', '/asset-item-location/update'),
('admin', '/asset-item-location/view'),
('admin', '/asset-item-location/view-map'),
('admin', '/asset-item-maintenance/*'),
('admin', '/asset-item-repair/*'),
('admin', '/asset-item-tracking-device-log/*'),
('admin', '/asset-item-tracking-device/*'),
('admin', '/asset-item-tracking-log/*'),
('admin', '/asset-item/*'),
('admin', '/asset-item/create'),
('admin', '/asset-item/delete'),
('admin', '/asset-item/download-file'),
('admin', '/asset-item/gen-pdf'),
('admin', '/asset-item/index'),
('admin', '/asset-item/kecamatan'),
('admin', '/asset-item/kelurahan'),
('admin', '/asset-item/list'),
('admin', '/asset-item/list-search'),
('admin', '/asset-item/update'),
('admin', '/asset-item/update-asset'),
('admin', '/asset-item/upload-file'),
('admin', '/asset-item/upload-file-search'),
('admin', '/asset-item/upload-file1'),
('admin', '/asset-item/upload-image'),
('admin', '/asset-item/view'),
('admin', '/asset-item/view-detail'),
('admin', '/asset-master-criteria-maintenance/*'),
('admin', '/asset-master-field-config/*'),
('admin', '/asset-master-field-config/create'),
('admin', '/asset-master-field-config/delete'),
('admin', '/asset-master-field-config/index'),
('admin', '/asset-master-field-config/update'),
('admin', '/asset-master-field-config/view'),
('admin', '/asset-master-location/*'),
('admin', '/asset-master-location/create'),
('admin', '/asset-master-location/delete'),
('admin', '/asset-master-location/index'),
('admin', '/asset-master-location/update'),
('admin', '/asset-master-location/view'),
('admin', '/asset-master-map-year/*'),
('admin', '/asset-master-request/*'),
('admin', '/asset-master-structure/*'),
('admin', '/asset-master/*'),
('admin', '/asset-master/create'),
('admin', '/asset-master/delete'),
('admin', '/asset-master/index'),
('admin', '/asset-master/update'),
('admin', '/asset-master/view'),
('admin', '/asset-received-to-item/*'),
('admin', '/asset-received/*'),
('admin', '/asset-received/create'),
('admin', '/asset-received/delete'),
('admin', '/asset-received/index'),
('admin', '/asset-received/update'),
('admin', '/asset-received/view'),
('admin', '/AssetItemTrackingLogController/*'),
('admin', '/conditional-asset-item/*'),
('admin', '/cpanel-leftmenu/*'),
('admin', '/cpanel-leftmenu/create'),
('admin', '/cpanel-leftmenu/delete'),
('admin', '/cpanel-leftmenu/index'),
('admin', '/cpanel-leftmenu/update'),
('admin', '/cpanel-leftmenu/view'),
('admin', '/dashboard/*'),
('admin', '/dashboard/index'),
('admin', '/dashboard/main'),
('admin', '/Departement/*'),
('admin', '/gii/*'),
('admin', '/gridview/export/download'),
('admin', '/hrm-pegawai/*'),
('admin', '/hrm-pegawai/create'),
('admin', '/hrm-pegawai/delete'),
('admin', '/hrm-pegawai/index'),
('admin', '/hrm-pegawai/update'),
('admin', '/hrm-pegawai/view'),
('admin', '/kabupaten/*'),
('admin', '/kabupaten/create'),
('admin', '/kabupaten/delete'),
('admin', '/kabupaten/index'),
('admin', '/kabupaten/update'),
('admin', '/kabupaten/view'),
('admin', '/kamus-petunjuk/*'),
('admin', '/kamus-petunjuk/create'),
('admin', '/kamus-petunjuk/delete'),
('admin', '/kamus-petunjuk/index'),
('admin', '/kamus-petunjuk/update'),
('admin', '/kamus-petunjuk/view'),
('admin', '/kecamatan/*'),
('admin', '/kecamatan/create'),
('admin', '/kecamatan/delete'),
('admin', '/kecamatan/index'),
('admin', '/kecamatan/update'),
('admin', '/kecamatan/view'),
('admin', '/kelurahan/*'),
('admin', '/kelurahan/create'),
('admin', '/kelurahan/delete'),
('admin', '/kelurahan/index'),
('admin', '/kelurahan/update'),
('admin', '/kelurahan/view'),
('admin', '/laporan/*'),
('admin', '/laporan/bulanan'),
('admin', '/laporan/captcha'),
('admin', '/laporan/error'),
('admin', '/laporan/harian'),
('admin', '/laporan/scan'),
('admin', '/map-maker/*'),
('admin', '/market-place/*'),
('admin', '/market-place/create'),
('admin', '/market-place/delete'),
('admin', '/market-place/index'),
('admin', '/market-place/update'),
('admin', '/market-place/view'),
('admin', '/mst-accrual/*'),
('admin', '/mst-status-condition/*'),
('admin', '/mst-status-received/*'),
('admin', '/mst-status-received/create'),
('admin', '/mst-status-received/delete'),
('admin', '/mst-status-received/index'),
('admin', '/mst-status-received/update'),
('admin', '/mst-status-received/view'),
('admin', '/perusahaan/*'),
('admin', '/perusahaan/create'),
('admin', '/perusahaan/delete'),
('admin', '/perusahaan/index'),
('admin', '/perusahaan/update'),
('admin', '/perusahaan/view'),
('admin', '/product-marketplace/*'),
('admin', '/product-marketplace/create'),
('admin', '/product-marketplace/delete'),
('admin', '/product-marketplace/index'),
('admin', '/product-marketplace/update'),
('admin', '/product-marketplace/view'),
('admin', '/propinsi/*'),
('admin', '/propinsi/create'),
('admin', '/propinsi/delete'),
('admin', '/propinsi/index'),
('admin', '/propinsi/update'),
('admin', '/propinsi/view'),
('admin', '/request-pick/*'),
('admin', '/request-pick/create'),
('admin', '/request-pick/delete'),
('admin', '/request-pick/index'),
('admin', '/request-pick/update'),
('admin', '/request-pick/view'),
('admin', '/sensor/*'),
('admin', '/sensor/create'),
('admin', '/sensor/delete'),
('admin', '/sensor/index'),
('admin', '/sensor/update'),
('admin', '/sensor/view'),
('admin', '/site/*'),
('admin', '/site/about'),
('admin', '/site/captcha'),
('admin', '/site/contact'),
('admin', '/site/error'),
('admin', '/site/index'),
('admin', '/site/login'),
('admin', '/site/logout'),
('admin', '/site/scan'),
('admin', '/supplier/*'),
('admin', '/supplier/change-password'),
('admin', '/supplier/create'),
('admin', '/supplier/delete'),
('admin', '/supplier/generate-user'),
('admin', '/supplier/index'),
('admin', '/supplier/update'),
('admin', '/supplier/userlist'),
('admin', '/supplier/view'),
('admin', '/tester/*'),
('admin', '/tester/create'),
('admin', '/tester/delete'),
('admin', '/tester/editable'),
('admin', '/tester/index'),
('admin', '/tester/update'),
('admin', '/tester/view'),
('admin', '/type-asset-item1/*'),
('admin', '/type-asset-item1/create'),
('admin', '/type-asset-item1/delete'),
('admin', '/type-asset-item1/index'),
('admin', '/type-asset-item1/update'),
('admin', '/type-asset-item1/view'),
('admin', '/type-asset-item2/*'),
('admin', '/type-asset-item2/create'),
('admin', '/type-asset-item2/delete'),
('admin', '/type-asset-item2/index'),
('admin', '/type-asset-item2/update'),
('admin', '/type-asset-item2/view'),
('admin', '/type-asset-item3/*'),
('admin', '/type-asset-item3/create'),
('admin', '/type-asset-item3/delete'),
('admin', '/type-asset-item3/index'),
('admin', '/type-asset-item3/update'),
('admin', '/type-asset-item3/view'),
('admin', '/type-asset-item4/*'),
('admin', '/type-asset-item4/create'),
('admin', '/type-asset-item4/delete'),
('admin', '/type-asset-item4/index'),
('admin', '/type-asset-item4/update'),
('admin', '/type-asset-item4/view'),
('admin', '/type-asset-item5/*'),
('admin', '/type-asset-item5/create'),
('admin', '/type-asset-item5/delete'),
('admin', '/type-asset-item5/index'),
('admin', '/type-asset-item5/update'),
('admin', '/type-asset-item5/view'),
('admin', '/type-asset1/*'),
('admin', '/type-asset1/create'),
('admin', '/type-asset1/delete'),
('admin', '/type-asset1/index'),
('admin', '/type-asset1/update'),
('admin', '/type-asset1/view'),
('admin', '/type-asset2/*'),
('admin', '/type-asset2/create'),
('admin', '/type-asset2/delete'),
('admin', '/type-asset2/index'),
('admin', '/type-asset2/update'),
('admin', '/type-asset2/view'),
('admin', '/type-asset3/*'),
('admin', '/type-asset3/create'),
('admin', '/type-asset3/delete'),
('admin', '/type-asset3/index'),
('admin', '/type-asset3/update'),
('admin', '/type-asset3/view'),
('admin', '/type-asset4/*'),
('admin', '/type-asset4/create'),
('admin', '/type-asset4/delete'),
('admin', '/type-asset4/index'),
('admin', '/type-asset4/update'),
('admin', '/type-asset4/view'),
('admin', '/type-asset5/*'),
('admin', '/type-asset5/create'),
('admin', '/type-asset5/delete'),
('admin', '/type-asset5/index'),
('admin', '/type-asset5/update'),
('admin', '/type-asset5/view'),
('admin', '/type-of-vendor/*'),
('admin', '/user-perusahaan/*'),
('admin', '/user-perusahaan/create'),
('admin', '/user-perusahaan/delete'),
('admin', '/user-perusahaan/index'),
('admin', '/user-perusahaan/update'),
('admin', '/user-perusahaan/view'),
('admin', '/user/*'),
('admin', '/user/create'),
('admin', '/user/delete'),
('admin', '/user/index'),
('admin', '/user/update'),
('admin', '/user/view'),
('admin', '/vendor/*'),
('admin', 'cpanel-leftmenu/create'),
('admin', 'cpanel-leftmenu/delete'),
('admin', 'cpanel-leftmenu/index'),
('admin', 'cpanel-leftmenu/update'),
('admin', 'cpanel-leftmenu/view'),
('admin', 'grievance-list-request/index'),
('admin', 'user/create'),
('admin', 'user/delete'),
('admin', 'user/index'),
('admin', 'user/update'),
('admin', 'user/view'),
('member', '/site/index');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cpanel_leftmenu`
--

CREATE TABLE `cpanel_leftmenu` (
  `id_leftmenu` int(11) NOT NULL,
  `id_parent_leftmenu` int(11) NOT NULL,
  `has_child` int(1) NOT NULL,
  `menu_name` varchar(200) NOT NULL,
  `menu_icon` varchar(100) NOT NULL,
  `value_indo` varchar(250) NOT NULL,
  `value_eng` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `is_public` int(1) NOT NULL DEFAULT 0,
  `auth` text NOT NULL,
  `mobile_display` set('NONE','MOBILE_TOP','MOBILE_BOTTOM') NOT NULL,
  `visible` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpanel_leftmenu`
--

INSERT INTO `cpanel_leftmenu` (`id_leftmenu`, `id_parent_leftmenu`, `has_child`, `menu_name`, `menu_icon`, `value_indo`, `value_eng`, `url`, `is_public`, `auth`, `mobile_display`, `visible`) VALUES
(2000, 0, 1, 'Dashboard', '', 'Dashboard', 'Dashboard', 'dashboard/index', 0, 'admin', '', 1),
(2001, 2000, 0, 'Utama', '', 'Utama', 'Utama', 'dashboard/index', 0, 'admin', 'MOBILE_TOP', 1),
(11000, 0, 1, 'Aset', '', 'Aset', 'Aset', 'asset-in-asset/list?c=LzloRzRtWjh0S3d3ZitTMko0UENYQT09', 0, 'admin', '', 1),
(11001, 11000, 0, 'Aset', '', 'Aset', 'Aset', 'asset-item/index', 0, 'admin', 'MOBILE_TOP', 1),
(11002, 11000, 0, 'Kategori Aset', '', 'Kategori Aset', 'Kategori Aset', 'asset-master/list', 0, 'admin', 'MOBILE_TOP', 1),
(11003, 11000, 0, 'Pencarian Data Aset', '', 'Pencarian Data Aset', 'Pencarian Data Aset', 'asset-item/list-search', 0, 'admin', '', 1),
(11100, 0, 1, 'Perencanaan Kebutuhan', '', 'Perencanaan Kebutuhan', 'Perencanaan Kebutuhan', '#', 0, 'admin', '', 1),
(11101, 11100, 0, 'Peta Kebutuhan vs Ketersediaan', '', 'Peta Kebutuhan vs Ketersediaan', 'Peta Kebutuhan vs Ketersediaan', 'asset-master-map-year/map', 0, 'admin', 'MOBILE_TOP', 1),
(11102, 11100, 0, 'Pengajuan Aset', '', 'Pengajuan Aset', 'Pengajuan Aset', 'asset-master-request/create', 0, 'admin', 'MOBILE_TOP', 1),
(11103, 11100, 0, 'Data Pengajuan Aset', '', 'Data Pengajuan Aset', 'Data Pengajuan Aset', 'asset-master-request/index', 0, 'admin', 'MOBILE_TOP', 1),
(11104, 11100, 0, 'Approval Pengajuan Aset', '', 'Approval Pengajuan Aset', 'Approval Pengajuan Aset', 'asset-master-request/approval', 0, 'admin', 'MOBILE_TOP', 1),
(11105, 11100, 0, 'Resume Kebutuhan Aset', '', 'Resume Kebutuhan Aset', 'Resume Kebutuhan Aset', 'asset-master-request/resume', 0, 'admin', 'MOBILE_TOP', 1),
(11200, 0, 1, 'Penerimaan Aset', '', 'Penerimaan Aset', 'Penerimaan Aset', '#', 0, 'admin', '', 1),
(11201, 11200, 0, 'Penerimaan Aset', '', 'Penerimaan Aset', 'Penerimaan Aset', 'asset-received/index', 0, 'admin', 'MOBILE_TOP', 1),
(11202, 11200, 0, 'Laporan Penerimaan Aset', '', 'Laporan Penerimaan Aset', 'Laporan Penerimaan Aset', 'asset-received/report', 0, 'admin', 'MOBILE_TOP', 1),
(11203, 11200, 0, 'Penomoran Aset', '', 'Penomoran Aset', 'Penomoran Aset', 'asset-received/numbering', 0, 'admin', 'MOBILE_TOP', 1),
(11204, 11200, 0, 'Warning Penomoran', '', 'Warning Penomoran', 'Warning Penomoran', 'asset-received/numbering', 0, 'admin', 'MOBILE_TOP', 1),
(11300, 0, 1, 'Inventarisasi Aset', '', 'Inventarisasi Aset', 'Inventarisasi Aset', '#', 0, 'admin', '', 1),
(11301, 11300, 0, 'Inventarisasi Aset', '', 'Inventarisasi Aset', 'Inventarisasi Aset', 'conditional-asset-item/list-number', 0, 'admin', 'MOBILE_TOP', 1),
(11302, 11300, 0, 'Updating Kondisi Aset', '', 'Updating Kondisi Aset', 'Updating Kondisi Aset', 'conditional-asset-item/update-condition', 0, 'admin', 'MOBILE_TOP', 1),
(11303, 11300, 0, 'Rekap Kondisi Aset', '', 'Rekap Kondisi Aset', 'Rekap Kondisi Aset', 'conditional-asset-item/resume', 0, 'admin', 'MOBILE_TOP', 1),
(11400, 0, 1, 'Pengoperasian Aset', '', 'Pengoperasian Aset', 'Pengoperasian Aset', '#', 0, 'admin', '', 1),
(11401, 11400, 0, 'Pengguna Aset', '', 'Pengguna Aset', 'Pengguna Aset', 'asset-item-distribution-current/index', 0, 'admin', 'MOBILE_TOP', 1),
(11402, 11401, 0, 'Pengelola Aset', '', 'Pengelola Aset', 'Pengelola Aset', 'asset-item-distribution-current/index', 0, 'admin', 'MOBILE_TOP', 1),
(11403, 11400, 0, 'History Pengguna Aset', '', 'History Pengguna Aset', 'History Pengguna Aset', 'asset-item-distribution-log/index', 0, 'admin', 'MOBILE_TOP', 1),
(11404, 11400, 0, 'Perpindahan Pengguna', '', 'Perpindahan Pengguna', 'Perpindahan Pengguna', 'asset-item-distribution-current/user_change', 0, 'admin', 'MOBILE_TOP', 1),
(11405, 11400, 0, 'Tracking Pergerakan Aset', '', 'Tracking Pergerakan Aset', 'Tracking Pergerakan Aset', 'asset-item-tracking-device/index', 0, 'admin', 'MOBILE_TOP', 1),
(11406, 11400, 0, 'History Pergerakan Aset', '', 'History Pergerakan Aset', 'History Pergerakan Aset', 'asset-item-tracking-device-log/index', 0, 'admin', 'MOBILE_TOP', 1),
(11407, 11400, 0, 'Perpindahan Lokasi', '', 'Perpindahan Lokasi', 'Perpindahan Lokasi', '', 0, 'admin', 'MOBILE_TOP', 0),
(11408, 11400, 0, 'History Perpindahan', '', 'History Perpindahan', 'History Perpindahan', '', 0, 'admin', 'MOBILE_TOP', 0),
(11409, 11400, 0, 'Peta Sebaran Aset', '', 'Peta Sebaran Aset', 'Peta Sebaran Aset', 'asset-item-tracking-device/map-distribute', 0, 'admin', 'MOBILE_TOP', 1),
(11500, 0, 1, 'Kerusakan & Perbaikan Aset', '', 'Kerusakan & Perbaikan Aset', 'Kerusakan & Perbaikan Aset', '#', 0, 'admin', '', 1),
(11501, 11500, 0, 'Pelaporan Aset Rusak', '', 'Pelaporan Aset Rusak', 'Pelaporan Aset Rusak', 'asset-item-incident/create', 0, 'admin', 'MOBILE_TOP', 1),
(11502, 11500, 0, 'Approval Pelaporan Aset', '', 'Approval Pelaporan Aset', 'Approval Pelaporan Aset', 'asset-item-incident/approval', 0, 'admin', 'MOBILE_TOP', 1),
(11503, 11500, 0, 'Perbaikan Aset', '', 'Perbaikan Aset', 'Perbaikan Aset', 'asset-item-repair/index', 0, 'admin', 'MOBILE_TOP', 1),
(11504, 11500, 0, 'History Perbaikan Aset', '', 'History Perbaikan Aset', 'History Perbaikan Aset', 'asset-item-repair/history', 0, 'admin', 'MOBILE_TOP', 1),
(11505, 11500, 0, 'Rekap Perbaikan Aset', '', 'Rekap Perbaikan Aset', 'Rekap Perbaikan Aset', 'asset-item-repair/resume', 0, 'admin', 'MOBILE_TOP', 1),
(11600, 0, 1, 'Perawatan Aset Rutin', '', 'Perawatan Aset Rutin', 'Perawatan Aset Rutin', '#', 0, 'admin', '', 1),
(11601, 11600, 0, 'Kriteria Perawatan Rutin', '', 'Kriteria Perawatan Rutin', 'Kriteria Perawatan Rutin', 'asset-master-criteria-maintenance/index', 0, 'admin', 'MOBILE_TOP', 1),
(11602, 11600, 0, 'Reminder Perawatan', '', 'Reminder Perawatan', 'Reminder Perawatan', 'asset-item-maintenance/reminder', 0, 'admin', 'MOBILE_TOP', 1),
(11603, 11600, 0, 'Predictive Maintenance', '', 'Predictive Maintenance', 'Predictive Maintenance', '', 0, 'admin', 'MOBILE_TOP', 1),
(11604, 11600, 0, 'Perawatan Rutin', '', 'Perawatan Rutin', 'Perawatan Rutin', 'asset-item-maintenance/index', 0, 'admin', 'MOBILE_TOP', 1),
(11605, 11600, 0, 'History Perawatan Aset', '', 'History Perawatan Aset', 'History Perawatan Aset', 'asset-item-maintenance/history', 0, 'admin', 'MOBILE_TOP', 1),
(11606, 11600, 0, 'Rekap Perawatan Aset', '', 'Rekap Perawatan Aset', 'Rekap Perawatan Aset', 'asset-item-maintenance/resume', 0, 'admin', 'MOBILE_TOP', 1),
(11900, 0, 1, 'Evaluasi Aset', '', 'Evaluasi Aset', 'Evaluasi Aset', '#', 0, 'admin', '', 1),
(11901, 11900, 0, 'Utilisasi Aset', '', 'Utilisasi Aset', 'Utilisasi Aset', 'evaluation/utilization', 0, 'admin', 'MOBILE_TOP', 1),
(11902, 11900, 0, 'Availabitily Aset', '', 'Availabitily Aset', 'Availabitily Aset', 'evaluation/availability', 0, 'admin', 'MOBILE_TOP', 1),
(11903, 11900, 0, 'Kualitas Aset', '', 'Kualitas Aset', 'Kualitas Aset', 'evaluation/current-quality', 0, 'admin', 'MOBILE_TOP', 1),
(11904, 11900, 0, 'Nilai & Umur Ekonomis', '', 'Nilai & Umur Ekonomis', 'Nilai & Umur Ekonomis', 'evaluation/economic-age', 0, 'admin', 'MOBILE_TOP', 1),
(11905, 11900, 0, 'Nilai Manfaat Aset', '', 'Nilai Manfaat Aset', 'Nilai Manfaat Aset', 'evaluation/value', 0, 'admin', 'MOBILE_TOP', 1),
(11906, 11900, 0, 'Reminder Aset Habis Usia', '', 'Reminder Aset Habis Usia', 'Reminder Aset Habis Usia', 'evaluation/reminder-age', 0, 'admin', 'MOBILE_TOP', 1),
(11907, 11900, 0, 'Rekapitulasi Evaluasi Aset', '', 'Rekapitulasi Evaluasi Aset', 'Rekapitulasi Evaluasi Aset', 'evaluation/evaluation-dashboard', 0, 'admin', 'MOBILE_TOP', 1),
(12000, 0, 1, 'Penghapusan Aset', '', 'Penghapusan Aset', 'Penghapusan Aset', '#', 0, 'admin', '', 1),
(12001, 12000, 0, 'Pemusnahan Aset', '', 'Pemusnahan Aset', 'Pemusnahan Aset', 'asset-item-deletion/index', 0, 'admin', 'MOBILE_TOP', 1),
(12002, 12000, 0, 'Pengalihan Aset', '', 'Pengalihan Aset', 'Pengalihan Aset', 'asset-item-deletion/destroy', 0, 'admin', 'MOBILE_TOP', 1),
(12003, 12000, 0, 'Laporan Penghapusan Aset', '', 'Laporan Penghapusan Aset', 'Laporan Penghapusan Aset', 'asset-item-deletion/resume', 0, 'admin', 'MOBILE_TOP', 1),
(12100, 0, 1, 'Pembaruan Aset', '', 'Pembaruan Aset', 'Pembaruan Aset', '#', 0, 'admin', '', 1),
(12101, 12100, 0, 'Kondisi Aset', '', 'Kondisi Aset', 'Kondisi Aset', '', 0, 'admin', 'MOBILE_TOP', 1),
(12102, 12100, 0, 'Pengajuan Pembaruan', '', 'Pengajuan Pembaruan', 'Pengajuan Pembaruan', '', 0, 'admin', 'MOBILE_TOP', 1),
(12103, 12100, 0, 'Laporan Pembaruan', '', 'Laporan Pembaruan', 'Laporan Pembaruan', '', 0, 'admin', 'MOBILE_TOP', 1),
(12200, 0, 1, 'Data Partner', '', 'Data Partner', 'Data Partner', '#', 0, 'admin', '', 1),
(12201, 12200, 0, 'Vendor', '', 'Vendor', 'Vendor', 'vendor/index', 0, 'admin', 'MOBILE_TOP', 1),
(12202, 12200, 0, 'Type Vendor', '', 'Type Vendor', 'Type Vendor', 'type-of-vendor/index', 0, 'admin', 'MOBILE_TOP', 1),
(12300, 0, 1, 'Pengguna & Pengelola Aset', '', 'Pengguna & Pengelola Aset', 'Pengguna & Pengelola Aset', '#', 0, 'admin', '', 1),
(12301, 12300, 0, 'Pegawai ', '', 'Pegawai ', 'Pegawai ', 'hrm-pegawai/index', 0, 'admin', 'MOBILE_TOP', 1),
(12302, 12300, 0, 'Pengelola Aset', '', 'Pengelola Aset', 'Pengelola Aset', 'departement/index', 0, 'admin', 'MOBILE_TOP', 1),
(12400, 0, 1, 'Asset & Akuntansi', '', 'Asset & Akuntansi', 'Asset & Akuntansi', '#', 0, 'admin', '', 1),
(12401, 12400, 0, 'Kode Akun', '', 'Kode Akun', 'Kode Akun', 'account-code/index', 0, 'admin', 'MOBILE_TOP', 1),
(12402, 12400, 0, 'Metode Penyusutan', '', 'Metode Penyusutan', 'Metode Penyusutan', 'mst-accrual/index', 0, 'admin', 'MOBILE_TOP', 1),
(22000, 0, 1, 'Data Master', '', 'Data Master', 'Data Master', '#', 0, 'admin', '', 1),
(22001, 22000, 0, 'Kamus Petujuk', '', 'Kamus Petujuk', 'Kamus Petujuk', 'kamus-petunjuk/index', 0, 'admin', 'MOBILE_TOP', 1),
(22002, 22000, 0, 'Type Asset 1', '', 'Type Asset 1', 'Type Asset 1', 'type-asset1/index', 0, 'admin', 'MOBILE_TOP', 1),
(22003, 22000, 0, 'Type Asset 2', '', 'Type Asset 2', 'Type Asset 2', 'type-asset2/index', 0, 'admin', 'MOBILE_TOP', 1),
(22004, 22000, 0, 'Type Asset 3', '', 'Type Asset 3', 'Type Asset 3', 'type-asset3/index', 0, 'admin', 'MOBILE_TOP', 1),
(22005, 22000, 0, 'Type Asset 4', '', 'Type Asset 4', 'Type Asset 4', 'type-asset4/index', 0, 'admin', 'MOBILE_TOP', 1),
(22006, 22000, 0, 'Type Asset 5', '', 'Type Asset 5', 'Type Asset 5', 'type-asset5/index', 0, 'admin', 'MOBILE_TOP', 1),
(22007, 22000, 0, 'Kode Aset', '', 'Kode Aset', 'Kode Aset', 'asset-master/index', 0, 'admin', 'MOBILE_TOP', 1),
(22008, 22000, 0, 'Type Aset', '', 'Type Aset', 'Type Aset', 'type-asset-item1/index', 0, 'admin', 'MOBILE_TOP', 1),
(22009, 22000, 0, 'Status SIMAK', '', 'Status SIMAK', 'Status SIMAK', 'type-asset-item2/index', 0, 'admin', 'MOBILE_TOP', 1),
(22010, 22000, 0, 'Status Penerimaan Aset', '', 'Status Penerimaan Aset', 'Status Penerimaan Aset', 'mst-status-received/index', 0, 'admin', 'MOBILE_TOP', 1),
(22011, 22000, 0, 'Propinsi', '', 'Propinsi', 'Propinsi', 'propinsi/index', 0, 'admin', 'MOBILE_TOP', 1),
(22012, 22000, 0, 'Kabupaten', '', 'Kabupaten', 'Kabupaten', 'kabupaten/index', 0, 'admin', 'MOBILE_TOP', 1),
(22013, 22000, 0, 'Kecamatan', '', 'Kecamatan', 'Kecamatan', 'kecamatan/index', 0, 'admin', 'MOBILE_TOP', 1),
(22014, 22000, 0, 'Kelurahan', '', 'Kelurahan', 'Kelurahan', 'kelurahan/index', 0, 'admin', 'MOBILE_TOP', 1),
(23000, 0, 1, 'Tester page', '', 'Tester page', 'Tester page', 'tester/index', 0, 'admin', '', 0),
(24000, 0, 1, 'Master Lokasi', '', 'Master Lokasi', 'Master Lokasi', '#', 0, 'admin', '', 1),
(24001, 24000, 0, 'Propinsi', '', 'Propinsi', 'Propinsi', 'propinsi/index', 0, 'admin', 'MOBILE_TOP', 1),
(24002, 24000, 0, 'Kabupaten', '', 'Kabupaten', 'Kabupaten', 'kabupaten/index', 0, 'admin', 'MOBILE_TOP', 1),
(24003, 24000, 0, 'Kecamatan', '', 'Kecamatan', 'Kecamatan', 'kecamatan/index', 0, 'admin', 'MOBILE_TOP', 1),
(24004, 24000, 0, 'Kelurahan', '', 'Kelurahan', 'Kelurahan', 'kelurahan/index', 0, 'admin', 'MOBILE_TOP', 1),
(25000, 0, 1, 'Setting', '', 'Setting', 'Setting', '#', 0, 'admin', '', 0),
(25001, 25000, 0, 'Setting Aplikasi', '', 'Setting Aplikasi', 'Setting Aplikasi', 'app-setting/index', 0, 'admin', 'MOBILE_TOP', 1),
(25002, 25000, 0, 'Konfigurasi Aset Master', '', 'Konfigurasi Aset Master', 'Konfigurasi Aset Master', 'asset-master-field-config/index', 0, 'admin', 'MOBILE_TOP', 0),
(25003, 25000, 0, 'Konfigurasi Aset Item', '', 'Konfigurasi Aset Item', 'Konfigurasi Aset Item', '', 0, 'admin', 'MOBILE_TOP', 0),
(26000, 0, 1, 'Manajemen User', '', 'Manajemen User', 'User Management', '#', 0, 'admin', '', 1),
(26001, 26000, 0, 'User Perusahaan', '', 'User Perusahaan', 'User Perusahaan', 'user-perusahaan/index', 0, 'admin', 'MOBILE_TOP', 0),
(26002, 26000, 0, 'User', '', 'User', 'User', 'user/index', 0, 'admin', 'MOBILE_TOP', 1),
(26003, 26000, 0, 'RBAC', '', 'RBAC', 'RBAC', 'admin/assignment', 0, 'admin', 'MOBILE_TOP', 0),
(27000, 0, 1, 'Management RBAC', '', 'Management RBAC', 'Management RBAC', '', 0, 'admin', '', 0),
(27001, 27000, 0, 'Assignments', '', 'Assignments', 'Assignments', 'admin/assignment', 0, 'admin', 'MOBILE_TOP', 1),
(27002, 27000, 0, 'Roles', '', 'Roles', 'Roles', 'admin/role', 0, 'admin', 'MOBILE_TOP', 0),
(27003, 27000, 0, 'Permissions', '', 'Permissions', 'Permissions', 'admin/permission', 0, 'admin', 'MOBILE_TOP', 0),
(27004, 27000, 0, 'Routes', '', 'Routes', 'Routes', 'admin/route', 0, 'admin', 'MOBILE_TOP', 0),
(27005, 27000, 0, 'Rules', '', 'Rules', 'Rules', 'admin/rule', 0, 'admin', 'MOBILE_TOP', 0),
(1100000, 0, 0, 'Logout ', '', 'Logout ', 'Logout ', 'site/logout', 0, 'admin, member', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `departement`
--

CREATE TABLE `departement` (
  `id_departement` bigint(20) NOT NULL,
  `departement_name` varchar(250) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departement`
--

INSERT INTO `departement` (`id_departement`, `departement_name`, `description`, `is_active`) VALUES
(1, 'Departemen Fasilitas', 'Departemen Penanggung Jawab Fasilitas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hrm_pegawai`
--

CREATE TABLE `hrm_pegawai` (
  `id_pegawai` bigint(20) NOT NULL,
  `id_perusahaan` bigint(20) DEFAULT NULL,
  `userid` varchar(45) NOT NULL,
  `cid` bigint(20) NOT NULL,
  `no_dossier` int(11) DEFAULT NULL,
  `NIP` varchar(100) DEFAULT NULL,
  `nama_lengkap` varchar(250) NOT NULL,
  `foto` varchar(250) DEFAULT NULL,
  `tempat_lahir` varchar(250) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `usia` int(4) DEFAULT 0,
  `usia_lebih_bulan` int(2) DEFAULT NULL,
  `jenis_kelamin` set('PRIA','WANITA') NOT NULL,
  `golongan_darah` set('A','B','AB','O','-') DEFAULT '-',
  `tinggi_badan` int(5) DEFAULT NULL,
  `berat_badan` int(5) DEFAULT NULL,
  `agama` set('ISLAM','KRISTEN','KATOLIK','HINDU','BUDHA','KONGHUCU','LAINNYA','-') DEFAULT '-',
  `status_pernikahan` set('BELUM MENIKAH','MENIKAH','DUDA/JANDA','-') DEFAULT '-',
  `no_identitas_pribadi` varchar(250) DEFAULT NULL,
  `NPWP` varchar(150) DEFAULT NULL,
  `no_kartu_kesehatan` varchar(150) DEFAULT NULL,
  `no_kartu_tenagakerja` varchar(150) DEFAULT NULL,
  `kartu_kesehatan` set('BPJS','ASURANSI') DEFAULT NULL,
  `no_kartu_keluarga` varchar(150) DEFAULT NULL,
  `scan_ktp` varchar(150) DEFAULT NULL,
  `scan_bpjs` varchar(150) DEFAULT NULL,
  `scan_npwp` varchar(150) DEFAULT NULL,
  `scan_paraf` varchar(150) DEFAULT NULL,
  `scan_kk` varchar(250) NOT NULL,
  `scan_tandatangan` varchar(150) DEFAULT NULL,
  `id_hrm_status_pegawai` int(11) NOT NULL DEFAULT 0,
  `id_hrm_status_organik` int(11) NOT NULL DEFAULT 0,
  `status_tenaga_kerja` set('WNI','WNA') NOT NULL DEFAULT 'WNI',
  `reg_tanggal_masuk` date DEFAULT NULL,
  `reg_tanggal_diangkat` date DEFAULT NULL,
  `reg_tanggal_training` date NOT NULL,
  `reg_status_pegawai` set('AKTIF','TIDAK AKTIF','PENSIUN','MPP') NOT NULL DEFAULT 'AKTIF',
  `tanggal_mpp` date DEFAULT NULL,
  `tanggal_pensiun` date DEFAULT NULL,
  `tanggal_terminasi` date NOT NULL,
  `id_hrm_mst_jenis_terminasi_bi` int(11) NOT NULL,
  `gelar_akademik` varchar(250) DEFAULT NULL,
  `gelar_profesi` varchar(250) DEFAULT NULL,
  `pdk_id_tingkatpendidikan` int(11) DEFAULT NULL,
  `pdk_sekolah_terakhir` varchar(250) DEFAULT NULL,
  `pdk_jurusan_terakhir` varchar(250) DEFAULT NULL,
  `pdk_ipk_terakhir` varchar(30) DEFAULT NULL,
  `pdk_tahun_lulus` int(4) DEFAULT NULL,
  `alamat_termutakhir` text DEFAULT NULL,
  `alamat_sesuai_identitas` text DEFAULT NULL,
  `mobilephone1` varchar(250) DEFAULT NULL,
  `mobilephone2` varchar(250) DEFAULT NULL,
  `telepon_rumah` varchar(250) DEFAULT NULL,
  `fax_rumah` varchar(250) DEFAULT NULL,
  `email1` varchar(200) NOT NULL,
  `email2` varchar(200) NOT NULL,
  `jbt_id_jabatan` bigint(20) NOT NULL DEFAULT 0,
  `jbt_jabatan` varchar(250) DEFAULT NULL,
  `jbt_id_tingkat_jabatan` bigint(20) DEFAULT NULL,
  `jbt_no_sk_jabatan` varchar(250) DEFAULT NULL,
  `jbt_tgl_keputusan` date DEFAULT NULL,
  `jbt_tanggal_berlaku` date DEFAULT NULL,
  `jbt_keterangan_mutasi` varchar(250) DEFAULT NULL,
  `pkt_id_pangkat` int(11) DEFAULT NULL,
  `pkt_no_sk` varchar(250) DEFAULT NULL,
  `pkt_tgl_keputusan` date DEFAULT NULL,
  `pkt_tgl_berlaku` date DEFAULT NULL,
  `pkt_gaji_pokok` double(20,2) DEFAULT NULL,
  `pkt_id_jenis_kenaikan_pangkat` int(11) DEFAULT NULL,
  `pkt_eselon` varchar(64) NOT NULL,
  `pkt_ruang` varchar(64) NOT NULL,
  `pos_id_hrm_kantor` bigint(20) NOT NULL DEFAULT 0,
  `pos_id_hrm_unit_kerja` bigint(20) NOT NULL DEFAULT 0,
  `pos_kantor` varchar(250) NOT NULL,
  `sta_total_hukuman_disiplin` int(11) NOT NULL,
  `sta_total_penghargaan` int(11) NOT NULL,
  `pst_masabakti_20` date DEFAULT NULL,
  `pst_masabakti_25` date DEFAULT NULL,
  `pst_masabakti_30` date DEFAULT NULL,
  `pst_masabakti_35` date DEFAULT NULL,
  `pst_masabakti_40` date DEFAULT NULL,
  `cuti_besar_terakhir_start` date NOT NULL,
  `cuti_besar_terakhir_end` date NOT NULL,
  `cuti_besar_terakhir_ke` int(10) NOT NULL,
  `cuti_besar_plan_1` date DEFAULT NULL,
  `cuti_besar_plan_2` date DEFAULT NULL,
  `cuti_besar_plan_3` date DEFAULT NULL,
  `cuti_besar_plan_4` date DEFAULT NULL,
  `cuti_besar_plan_5` date DEFAULT NULL,
  `cuti_besar_plan_6` date DEFAULT NULL,
  `cuti_besar_plan_7` date DEFAULT NULL,
  `cuti_besar_ambil_1` int(1) DEFAULT NULL,
  `cuti_besar_ambil_2` int(1) DEFAULT NULL,
  `cuti_besar_ambil_3` int(1) DEFAULT NULL,
  `cuti_besar_ambil_4` int(1) DEFAULT NULL,
  `cuti_besar_ambil_5` int(1) DEFAULT NULL,
  `cuti_besar_ambil_6` int(1) DEFAULT NULL,
  `cuti_besar_ambil_7` int(1) DEFAULT NULL,
  `cuti_besar_aktual_1` date DEFAULT NULL,
  `cuti_besar_aktual_2` date DEFAULT NULL,
  `cuti_besar_aktual_3` date DEFAULT NULL,
  `cuti_besar_aktual_4` date DEFAULT NULL,
  `cuti_besar_aktual_5` date DEFAULT NULL,
  `cuti_besar_aktual_6` date DEFAULT NULL,
  `cuti_besar_aktual_7` date DEFAULT NULL,
  `cuti_besar_aktual_end_1` date DEFAULT NULL,
  `cuti_besar_aktual_end_2` date DEFAULT NULL,
  `cuti_besar_aktual_end_3` date DEFAULT NULL,
  `cuti_besar_aktual_end_4` date DEFAULT NULL,
  `cuti_besar_aktual_end_5` date DEFAULT NULL,
  `cuti_besar_aktual_end_6` date DEFAULT NULL,
  `cuti_besar_aktual_end_7` date DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_user` varchar(64) NOT NULL,
  `created_ip_address` varchar(64) NOT NULL,
  `modified_date` date NOT NULL,
  `modified_user` varchar(64) NOT NULL,
  `modified_ip_address` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hrm_pegawai`
--

INSERT INTO `hrm_pegawai` (`id_pegawai`, `id_perusahaan`, `userid`, `cid`, `no_dossier`, `NIP`, `nama_lengkap`, `foto`, `tempat_lahir`, `tanggal_lahir`, `usia`, `usia_lebih_bulan`, `jenis_kelamin`, `golongan_darah`, `tinggi_badan`, `berat_badan`, `agama`, `status_pernikahan`, `no_identitas_pribadi`, `NPWP`, `no_kartu_kesehatan`, `no_kartu_tenagakerja`, `kartu_kesehatan`, `no_kartu_keluarga`, `scan_ktp`, `scan_bpjs`, `scan_npwp`, `scan_paraf`, `scan_kk`, `scan_tandatangan`, `id_hrm_status_pegawai`, `id_hrm_status_organik`, `status_tenaga_kerja`, `reg_tanggal_masuk`, `reg_tanggal_diangkat`, `reg_tanggal_training`, `reg_status_pegawai`, `tanggal_mpp`, `tanggal_pensiun`, `tanggal_terminasi`, `id_hrm_mst_jenis_terminasi_bi`, `gelar_akademik`, `gelar_profesi`, `pdk_id_tingkatpendidikan`, `pdk_sekolah_terakhir`, `pdk_jurusan_terakhir`, `pdk_ipk_terakhir`, `pdk_tahun_lulus`, `alamat_termutakhir`, `alamat_sesuai_identitas`, `mobilephone1`, `mobilephone2`, `telepon_rumah`, `fax_rumah`, `email1`, `email2`, `jbt_id_jabatan`, `jbt_jabatan`, `jbt_id_tingkat_jabatan`, `jbt_no_sk_jabatan`, `jbt_tgl_keputusan`, `jbt_tanggal_berlaku`, `jbt_keterangan_mutasi`, `pkt_id_pangkat`, `pkt_no_sk`, `pkt_tgl_keputusan`, `pkt_tgl_berlaku`, `pkt_gaji_pokok`, `pkt_id_jenis_kenaikan_pangkat`, `pkt_eselon`, `pkt_ruang`, `pos_id_hrm_kantor`, `pos_id_hrm_unit_kerja`, `pos_kantor`, `sta_total_hukuman_disiplin`, `sta_total_penghargaan`, `pst_masabakti_20`, `pst_masabakti_25`, `pst_masabakti_30`, `pst_masabakti_35`, `pst_masabakti_40`, `cuti_besar_terakhir_start`, `cuti_besar_terakhir_end`, `cuti_besar_terakhir_ke`, `cuti_besar_plan_1`, `cuti_besar_plan_2`, `cuti_besar_plan_3`, `cuti_besar_plan_4`, `cuti_besar_plan_5`, `cuti_besar_plan_6`, `cuti_besar_plan_7`, `cuti_besar_ambil_1`, `cuti_besar_ambil_2`, `cuti_besar_ambil_3`, `cuti_besar_ambil_4`, `cuti_besar_ambil_5`, `cuti_besar_ambil_6`, `cuti_besar_ambil_7`, `cuti_besar_aktual_1`, `cuti_besar_aktual_2`, `cuti_besar_aktual_3`, `cuti_besar_aktual_4`, `cuti_besar_aktual_5`, `cuti_besar_aktual_6`, `cuti_besar_aktual_7`, `cuti_besar_aktual_end_1`, `cuti_besar_aktual_end_2`, `cuti_besar_aktual_end_3`, `cuti_besar_aktual_end_4`, `cuti_besar_aktual_end_5`, `cuti_besar_aktual_end_6`, `cuti_besar_aktual_end_7`, `created_date`, `created_user`, `created_ip_address`, `modified_date`, `modified_user`, `modified_ip_address`) VALUES
(1, NULL, '', 0, NULL, '2', 'Basuki Sudirman', NULL, '', NULL, 0, NULL, 'WANITA', '-', NULL, NULL, '-', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, 0, 'WNI', NULL, NULL, '0000-00-00', 'AKTIF', NULL, NULL, '0000-00-00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, '2@gmail.com', '', 0, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, 0, '', 0, 0, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '0000-00-00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', '0000-00-00', '', ''),
(2, NULL, '', 0, NULL, '1', 'Ramdan', NULL, 'Bandung', '0000-00-00', 0, NULL, 'WANITA', '-', NULL, NULL, '-', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, 0, 'WNI', NULL, NULL, '0000-00-00', 'AKTIF', NULL, NULL, '0000-00-00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '085724589598', NULL, NULL, NULL, 'muchramdan123@gmail.com', '', 0, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, 0, '', 0, 0, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '0000-00-00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', '0000-00-00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id_kabupaten` int(11) NOT NULL,
  `id_propinsi` int(11) NOT NULL,
  `nama_kabupaten` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`id_kabupaten`, `id_propinsi`, `nama_kabupaten`) VALUES
(1101, 11, 'Kab. Simeulue'),
(1102, 11, 'Kab. Aceh Singkil'),
(1103, 11, 'Kab. Aceh Selatan'),
(1104, 11, 'Kab. Aceh Tenggara'),
(1105, 11, 'Kab. Aceh Timur'),
(1106, 11, 'Kab. Aceh Tengah'),
(1107, 11, 'Kab. Aceh Barat'),
(1108, 11, 'Kab. Aceh Besar'),
(1109, 11, 'Kab. Pidie'),
(1110, 11, 'Kab. Bireuen'),
(1111, 11, 'Kab. Aceh Utara'),
(1112, 11, 'Kab. Aceh Barat Daya'),
(1113, 11, 'Kab. Gayo Lues'),
(1114, 11, 'Kab. Aceh Tamiang'),
(1115, 11, 'Kab. Nagan Raya'),
(1116, 11, 'Kab. Aceh Jaya'),
(1117, 11, 'Kab. Bener Meriah'),
(1118, 11, 'Kab. Pidie Jaya'),
(1171, 11, 'Kota Banda Aceh'),
(1172, 11, 'Kota Sabang'),
(1173, 11, 'Kota Langsa'),
(1174, 11, 'Kota Lhokseumawe'),
(1175, 11, 'Kota Subulussalam'),
(1201, 12, 'Kab. Nias'),
(1202, 12, 'Kab. Mandailing Natal'),
(1203, 12, 'Kab. Tapanuli Selatan'),
(1204, 12, 'Kab. Tapanuli Tengah'),
(1205, 12, 'Kab. Tapanuli Utara'),
(1206, 12, 'Kab. Toba Samosir'),
(1207, 12, 'Kab. Labuhan Batu'),
(1208, 12, 'Kab. Asahan'),
(1209, 12, 'Kab. Simalungun'),
(1210, 12, 'Kab. Dairi'),
(1211, 12, 'Kab. Karo'),
(1212, 12, 'Kab. Deli Serdang'),
(1213, 12, 'Kab. Langkat'),
(1214, 12, 'Kab. Nias Selatan'),
(1215, 12, 'Kab. Humbang Hasundutan'),
(1216, 12, 'Kab. Pakpak Bharat'),
(1217, 12, 'Kab. Samosir'),
(1218, 12, 'Kab. Serdang Bedagai'),
(1219, 12, 'Kab. Batu Bara'),
(1220, 12, 'Kab. Padang Lawas Utara'),
(1221, 12, 'Kab. Padang Lawas'),
(1222, 12, 'Kab. Labuhan Batu Selatan'),
(1223, 12, 'Kab. Labuhan Batu Utara'),
(1224, 12, 'Kab. Nias Utara'),
(1225, 12, 'Kab. Nias Barat'),
(1271, 12, 'Kota Sibolga'),
(1272, 12, 'Kota Tanjung Balai'),
(1273, 12, 'Kota Pematang Siantar'),
(1274, 12, 'Kota Tebing Tinggi'),
(1275, 12, 'Kota Medan'),
(1276, 12, 'Kota Binjai'),
(1277, 12, 'Kota Padangsidimpuan'),
(1278, 12, 'Kota Gunungsitoli'),
(1301, 13, 'Kab. Kepulauan Mentawai'),
(1302, 13, 'Kab. Pesisir Selatan'),
(1303, 13, 'Kab. Solok'),
(1304, 13, 'Kab. Sijunjung'),
(1305, 13, 'Kab. Tanah Datar'),
(1306, 13, 'Kab. Padang Pariaman'),
(1307, 13, 'Kab. Agam'),
(1308, 13, 'Kab. Lima Puluh Kota'),
(1309, 13, 'Kab. Pasaman'),
(1310, 13, 'Kab. Solok Selatan'),
(1311, 13, 'Kab. Dharmasraya'),
(1312, 13, 'Kab. Pasaman Barat'),
(1371, 13, 'Kota Padang'),
(1372, 13, 'Kota Solok'),
(1373, 13, 'Kota Sawah Lunto'),
(1374, 13, 'Kota Padang Panjang'),
(1375, 13, 'Kota Bukittinggi'),
(1376, 13, 'Kota Payakumbuh'),
(1377, 13, 'Kota Pariaman'),
(1401, 14, 'Kab. Kuantan Singingi'),
(1402, 14, 'Kab. Indragiri Hulu'),
(1403, 14, 'Kab. Indragiri Hilir'),
(1404, 14, 'Kab. Pelalawan'),
(1405, 14, 'Kab. S I A K'),
(1406, 14, 'Kab. Kampar'),
(1407, 14, 'Kab. Rokan Hulu'),
(1408, 14, 'Kab. Bengkalis'),
(1409, 14, 'Kab. Rokan Hilir'),
(1410, 14, 'Kab. Kepulauan Meranti'),
(1471, 14, 'Kota Pekanbaru'),
(1473, 14, 'Kota D U M A I'),
(1501, 15, 'Kab. Kerinci'),
(1502, 15, 'Kab. Merangin'),
(1503, 15, 'Kab. Sarolangun'),
(1504, 15, 'Kab. Batang Hari'),
(1505, 15, 'Kab. Muaro Jambi'),
(1506, 15, 'Kab. Tanjung Jabung Timur'),
(1507, 15, 'Kab. Tanjung Jabung Barat'),
(1508, 15, 'Kab. Tebo'),
(1509, 15, 'Kab. Bungo'),
(1571, 15, 'Kota Jambi'),
(1572, 15, 'Kota Sungai Penuh'),
(1601, 16, 'Kab. Ogan Komering Ulu'),
(1602, 16, 'Kab. Ogan Komering Ilir'),
(1603, 16, 'Kab. Muara Enim'),
(1604, 16, 'Kab. Lahat'),
(1605, 16, 'Kab. Musi Rawas'),
(1606, 16, 'Kab. Musi Banyuasin'),
(1607, 16, 'Kab. Banyu Asin'),
(1608, 16, 'Kab. Ogan Komering Ulu Selatan'),
(1609, 16, 'Kab. Ogan Komering Ulu Timur'),
(1610, 16, 'Kab. Ogan Ilir'),
(1611, 16, 'Kab. Empat Lawang'),
(1671, 16, 'Kota Palembang'),
(1672, 16, 'Kota Prabumulih'),
(1673, 16, 'Kota Pagar Alam'),
(1674, 16, 'Kota Lubuklinggau'),
(1701, 17, 'Kab. Bengkulu Selatan'),
(1702, 17, 'Kab. Rejang Lebong'),
(1703, 17, 'Kab. Bengkulu Utara'),
(1704, 17, 'Kab. Kaur'),
(1705, 17, 'Kab. Seluma'),
(1706, 17, 'Kab. Mukomuko'),
(1707, 17, 'Kab. Lebong'),
(1708, 17, 'Kab. Kepahiang'),
(1709, 17, 'Kab. Bengkulu Tengah'),
(1771, 17, 'Kota Bengkulu'),
(1801, 18, 'Kab. Lampung Barat'),
(1802, 18, 'Kab. Tanggamus'),
(1803, 18, 'Kab. Lampung Selatan'),
(1804, 18, 'Kab. Lampung Timur'),
(1805, 18, 'Kab. Lampung Tengah'),
(1806, 18, 'Kab. Lampung Utara'),
(1807, 18, 'Kab. Way Kanan'),
(1808, 18, 'Kab. Tulangbawang'),
(1809, 18, 'Kab. Pesawaran'),
(1810, 18, 'Kab. Pringsewu'),
(1811, 18, 'Kab. Mesuji'),
(1812, 18, 'Kab. Tulang Bawang Barat'),
(1813, 18, 'Kab. Pesisir Barat'),
(1871, 18, 'Kota Bandar Lampung'),
(1872, 18, 'Kota Metro'),
(1901, 19, 'Kab. Bangka'),
(1902, 19, 'Kab. Belitung'),
(1903, 19, 'Kab. Bangka Barat'),
(1904, 19, 'Kab. Bangka Tengah'),
(1905, 19, 'Kab. Bangka Selatan'),
(1906, 19, 'Kab. Belitung Timur'),
(1971, 19, 'Kota Pangkal Pinang'),
(2101, 21, 'Kab. Karimun'),
(2102, 21, 'Kab. Bintan'),
(2103, 21, 'Kab. Natuna'),
(2104, 21, 'Kab. Lingga'),
(2105, 21, 'Kab. Kepulauan Anambas'),
(2171, 21, 'Kota B A T A M'),
(2172, 21, 'Kota Tanjung Pinang'),
(3101, 31, 'Kab. Kepulauan Seribu'),
(3171, 31, 'Kota Jakarta Selatan'),
(3172, 31, 'Kota Jakarta Timur'),
(3173, 31, 'Kota Jakarta Pusat'),
(3174, 31, 'Kota Jakarta Barat'),
(3175, 31, 'Kota Jakarta Utara'),
(3201, 32, 'Kab. Bogor'),
(3202, 32, 'Kab. Sukabumi'),
(3203, 32, 'Kab. Cianjur'),
(3204, 32, 'Kab. Bandung'),
(3205, 32, 'Kab. Garut'),
(3206, 32, 'Kab. Tasikmalaya'),
(3207, 32, 'Kab. Ciamis'),
(3208, 32, 'Kab. Kuningan'),
(3209, 32, 'Kab. Cirebon'),
(3210, 32, 'Kab. Majalengka'),
(3211, 32, 'Kab. Sumedang'),
(3212, 32, 'Kab. Indramayu'),
(3213, 32, 'Kab. Subang'),
(3214, 32, 'Kab. Purwakarta'),
(3215, 32, 'Kab. Karawang'),
(3216, 32, 'Kab. Bekasi'),
(3217, 32, 'Kab. Bandung Barat'),
(3218, 32, 'Kab. Pangandaran'),
(3271, 32, 'Kota Bogor'),
(3272, 32, 'Kota Sukabumi'),
(3273, 32, 'Kota Bandung'),
(3274, 32, 'Kota Cirebon'),
(3275, 32, 'Kota Bekasi'),
(3276, 32, 'Kota Depok'),
(3277, 32, 'Kota Cimahi'),
(3278, 32, 'Kota Tasikmalaya'),
(3279, 32, 'Kota Banjar'),
(3301, 33, 'Kab. Cilacap'),
(3302, 33, 'Kab. Banyumas'),
(3303, 33, 'Kab. Purbalingga'),
(3304, 33, 'Kab. Banjarnegara'),
(3305, 33, 'Kab. Kebumen'),
(3306, 33, 'Kab. Purworejo'),
(3307, 33, 'Kab. Wonosobo'),
(3308, 33, 'Kab. Magelang'),
(3309, 33, 'Kab. Boyolali'),
(3310, 33, 'Kab. Klaten'),
(3311, 33, 'Kab. Sukoharjo'),
(3312, 33, 'Kab. Wonogiri'),
(3313, 33, 'Kab. Karanganyar'),
(3314, 33, 'Kab. Sragen'),
(3315, 33, 'Kab. Grobogan'),
(3316, 33, 'Kab. Blora'),
(3317, 33, 'Kab. Rembang'),
(3318, 33, 'Kab. Pati'),
(3319, 33, 'Kab. Kudus'),
(3320, 33, 'Kab. Jepara'),
(3321, 33, 'Kab. Demak'),
(3322, 33, 'Kab. Semarang'),
(3323, 33, 'Kab. Temanggung'),
(3324, 33, 'Kab. Kendal'),
(3325, 33, 'Kab. Batang'),
(3326, 33, 'Kab. Pekalongan'),
(3327, 33, 'Kab. Pemalang'),
(3328, 33, 'Kab. Tegal'),
(3329, 33, 'Kab. Brebes'),
(3371, 33, 'Kota Magelang'),
(3372, 33, 'Kota Surakarta'),
(3373, 33, 'Kota Salatiga'),
(3374, 33, 'Kota Semarang'),
(3375, 33, 'Kota Pekalongan'),
(3376, 33, 'Kota Tegal'),
(3401, 34, 'Kab. Kulon Progo'),
(3402, 34, 'Kab. Bantul'),
(3403, 34, 'Kab. Gunung Kidul'),
(3404, 34, 'Kab. Sleman'),
(3471, 34, 'Kota Yogyakarta'),
(3501, 35, 'Kab. Pacitan'),
(3502, 35, 'Kab. Ponorogo'),
(3503, 35, 'Kab. Trenggalek'),
(3504, 35, 'Kab. Tulungagung'),
(3505, 35, 'Kab. Blitar'),
(3506, 35, 'Kab. Kediri'),
(3507, 35, 'Kab. Malang'),
(3508, 35, 'Kab. Lumajang'),
(3509, 35, 'Kab. Jember'),
(3510, 35, 'Kab. Banyuwangi'),
(3511, 35, 'Kab. Bondowoso'),
(3512, 35, 'Kab. Situbondo'),
(3513, 35, 'Kab. Probolinggo'),
(3514, 35, 'Kab. Pasuruan'),
(3515, 35, 'Kab. Sidoarjo'),
(3516, 35, 'Kab. Mojokerto'),
(3517, 35, 'Kab. Jombang'),
(3518, 35, 'Kab. Nganjuk'),
(3519, 35, 'Kab. Madiun'),
(3520, 35, 'Kab. Magetan'),
(3521, 35, 'Kab. Ngawi'),
(3522, 35, 'Kab. Bojonegoro'),
(3523, 35, 'Kab. Tuban'),
(3524, 35, 'Kab. Lamongan'),
(3525, 35, 'Kab. Gresik'),
(3526, 35, 'Kab. Bangkalan'),
(3527, 35, 'Kab. Sampang'),
(3528, 35, 'Kab. Pamekasan'),
(3529, 35, 'Kab. Sumenep'),
(3571, 35, 'Kota Kediri'),
(3572, 35, 'Kota Blitar'),
(3573, 35, 'Kota Malang'),
(3574, 35, 'Kota Probolinggo'),
(3575, 35, 'Kota Pasuruan'),
(3576, 35, 'Kota Mojokerto'),
(3577, 35, 'Kota Madiun'),
(3578, 35, 'Kota Surabaya'),
(3579, 35, 'Kota Batu'),
(3601, 36, 'Kab. Pandeglang'),
(3602, 36, 'Kab. Lebak'),
(3603, 36, 'Kab. Tangerang'),
(3604, 36, 'Kab. Serang'),
(3671, 36, 'Kota Tangerang'),
(3672, 36, 'Kota Cilegon'),
(3673, 36, 'Kota Serang'),
(3674, 36, 'Kota Tangerang Selatan'),
(5101, 51, 'Kab. Jembrana'),
(5102, 51, 'Kab. Tabanan'),
(5103, 51, 'Kab. Badung'),
(5104, 51, 'Kab. Gianyar'),
(5105, 51, 'Kab. Klungkung'),
(5106, 51, 'Kab. Bangli'),
(5107, 51, 'Kab. Karang Asem'),
(5108, 51, 'Kab. Buleleng'),
(5171, 51, 'Kota Denpasar'),
(5201, 52, 'Kab. Lombok Barat'),
(5202, 52, 'Kab. Lombok Tengah'),
(5203, 52, 'Kab. Lombok Timur'),
(5204, 52, 'Kab. Sumbawa'),
(5205, 52, 'Kab. Dompu'),
(5206, 52, 'Kab. Bima'),
(5207, 52, 'Kab. Sumbawa Barat'),
(5208, 52, 'Kab. Lombok Utara'),
(5271, 52, 'Kota Mataram'),
(5272, 52, 'Kota Bima'),
(5301, 53, 'Kab. Sumba Barat'),
(5302, 53, 'Kab. Sumba Timur'),
(5303, 53, 'Kab. Kupang'),
(5304, 53, 'Kab. Timor Tengah Selatan'),
(5305, 53, 'Kab. Timor Tengah Utara'),
(5306, 53, 'Kab. Belu'),
(5307, 53, 'Kab. Alor'),
(5308, 53, 'Kab. Lembata'),
(5309, 53, 'Kab. Flores Timur'),
(5310, 53, 'Kab. Sikka'),
(5311, 53, 'Kab. Ende'),
(5312, 53, 'Kab. Ngada'),
(5313, 53, 'Kab. Manggarai'),
(5314, 53, 'Kab. Rote Ndao'),
(5315, 53, 'Kab. Manggarai Barat'),
(5316, 53, 'Kab. Sumba Tengah'),
(5317, 53, 'Kab. Sumba Barat Daya'),
(5318, 53, 'Kab. Nagekeo'),
(5319, 53, 'Kab. Manggarai Timur'),
(5320, 53, 'Kab. Sabu Raijua'),
(5371, 53, 'Kota Kupang'),
(6101, 61, 'Kab. Sambas'),
(6102, 61, 'Kab. Bengkayang'),
(6103, 61, 'Kab. Landak'),
(6104, 61, 'Kab. Pontianak'),
(6105, 61, 'Kab. Sanggau'),
(6106, 61, 'Kab. Ketapang'),
(6107, 61, 'Kab. Sintang'),
(6108, 61, 'Kab. Kapuas Hulu'),
(6109, 61, 'Kab. Sekadau'),
(6110, 61, 'Kab. Melawi'),
(6111, 61, 'Kab. Kayong Utara'),
(6112, 61, 'Kab. Kubu Raya'),
(6171, 61, 'Kota Pontianak'),
(6172, 61, 'Kota Singkawang'),
(6201, 62, 'Kab. Kotawaringin Barat'),
(6202, 62, 'Kab. Kotawaringin Timur'),
(6203, 62, 'Kab. Kapuas'),
(6204, 62, 'Kab. Barito Selatan'),
(6205, 62, 'Kab. Barito Utara'),
(6206, 62, 'Kab. Sukamara'),
(6207, 62, 'Kab. Lamandau'),
(6208, 62, 'Kab. Seruyan'),
(6209, 62, 'Kab. Katingan'),
(6210, 62, 'Kab. Pulang Pisau'),
(6211, 62, 'Kab. Gunung Mas'),
(6212, 62, 'Kab. Barito Timur'),
(6213, 62, 'Kab. Murung Raya'),
(6271, 62, 'Kota Palangka Raya'),
(6301, 63, 'Kab. Tanah Laut'),
(6302, 63, 'Kab. Kota Baru'),
(6303, 63, 'Kab. Banjar'),
(6304, 63, 'Kab. Barito Kuala'),
(6305, 63, 'Kab. Tapin'),
(6306, 63, 'Kab. Hulu Sungai Selatan'),
(6307, 63, 'Kab. Hulu Sungai Tengah'),
(6308, 63, 'Kab. Hulu Sungai Utara'),
(6309, 63, 'Kab. Tabalong'),
(6310, 63, 'Kab. Tanah Bumbu'),
(6311, 63, 'Kab. Balangan'),
(6371, 63, 'Kota Banjarmasin'),
(6372, 63, 'Kota Banjar Baru'),
(6401, 64, 'Kab. Paser'),
(6402, 64, 'Kab. Kutai Barat'),
(6403, 64, 'Kab. Kutai Kartanegara'),
(6404, 64, 'Kab. Kutai Timur'),
(6405, 64, 'Kab. Berau'),
(6409, 64, 'Kab. Penajam Paser Utara'),
(6471, 64, 'Kota Balikpapan'),
(6472, 64, 'Kota Samarinda'),
(6474, 64, 'Kota Bontang'),
(6501, 65, 'Kab. Malinau'),
(6502, 65, 'Kab. Bulungan'),
(6503, 65, 'Kab. Tana Tidung'),
(6504, 65, 'Kab. Nunukan'),
(6571, 65, 'Kota Tarakan'),
(7101, 71, 'Kab. Bolaang Mongondow'),
(7102, 71, 'Kab. Minahasa'),
(7103, 71, 'Kab. Kepulauan Sangihe'),
(7104, 71, 'Kab. Kepulauan Talaud'),
(7105, 71, 'Kab. Minahasa Selatan'),
(7106, 71, 'Kab. Minahasa Utara'),
(7107, 71, 'Kab. Bolaang Mongondow Utara'),
(7108, 71, 'Kab. Siau Tagulandang Biaro'),
(7109, 71, 'Kab. Minahasa Tenggara'),
(7110, 71, 'Kab. Bolaang Mongondow Selatan'),
(7111, 71, 'Kab. Bolaang Mongondow Timur'),
(7171, 71, 'Kota Manado'),
(7172, 71, 'Kota Bitung'),
(7173, 71, 'Kota Tomohon'),
(7174, 71, 'Kota Kotamobagu'),
(7201, 72, 'Kab. Banggai Kepulauan'),
(7202, 72, 'Kab. Banggai'),
(7203, 72, 'Kab. Morowali'),
(7204, 72, 'Kab. Poso'),
(7205, 72, 'Kab. Donggala'),
(7206, 72, 'Kab. Toli-toli'),
(7207, 72, 'Kab. Buol'),
(7208, 72, 'Kab. Parigi Moutong'),
(7209, 72, 'Kab. Tojo Una-una'),
(7210, 72, 'Kab. Sigi'),
(7271, 72, 'Kota Palu'),
(7301, 73, 'Kab. Kepulauan Selayar'),
(7302, 73, 'Kab. Bulukumba'),
(7303, 73, 'Kab. Bantaeng'),
(7304, 73, 'Kab. Jeneponto'),
(7305, 73, 'Kab. Takalar'),
(7306, 73, 'Kab. Gowa'),
(7307, 73, 'Kab. Sinjai'),
(7308, 73, 'Kab. Maros'),
(7309, 73, 'Kab. Pangkajene Dan Kepulauan'),
(7310, 73, 'Kab. Barru'),
(7311, 73, 'Kab. Bone'),
(7312, 73, 'Kab. Soppeng'),
(7313, 73, 'Kab. Wajo'),
(7314, 73, 'Kab. Sidenreng Rappang'),
(7315, 73, 'Kab. Pinrang'),
(7316, 73, 'Kab. Enrekang'),
(7317, 73, 'Kab. Luwu'),
(7318, 73, 'Kab. Tana Toraja'),
(7322, 73, 'Kab. Luwu Utara'),
(7325, 73, 'Kab. Luwu Timur'),
(7326, 73, 'Kab. Toraja Utara'),
(7371, 73, 'Kota Makassar'),
(7372, 73, 'Kota Parepare'),
(7373, 73, 'Kota Palopo'),
(7401, 74, 'Kab. Buton'),
(7402, 74, 'Kab. Muna'),
(7403, 74, 'Kab. Konawe'),
(7404, 74, 'Kab. Kolaka'),
(7405, 74, 'Kab. Konawe Selatan'),
(7406, 74, 'Kab. Bombana'),
(7407, 74, 'Kab. Wakatobi'),
(7408, 74, 'Kab. Kolaka Utara'),
(7409, 74, 'Kab. Buton Utara'),
(7410, 74, 'Kab. Konawe Utara'),
(7471, 74, 'Kota Kendari'),
(7472, 74, 'Kota Baubau'),
(7501, 75, 'Kab. Boalemo'),
(7502, 75, 'Kab. Gorontalo'),
(7503, 75, 'Kab. Pohuwato'),
(7504, 75, 'Kab. Bone Bolango'),
(7505, 75, 'Kab. Gorontalo Utara'),
(7571, 75, 'Kota Gorontalo'),
(7601, 76, 'Kab. Majene'),
(7602, 76, 'Kab. Polewali Mandar'),
(7603, 76, 'Kab. Mamasa'),
(7604, 76, 'Kab. Mamuju'),
(7605, 76, 'Kab. Mamuju Utara'),
(8101, 81, 'Kab. Maluku Tenggara Barat'),
(8102, 81, 'Kab. Maluku Tenggara'),
(8103, 81, 'Kab. Maluku Tengah'),
(8104, 81, 'Kab. Buru'),
(8105, 81, 'Kab. Kepulauan Aru'),
(8106, 81, 'Kab. Seram Bagian Barat'),
(8107, 81, 'Kab. Seram Bagian Timur'),
(8108, 81, 'Kab. Maluku Barat Daya'),
(8109, 81, 'Kab. Buru Selatan'),
(8171, 81, 'Kota Ambon'),
(8172, 81, 'Kota Tual'),
(8201, 82, 'Kab. Halmahera Barat'),
(8202, 82, 'Kab. Halmahera Tengah'),
(8203, 82, 'Kab. Kepulauan Sula'),
(8204, 82, 'Kab. Halmahera Selatan'),
(8205, 82, 'Kab. Halmahera Utara'),
(8206, 82, 'Kab. Halmahera Timur'),
(8207, 82, 'Kab. Pulau Morotai'),
(8271, 82, 'Kota Ternate'),
(8272, 82, 'Kota Tidore Kepulauan'),
(9101, 91, 'Kab. Fakfak'),
(9102, 91, 'Kab. Kaimana'),
(9103, 91, 'Kab. Teluk Wondama'),
(9104, 91, 'Kab. Teluk Bintuni'),
(9105, 91, 'Kab. Manokwari'),
(9106, 91, 'Kab. Sorong Selatan'),
(9107, 91, 'Kab. Sorong'),
(9108, 91, 'Kab. Raja Ampat'),
(9109, 91, 'Kab. Tambrauw'),
(9110, 91, 'Kab. Maybrat'),
(9171, 91, 'Kota Sorong'),
(9401, 94, 'Kab. Merauke'),
(9402, 94, 'Kab. Jayawijaya'),
(9403, 94, 'Kab. Jayapura'),
(9404, 94, 'Kab. Nabire'),
(9408, 94, 'Kab. Kepulauan Yapen'),
(9409, 94, 'Kab. Biak Numfor'),
(9410, 94, 'Kab. Paniai'),
(9411, 94, 'Kab. Puncak Jaya'),
(9412, 94, 'Kab. Mimika'),
(9413, 94, 'Kab. Boven Digoel'),
(9414, 94, 'Kab. Mappi'),
(9415, 94, 'Kab. Asmat'),
(9416, 94, 'Kab. Yahukimo'),
(9417, 94, 'Kab. Pegunungan Bintang'),
(9418, 94, 'Kab. Tolikara'),
(9419, 94, 'Kab. Sarmi'),
(9420, 94, 'Kab. Keerom'),
(9426, 94, 'Kab. Waropen'),
(9427, 94, 'Kab. Supiori'),
(9428, 94, 'Kab. Mamberamo Raya'),
(9429, 94, 'Kab. Nduga'),
(9430, 94, 'Kab. Lanny Jaya'),
(9431, 94, 'Kab. Mamberamo Tengah'),
(9432, 94, 'Kab. Yalimo'),
(9433, 94, 'Kab. Puncak'),
(9434, 94, 'Kab. Dogiyai'),
(9435, 94, 'Kab. Intan Jaya'),
(9436, 94, 'Kab. Deiyai'),
(9471, 94, 'Kota Jayapura');

-- --------------------------------------------------------

--
-- Table structure for table `kamus_petunjuk`
--

CREATE TABLE `kamus_petunjuk` (
  `id_kamus_petunjuk` bigint(20) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `is_visible` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamus_petunjuk`
--

INSERT INTO `kamus_petunjuk` (`id_kamus_petunjuk`, `nama`, `deskripsi`, `is_visible`) VALUES
(100, 'Petunjuk Form Visitor', 'Form-form ini dapat digunakan oleh anda pengunjung website kami untuk berbagai keperluan.Silakan dipilih beberapa form yang sesuai dengan keperluan anda.', 1),
(101, 'Grievance Submission - Info', 'Form ini bisa anda gunakan untuk mengajukan \"grievance\" / keluhan secara spesifik kepada perusahaan kami.', 1),
(102, 'Grievance List Request - Info', 'Form ini digunakan untuk keperluan mengajukan list/daftar grievance yang pernah dimiliki. Anda silakan mengisi form pengajuannya, kemudian akan kami proses jika syarat dan ketentuan terpenuhi.', 1),
(103, 'Supplier List Request - Info', 'Form ini digunakan untuk melakukan pengajuan daftar/list supplier yang kami miliki. Anda silakan mengisi form pengajuannya. Jika menurut kami memenuhi syarat dan ketentukan maka daftar list supplier tersebut akan kami kirimkan.', 1),
(111, 'Grievance List Request - Petunjuk', 'Form ini merupakan form yang digunakan untuk mengajukan permintaan \"Grievance List Request\". \r\nAnda silakan mengisi terlebih dahulu data diri pribadi atau instansi yang anda wakili, serta tujuan dari permintaan ini.', 1),
(112, 'Grievance Submission - Petunjuk', 'Form ini merupakan form yang digunakan untuk mengajukan permintaan \"Grievance Submission\". Anda silakan mengisi terlebih dahulu data diri pribadi atau instansi yang anda wakili, serta tujuan dari permintaan ini.', 1),
(113, 'Supplier List Request  - Petunjuk', 'Form ini merupakan form yang digunakan untuk mengajukan permintaan \"Supplier List Request\". Anda silakan mengisi terlebih dahulu data diri pribadi atau instansi yang anda wakili, serta tujuan dari permintaan ini', 1),
(501, 'Disclaimer Assesment', 'Saya bersedia mengisi data-data terkait ini dengan sejujur-jujurnya dan penuh tanggung jawab. ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `nama_kecamatan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `id_kabupaten`, `nama_kecamatan`) VALUES
(1, 1101, 'Simelue A'),
(2, 1102, 'Aceh Singkil A');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kelurahan` bigint(20) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `nama_kelurahan` varchar(250) NOT NULL,
  `kodepos` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id_kelurahan`, `id_kecamatan`, `nama_kelurahan`, `kodepos`) VALUES
(1, 1, 'Kelurahan Simelua A', 12),
(2, 2, 'Kelurahan Aceh Singkli', 1),
(3, 1, 'ssa asarrr', 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `id_log_activity` bigint(20) NOT NULL,
  `log_date` date NOT NULL,
  `log_datetime` datetime NOT NULL,
  `tablename` varchar(200) NOT NULL,
  `related_id` bigint(20) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `ip_address_user` varchar(100) NOT NULL,
  `additional_info1` text DEFAULT NULL,
  `additional_info2` text DEFAULT NULL,
  `additional_info3` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_application`
--

CREATE TABLE `mobile_application` (
  `id` bigint(20) NOT NULL,
  `application_id` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `version` varchar(10) NOT NULL,
  `realese_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `mobile_application`
--

INSERT INTO `mobile_application` (`id`, `application_id`, `description`, `version`, `realese_date`) VALUES
(1, 'template_flutter_1_0', 'Aplikasi Template Flutter Versi 1.0', '1.0', '2019-06-20');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_session`
--

CREATE TABLE `mobile_session` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `device_mobile_id` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `application_id` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `valid_date_time` datetime NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `mobile_session`
--

INSERT INTO `mobile_session` (`id`, `id_user`, `auth_key`, `device_mobile_id`, `application_id`, `valid_date_time`, `status`) VALUES
(81, 6, 'OMg1EbgAKTCRA9ce3PjUOk9gEglPmub1', 'sdfWtYdhdh', 'template_flutter_1_0', '2019-09-23 16:08:15', 0),
(82, 6, 'vK_MIUwzLcVafoXtcm92w8JNPeSidGgs', '1606-MMB29M', 'template_flutter_1_0', '2019-09-28 05:07:30', 1),
(83, 107, 'qdRM-V96cvghr26I-cjMDS8qhSIny9Ml', '1606-MMB29M', 'template_flutter_1_0', '2019-10-12 06:57:47', 1),
(84, 1, 'phMa4-GH2ecpNZ6XH1evsmsNEFKpoSdd', '1606-MMB29M', 'template_flutter_1_0', '2020-03-15 16:14:58', 0),
(85, 1, 'bg_8XYwkN3FP5gh8KJZXffd0L0Wzf2rn', '1606-MMB29M', 'template_flutter_1_0', '2020-03-15 16:25:51', 0),
(86, 1, 'rXkzu5hAvnM-qjf5T9V14FXaDW33TLx4', '1606-MMB29M', 'template_flutter_1_0', '2020-03-15 16:31:57', 0),
(87, 1, 'Y03uav4O9jD2XDd39vAOFOm2k80WXbIZ', '1606-MMB29M', 'template_flutter_1_0', '2020-03-15 16:53:10', 0),
(88, 1, 'pt6ECMDGjA7pJJIML_AYDa5irwLwE2no', '1606-MMB29M', 'template_flutter_1_0', '2020-03-15 16:55:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_accrual`
--

CREATE TABLE `mst_accrual` (
  `id_mst_accrual` int(11) NOT NULL,
  `method` varchar(250) NOT NULL,
  `notes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_accrual`
--

INSERT INTO `mst_accrual` (`id_mst_accrual`, `method`, `notes`) VALUES
(1, 'Metode garis lurus (straight line method)', '(Harga Perolehan – Nilai Sisa) : Umur Ekonomis'),
(2, 'Metode Saldo Menurun (Declining Balanced Method)', '=DB(cost;salvage;life;periode;[month])'),
(3, 'Metode Penyusutan Jumlah Angka Tahun', NULL),
(4, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_log_activity`
--

CREATE TABLE `mst_log_activity` (
  `id_mst_log_activity` int(11) NOT NULL,
  `activity` varchar(200) NOT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_log_activity`
--

INSERT INTO `mst_log_activity` (`id_mst_log_activity`, `activity`, `notes`) VALUES
(1, 'CREATE', NULL),
(2, 'READ', NULL),
(3, 'UPDATE', NULL),
(4, 'DELETE', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_status_condition`
--

CREATE TABLE `mst_status_condition` (
  `id_mst_status_condition` int(11) NOT NULL,
  `condition` varchar(200) NOT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_status_condition`
--

INSERT INTO `mst_status_condition` (`id_mst_status_condition`, `condition`, `notes`) VALUES
(10, 'BAIK', NULL),
(20, 'BERFUNGSI BAIK TETAPI ADA KEKURANGAN SEDIKIT', NULL),
(30, 'TIDAK BERFUNGSI DENGAN BAIK', NULL),
(40, 'RUSAK', NULL),
(50, 'RUSAK PARAH', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_status_received`
--

CREATE TABLE `mst_status_received` (
  `id_status_received` int(11) NOT NULL,
  `status_received` varchar(200) NOT NULL,
  `is_active` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_status_received`
--

INSERT INTO `mst_status_received` (`id_status_received`, `status_received`, `is_active`) VALUES
(1, 'B', 1),
(2, 'RR', 1),
(3, 'RB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `security_code` bigint(20) NOT NULL,
  `qrcode_perusahaan` varchar(200) NOT NULL,
  `nama_perusahaan` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `email1` varchar(200) NOT NULL,
  `email2` varchar(200) NOT NULL,
  `phone1` int(11) NOT NULL,
  `phone2` int(11) NOT NULL,
  `media_sosial1` varchar(200) NOT NULL,
  `media_sosial2` varchar(200) NOT NULL,
  `media_sosial3` varchar(200) NOT NULL,
  `npwp` varchar(50) NOT NULL,
  `id_type_packet` int(11) NOT NULL,
  `packet_expired_date` date NOT NULL,
  `last_payment_date` date NOT NULL,
  `last_amount_payment` bigint(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `security_code`, `qrcode_perusahaan`, `nama_perusahaan`, `alamat`, `email1`, `email2`, `phone1`, `phone2`, `media_sosial1`, `media_sosial2`, `media_sosial3`, `npwp`, `id_type_packet`, `packet_expired_date`, `last_payment_date`, `last_amount_payment`, `status`) VALUES
(1, 0, '', 'Company 1', '', '', '', 0, 0, '', '', '', '', 0, '0000-00-00', '0000-00-00', 0, 0),
(15, 0, '', 'Company 2', '', '', '', 0, 0, '', '', '', '', 0, '0000-00-00', '0000-00-00', 0, 0),
(16, 0, '', 'Andara Shop', '', '', '', 981289123, 0, '', '', '', '', 0, '0000-00-00', '0000-00-00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_marketplace`
--

CREATE TABLE `product_marketplace` (
  `id_product_marketplace` bigint(20) NOT NULL,
  `product_name` varchar(250) NOT NULL,
  `id_marketplace` bigint(20) NOT NULL,
  `product_id` varchar(50) NOT NULL,
  `stock` int(11) NOT NULL,
  `weight` double NOT NULL,
  `price` bigint(20) NOT NULL,
  `discount_price` double(8,2) NOT NULL,
  `condition` enum('new','ex') NOT NULL,
  `description` text NOT NULL,
  `insurance` enum('yes','no') NOT NULL,
  `brand` varchar(250) NOT NULL,
  `opt_optional_price` text NOT NULL,
  `imei` varchar(50) DEFAULT NULL,
  `cid` varchar(30) DEFAULT NULL,
  `barcode1` varchar(50) DEFAULT NULL,
  `last_update` datetime NOT NULL,
  `last_user_update` int(11) NOT NULL,
  `last_update_ip_address` varchar(200) NOT NULL,
  `token` bigint(20) DEFAULT NULL,
  `flag_new_changes` int(1) DEFAULT 0,
  `flag_ack_devices` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_marketplace`
--

INSERT INTO `product_marketplace` (`id_product_marketplace`, `product_name`, `id_marketplace`, `product_id`, `stock`, `weight`, `price`, `discount_price`, `condition`, `description`, `insurance`, `brand`, `opt_optional_price`, `imei`, `cid`, `barcode1`, `last_update`, `last_user_update`, `last_update_ip_address`, `token`, `flag_new_changes`, `flag_ack_devices`) VALUES
(1, 'Suki Sabu-Sabu', 1, '319722896', 0, 0, 15000, 0.00, 'new', '', 'yes', '', '', '987654321', '123456789', '8996001302323', '0000-00-00 00:00:00', 0, '', NULL, 0, 1),
(2, 'Sabu-Sabu 2', 1, '321368436', 0, 0, 17500, 0.00, 'new', '', 'yes', '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', 0, '', NULL, 0, 1),
(3, 'MURAHHH!!!! EasyTouch Blood Hemoglobin HB Isi 25 Strip Easy Touch', 2, '321370932', 0, 0, 114499, 0.00, 'new', '', 'yes', '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', 0, '', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--

CREATE TABLE `propinsi` (
  `id_propinsi` int(11) NOT NULL,
  `nama_propinsi` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`id_propinsi`, `nama_propinsi`) VALUES
(11, 'Aceh'),
(12, 'Sumatera Utara'),
(13, 'Sumatera Barat'),
(14, 'Riau'),
(15, 'Jambi'),
(16, 'Sumatera Selatan'),
(17, 'Bengkulu'),
(18, 'Lampung'),
(19, 'Kepulauan Bangka Belitung'),
(21, 'Kepulauan Riau'),
(31, 'DKI Jakarta'),
(32, 'Jawa Barat'),
(33, 'Jawa Tengah'),
(34, 'DI Yogyakarta'),
(35, 'Jawa Timur'),
(36, 'Banten'),
(51, 'Bali'),
(52, 'Nusa Tenggara Barat'),
(53, 'Nusa Tenggara Timur'),
(61, 'Kalimantan Barat'),
(62, 'Kalimantan Tengah'),
(63, 'Kalimantan Selatan'),
(64, 'Kalimantan Timur'),
(65, 'Kalimantan Utara'),
(71, 'Sulawesi Utara'),
(72, 'Sulawesi Tengah'),
(73, 'Sulawesi Selatan'),
(74, 'Sulawesi Tenggara'),
(75, 'Gorontalo'),
(76, 'Sulawesi Barat'),
(81, 'Maluku'),
(82, 'Maluku Utara'),
(91, 'Papua Barat'),
(94, 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id_role_menu` int(11) NOT NULL,
  `menu` varchar(200) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id_role_menu`, `menu`, `is_active`) VALUES
(101, 'Aset', 1),
(102, 'Kode Barang', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_user_access`
--

CREATE TABLE `role_user_access` (
  `id_role_user_access` bigint(20) NOT NULL,
  `id_role_menu` int(11) NOT NULL,
  `role_name` varchar(64) NOT NULL,
  `user_read` int(1) NOT NULL DEFAULT 0,
  `user_write` int(1) NOT NULL DEFAULT 0,
  `user_delete` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_asset1`
--

CREATE TABLE `type_asset1` (
  `id_type_asset` int(11) NOT NULL,
  `type_asset` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_asset1`
--

INSERT INTO `type_asset1` (`id_type_asset`, `type_asset`, `description`, `is_active`) VALUES
(1, 'Tanah', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_asset2`
--

CREATE TABLE `type_asset2` (
  `id_type_asset` int(11) NOT NULL,
  `type_asset` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_asset3`
--

CREATE TABLE `type_asset3` (
  `id_type_asset` int(11) NOT NULL,
  `type_asset` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_asset4`
--

CREATE TABLE `type_asset4` (
  `id_type_asset` int(11) NOT NULL,
  `type_asset` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_asset5`
--

CREATE TABLE `type_asset5` (
  `id_type_asset` int(11) NOT NULL,
  `type_asset` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_asset_item1`
--

CREATE TABLE `type_asset_item1` (
  `id_type_asset_item` int(11) NOT NULL,
  `type_asset_item` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_asset_item1`
--

INSERT INTO `type_asset_item1` (`id_type_asset_item`, `type_asset_item`, `description`, `is_active`) VALUES
(10, 'Brantas', NULL, 1),
(20, 'Bengawan SOlo', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_asset_item2`
--

CREATE TABLE `type_asset_item2` (
  `id_type_asset_item` int(11) NOT NULL,
  `type_asset_item` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_asset_item2`
--

INSERT INTO `type_asset_item2` (`id_type_asset_item`, `type_asset_item`, `description`, `is_active`) VALUES
(100, 'Terpelihara', NULL, 1),
(200, 'Tidak Terpelihara', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_asset_item3`
--

CREATE TABLE `type_asset_item3` (
  `id_type_asset_item` int(11) NOT NULL,
  `type_asset_item` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_asset_item3`
--

INSERT INTO `type_asset_item3` (`id_type_asset_item`, `type_asset_item`, `description`, `is_active`) VALUES
(1000, 'MANGGA', NULL, 1),
(1001, 'PETE', NULL, 1),
(1002, 'NANGKA', NULL, 1),
(1003, 'MAHONI', NULL, 1),
(1004, 'RENGAS', NULL, 1),
(1005, 'ARENG', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_asset_item4`
--

CREATE TABLE `type_asset_item4` (
  `id_type_asset_item` int(11) NOT NULL,
  `type_asset_item` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_asset_item4`
--

INSERT INTO `type_asset_item4` (`id_type_asset_item`, `type_asset_item`, `description`, `is_active`) VALUES
(201, 'BESAR', NULL, 1),
(202, 'SEDANG', NULL, 1),
(203, 'KECIL', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_asset_item5`
--

CREATE TABLE `type_asset_item5` (
  `id_type_asset_item` int(11) NOT NULL,
  `type_asset_item` varchar(250) NOT NULL,
  `description` text DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_of_vendor`
--

CREATE TABLE `type_of_vendor` (
  `id_type_of_vendor` int(11) NOT NULL,
  `type_of_vendor` varchar(100) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `max_score` double(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_of_vendor`
--

INSERT INTO `type_of_vendor` (`id_type_of_vendor`, `type_of_vendor`, `description`, `max_score`) VALUES
(1, 'CPKO', NULL, 22.00),
(2, 'Manpower', NULL, 10.00),
(3, 'Chemical & Packaging', NULL, 30.00),
(4, 'CNO', NULL, 22.00),
(1, 'CPKO', NULL, 22.00),
(2, 'Manpower', NULL, 10.00),
(3, 'Chemical & Packaging', NULL, 30.00),
(4, 'CNO', NULL, 22.00),
(1, 'CPKO', NULL, 22.00),
(2, 'Manpower', NULL, 10.00),
(3, 'Chemical & Packaging', NULL, 30.00),
(4, 'CNO', NULL, 22.00);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `full_name` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(250) NOT NULL,
  `status` smallint(6) NOT NULL,
  `password_reset_token` varchar(250) NOT NULL,
  `user_level` enum('admin','member','management (SA)','management (ST)','MEMBER','ADM') NOT NULL DEFAULT 'ADM',
  `role` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `full_name`, `username`, `email`, `password_hash`, `auth_key`, `status`, `password_reset_token`, `user_level`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin@admin.com', '$2y$13$wUP89zDmoJhxVQ55PqilV.K/5e3.K2RSRuhHShtr5zVJzSXZtBFJS', 'pt6ECMDGjA7pJJIML_AYDa5irwLwE2no', 10, 'asdas', 'admin', 20, 1530780329, 1581674106),
(36, 'admin4', 'admin45', 'admin@gmail.com', '$2y$13$wUP89zDmoJhxVQ55PqilV.K/5e3.K2RSRuhHShtr5zVJzSXZtBFJS', 'u1XDKCQQm7FlAdN5y4bUGZZaxXTk1laX', 10, 'yUM8pRVQCiqpDMhLHNi_LzxLtSZN4_jO_1565786618', 'admin', 10, 1565786618, 1565787037),
(37, 'admin5 kelima', 'admin5', 'admin@gmail.com', '$2y$13$D.b3Blb001f2uuDt767LZeIOaTHniVmLMseitoJlXQZli38vBTylq', 'PpmWggpvXU8pyf_rw0A4khueBDLBb2FO', 10, 'jFsygbE0MmjufTXvz6dOtRiM_4Kjppv5_1565787233', 'admin', 10, 1565787232, 1565790121);

-- --------------------------------------------------------

--
-- Table structure for table `user_perusahaan`
--

CREATE TABLE `user_perusahaan` (
  `id_user_perusahaan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_perusahaan`
--

INSERT INTO `user_perusahaan` (`id_user_perusahaan`, `id_user`, `id_perusahaan`, `created_date`, `created_user`, `created_ip_address`) VALUES
(1, 5, 1, '0000-00-00 00:00:00', 0, ''),
(2, 8, 15, '0000-00-00 00:00:00', 0, ''),
(3, 1, 2, '0000-00-00 00:00:00', 0, ''),
(4, 10, 1, '2019-02-25 06:14:48', 0, '::1'),
(5, 12, 99, '2019-03-08 04:20:19', 0, '::1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_code`
--
ALTER TABLE `account_code`
  ADD PRIMARY KEY (`id_account_code`),
  ADD KEY `id_account_code_parent` (`id_account_code_parent`);

--
-- Indexes for table `app_field_config`
--
ALTER TABLE `app_field_config`
  ADD PRIMARY KEY (`id_app_field_config`),
  ADD KEY `classname` (`classname`,`fieldname`),
  ADD KEY `varian_group` (`varian_group`);

--
-- Indexes for table `app_setting`
--
ALTER TABLE `app_setting`
  ADD PRIMARY KEY (`id_app_setting`);

--
-- Indexes for table `app_vocabulary`
--
ALTER TABLE `app_vocabulary`
  ADD PRIMARY KEY (`id_app_vocabulary`),
  ADD KEY `master_vocab` (`master_vocab`);

--
-- Indexes for table `asset_code`
--
ALTER TABLE `asset_code`
  ADD PRIMARY KEY (`id_asset_code`);

--
-- Indexes for table `asset_item`
--
ALTER TABLE `asset_item`
  ADD PRIMARY KEY (`id_asset_item`),
  ADD KEY `id_asset_master` (`id_asset_master`),
  ADD KEY `id_asset_received` (`id_asset_received`),
  ADD KEY `id_asset_item_location` (`id_asset_item_location`),
  ADD KEY `id_asset_item_parent` (`id_asset_item_parent`);

--
-- Indexes for table `asset_item_condition_log`
--
ALTER TABLE `asset_item_condition_log`
  ADD PRIMARY KEY (`id_asset_item_condition_log`),
  ADD KEY `id_asset_item` (`id_asset_item`,`condition_log_date`),
  ADD KEY `id_mst_status_condition` (`id_mst_status_condition`);

--
-- Indexes for table `asset_item_deletion`
--
ALTER TABLE `asset_item_deletion`
  ADD PRIMARY KEY (`id_asset_item_deletion`);

--
-- Indexes for table `asset_item_distribution_current`
--
ALTER TABLE `asset_item_distribution_current`
  ADD PRIMARY KEY (`id_asset_item_distribution_current`),
  ADD KEY `id_asset_item` (`id_asset_item`,`distribute_to`,`id_pegawai`,`id_departement`,`id_asset_item_location`,`status`,`start_date`,`start_month`,`start_year`);

--
-- Indexes for table `asset_item_distribution_log`
--
ALTER TABLE `asset_item_distribution_log`
  ADD PRIMARY KEY (`id_asset_item_distribution_log`),
  ADD KEY `id_asset_item` (`id_asset_item`,`distribute_to`,`id_pegawai`,`id_departement`,`id_asset_item_location`,`status`,`start_date`,`start_month`,`start_year`);

--
-- Indexes for table `asset_item_incident`
--
ALTER TABLE `asset_item_incident`
  ADD PRIMARY KEY (`id_asset_item_incident`),
  ADD KEY `id_asset_item` (`id_asset_item`,`incident_date`);

--
-- Indexes for table `asset_item_location`
--
ALTER TABLE `asset_item_location`
  ADD PRIMARY KEY (`id_asset_item_location`),
  ADD KEY `id_asset_master` (`id_asset_master`),
  ADD KEY `id_kabupaten` (`id_kabupaten`,`id_propinsi`,`id_kecamatan`,`id_kelurahan`);

--
-- Indexes for table `asset_item_maintenance`
--
ALTER TABLE `asset_item_maintenance`
  ADD PRIMARY KEY (`id_asset_item_maintenance`),
  ADD KEY `id_asset_item` (`id_asset_item`,`id_asset_master_criteria_maintenance`),
  ADD KEY `id_vendor` (`id_vendor`);

--
-- Indexes for table `asset_item_repair`
--
ALTER TABLE `asset_item_repair`
  ADD PRIMARY KEY (`id_asset_item_repair`),
  ADD KEY `id_asset_item` (`id_asset_item`,`id_asset_item_incident`),
  ADD KEY `id_vendor` (`id_vendor`);

--
-- Indexes for table `asset_item_tracking_device`
--
ALTER TABLE `asset_item_tracking_device`
  ADD PRIMARY KEY (`id_asset_item_tracking_device`),
  ADD KEY `id_asset_item` (`id_asset_item`,`id_device`) USING BTREE;

--
-- Indexes for table `asset_item_tracking_device_log`
--
ALTER TABLE `asset_item_tracking_device_log`
  ADD PRIMARY KEY (`id_asset_item_tracking_device_log`),
  ADD KEY `id_asset_item` (`id_asset_item`,`id_device`);

--
-- Indexes for table `asset_item_tracking_log`
--
ALTER TABLE `asset_item_tracking_log`
  ADD PRIMARY KEY (`id_asset_item_tracking_log`);

--
-- Indexes for table `asset_master`
--
ALTER TABLE `asset_master`
  ADD PRIMARY KEY (`id_asset_master`);

--
-- Indexes for table `asset_master_criteria_maintenance`
--
ALTER TABLE `asset_master_criteria_maintenance`
  ADD PRIMARY KEY (`id_asset_master_criteria_maintenance`);

--
-- Indexes for table `asset_master_field_config`
--
ALTER TABLE `asset_master_field_config`
  ADD PRIMARY KEY (`id_asset_master_field_config`);

--
-- Indexes for table `asset_master_location`
--
ALTER TABLE `asset_master_location`
  ADD PRIMARY KEY (`id_asset_master_location`),
  ADD KEY `id_asset_master` (`id_asset_master`);

--
-- Indexes for table `asset_master_map_year`
--
ALTER TABLE `asset_master_map_year`
  ADD PRIMARY KEY (`id_asset_master_map_year`),
  ADD KEY `id_asset_master` (`id_asset_master`,`year`);

--
-- Indexes for table `asset_master_request`
--
ALTER TABLE `asset_master_request`
  ADD PRIMARY KEY (`id_asset_master_request`),
  ADD KEY `id_asset_master` (`id_asset_master`,`request_date`);

--
-- Indexes for table `asset_master_structure`
--
ALTER TABLE `asset_master_structure`
  ADD PRIMARY KEY (`id_asset_master_structure`),
  ADD KEY `id_asset_master_parent` (`id_asset_master_parent`,`id_asset_master_child`);

--
-- Indexes for table `asset_received`
--
ALTER TABLE `asset_received`
  ADD PRIMARY KEY (`id_asset_received`),
  ADD KEY `id_asset_master` (`id_asset_master`);

--
-- Indexes for table `asset_received_to_item`
--
ALTER TABLE `asset_received_to_item`
  ADD PRIMARY KEY (`id_asset_received_to_item`),
  ADD KEY `id_asset_received` (`id_asset_received`),
  ADD KEY `id_asset_item` (`id_asset_item`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `cpanel_leftmenu`
--
ALTER TABLE `cpanel_leftmenu`
  ADD PRIMARY KEY (`id_leftmenu`);

--
-- Indexes for table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id_departement`);

--
-- Indexes for table `hrm_pegawai`
--
ALTER TABLE `hrm_pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `jenis_kelamin` (`jenis_kelamin`),
  ADD KEY `NIP` (`NIP`),
  ADD KEY `agama` (`agama`),
  ADD KEY `status_pernikahan` (`status_pernikahan`),
  ADD KEY `golongan_darah` (`golongan_darah`),
  ADD KEY `cid` (`cid`),
  ADD KEY `id_hrm_status_pegawai` (`id_hrm_status_pegawai`),
  ADD KEY `id_hrm_status_organik` (`id_hrm_status_organik`),
  ADD KEY `pdk_id_tingkatpendidikan` (`pdk_id_tingkatpendidikan`),
  ADD KEY `reg_status_pegawai` (`reg_status_pegawai`),
  ADD KEY `id_perusahaan` (`id_perusahaan`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id_kabupaten`),
  ADD KEY `id_propinsi` (`id_propinsi`);

--
-- Indexes for table `kamus_petunjuk`
--
ALTER TABLE `kamus_petunjuk`
  ADD PRIMARY KEY (`id_kamus_petunjuk`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`),
  ADD KEY `id_kabupaten` (`id_kabupaten`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`),
  ADD KEY `id_kecamatan` (`id_kecamatan`),
  ADD KEY `kodepos` (`kodepos`);

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id_log_activity`),
  ADD KEY `log_date` (`log_date`,`tablename`,`related_id`,`id_activity`,`userid`);

--
-- Indexes for table `mobile_application`
--
ALTER TABLE `mobile_application`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mobile_session`
--
ALTER TABLE `mobile_session`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mst_accrual`
--
ALTER TABLE `mst_accrual`
  ADD PRIMARY KEY (`id_mst_accrual`);

--
-- Indexes for table `mst_log_activity`
--
ALTER TABLE `mst_log_activity`
  ADD PRIMARY KEY (`id_mst_log_activity`);

--
-- Indexes for table `mst_status_condition`
--
ALTER TABLE `mst_status_condition`
  ADD PRIMARY KEY (`id_mst_status_condition`);

--
-- Indexes for table `mst_status_received`
--
ALTER TABLE `mst_status_received`
  ADD PRIMARY KEY (`id_status_received`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `product_marketplace`
--
ALTER TABLE `product_marketplace`
  ADD PRIMARY KEY (`id_product_marketplace`),
  ADD KEY `id_marketplace` (`id_marketplace`);

--
-- Indexes for table `propinsi`
--
ALTER TABLE `propinsi`
  ADD PRIMARY KEY (`id_propinsi`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id_role_menu`);

--
-- Indexes for table `type_asset1`
--
ALTER TABLE `type_asset1`
  ADD PRIMARY KEY (`id_type_asset`);

--
-- Indexes for table `type_asset2`
--
ALTER TABLE `type_asset2`
  ADD PRIMARY KEY (`id_type_asset`);

--
-- Indexes for table `type_asset3`
--
ALTER TABLE `type_asset3`
  ADD PRIMARY KEY (`id_type_asset`);

--
-- Indexes for table `type_asset4`
--
ALTER TABLE `type_asset4`
  ADD PRIMARY KEY (`id_type_asset`);

--
-- Indexes for table `type_asset5`
--
ALTER TABLE `type_asset5`
  ADD PRIMARY KEY (`id_type_asset`);

--
-- Indexes for table `type_asset_item1`
--
ALTER TABLE `type_asset_item1`
  ADD PRIMARY KEY (`id_type_asset_item`);

--
-- Indexes for table `type_asset_item2`
--
ALTER TABLE `type_asset_item2`
  ADD PRIMARY KEY (`id_type_asset_item`);

--
-- Indexes for table `type_asset_item3`
--
ALTER TABLE `type_asset_item3`
  ADD PRIMARY KEY (`id_type_asset_item`);

--
-- Indexes for table `type_asset_item4`
--
ALTER TABLE `type_asset_item4`
  ADD PRIMARY KEY (`id_type_asset_item`);

--
-- Indexes for table `type_asset_item5`
--
ALTER TABLE `type_asset_item5`
  ADD PRIMARY KEY (`id_type_asset_item`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `email` (`email`),
  ADD KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `user_perusahaan`
--
ALTER TABLE `user_perusahaan`
  ADD PRIMARY KEY (`id_user_perusahaan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_code`
--
ALTER TABLE `account_code`
  MODIFY `id_account_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `app_field_config`
--
ALTER TABLE `app_field_config`
  MODIFY `id_app_field_config` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- AUTO_INCREMENT for table `app_setting`
--
ALTER TABLE `app_setting`
  MODIFY `id_app_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `app_vocabulary`
--
ALTER TABLE `app_vocabulary`
  MODIFY `id_app_vocabulary` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `asset_code`
--
ALTER TABLE `asset_code`
  MODIFY `id_asset_code` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_item`
--
ALTER TABLE `asset_item`
  MODIFY `id_asset_item` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `asset_item_condition_log`
--
ALTER TABLE `asset_item_condition_log`
  MODIFY `id_asset_item_condition_log` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_item_deletion`
--
ALTER TABLE `asset_item_deletion`
  MODIFY `id_asset_item_deletion` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asset_item_distribution_current`
--
ALTER TABLE `asset_item_distribution_current`
  MODIFY `id_asset_item_distribution_current` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `asset_item_distribution_log`
--
ALTER TABLE `asset_item_distribution_log`
  MODIFY `id_asset_item_distribution_log` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asset_item_incident`
--
ALTER TABLE `asset_item_incident`
  MODIFY `id_asset_item_incident` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `asset_item_location`
--
ALTER TABLE `asset_item_location`
  MODIFY `id_asset_item_location` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `asset_item_maintenance`
--
ALTER TABLE `asset_item_maintenance`
  MODIFY `id_asset_item_maintenance` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asset_item_repair`
--
ALTER TABLE `asset_item_repair`
  MODIFY `id_asset_item_repair` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `asset_item_tracking_device`
--
ALTER TABLE `asset_item_tracking_device`
  MODIFY `id_asset_item_tracking_device` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_item_tracking_device_log`
--
ALTER TABLE `asset_item_tracking_device_log`
  MODIFY `id_asset_item_tracking_device_log` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_item_tracking_log`
--
ALTER TABLE `asset_item_tracking_log`
  MODIFY `id_asset_item_tracking_log` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_master`
--
ALTER TABLE `asset_master`
  MODIFY `id_asset_master` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `asset_master_criteria_maintenance`
--
ALTER TABLE `asset_master_criteria_maintenance`
  MODIFY `id_asset_master_criteria_maintenance` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `asset_master_field_config`
--
ALTER TABLE `asset_master_field_config`
  MODIFY `id_asset_master_field_config` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asset_master_location`
--
ALTER TABLE `asset_master_location`
  MODIFY `id_asset_master_location` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_master_map_year`
--
ALTER TABLE `asset_master_map_year`
  MODIFY `id_asset_master_map_year` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_master_request`
--
ALTER TABLE `asset_master_request`
  MODIFY `id_asset_master_request` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `asset_master_structure`
--
ALTER TABLE `asset_master_structure`
  MODIFY `id_asset_master_structure` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `asset_received`
--
ALTER TABLE `asset_received`
  MODIFY `id_asset_received` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `asset_received_to_item`
--
ALTER TABLE `asset_received_to_item`
  MODIFY `id_asset_received_to_item` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `departement`
--
ALTER TABLE `departement`
  MODIFY `id_departement` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hrm_pegawai`
--
ALTER TABLE `hrm_pegawai`
  MODIFY `id_pegawai` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9472;

--
-- AUTO_INCREMENT for table `kamus_petunjuk`
--
ALTER TABLE `kamus_petunjuk`
  MODIFY `id_kamus_petunjuk` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id_kelurahan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id_log_activity` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mobile_application`
--
ALTER TABLE `mobile_application`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mobile_session`
--
ALTER TABLE `mobile_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `mst_accrual`
--
ALTER TABLE `mst_accrual`
  MODIFY `id_mst_accrual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mst_log_activity`
--
ALTER TABLE `mst_log_activity`
  MODIFY `id_mst_log_activity` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mst_status_condition`
--
ALTER TABLE `mst_status_condition`
  MODIFY `id_mst_status_condition` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `mst_status_received`
--
ALTER TABLE `mst_status_received`
  MODIFY `id_status_received` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_marketplace`
--
ALTER TABLE `product_marketplace`
  MODIFY `id_product_marketplace` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `propinsi`
--
ALTER TABLE `propinsi`
  MODIFY `id_propinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id_role_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `type_asset1`
--
ALTER TABLE `type_asset1`
  MODIFY `id_type_asset` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `type_asset2`
--
ALTER TABLE `type_asset2`
  MODIFY `id_type_asset` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_asset3`
--
ALTER TABLE `type_asset3`
  MODIFY `id_type_asset` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_asset4`
--
ALTER TABLE `type_asset4`
  MODIFY `id_type_asset` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_asset5`
--
ALTER TABLE `type_asset5`
  MODIFY `id_type_asset` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_asset_item1`
--
ALTER TABLE `type_asset_item1`
  MODIFY `id_type_asset_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `type_asset_item2`
--
ALTER TABLE `type_asset_item2`
  MODIFY `id_type_asset_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `type_asset_item3`
--
ALTER TABLE `type_asset_item3`
  MODIFY `id_type_asset_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1006;

--
-- AUTO_INCREMENT for table `type_asset_item4`
--
ALTER TABLE `type_asset_item4`
  MODIFY `id_type_asset_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT for table `type_asset_item5`
--
ALTER TABLE `type_asset_item5`
  MODIFY `id_type_asset_item` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user_perusahaan`
--
ALTER TABLE `user_perusahaan`
  MODIFY `id_user_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
