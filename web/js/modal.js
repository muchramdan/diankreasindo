$(function () {
     $('.modalButton').on('click', function () {
         $('#modal').modal({backdrop: true})
             .find('#modalContent')
             .load($(this).attr('value'));
     });

});
