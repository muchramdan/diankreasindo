<?php

namespace app\common\uicomponent;

use Yii;

class AlertUI
{
	public static function alertDangerMode1($alert){
		return '
			<div class="alert alert-danger alert-dismissible">
                
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                '.$alert.'
              </div>
		';
	}
}

?>
