<?php

return [
    'class' => 'yii\db\Connection',
    //'dsn' => 'mysql:host=192.168.1.12;dbname=assetmgt',
	'dsn' => 'mysql:host=localhost:3308;dbname=assetmgt_full',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
