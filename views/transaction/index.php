<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\TransactionTypes;
use app\models\TransactionDetails;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = $this->title;

// ambil list jenis transaksi untuk filtering
$typeList = ArrayHelper::map(TransactionTypes::find()->asArray()->all(), 'id', 'name');

?>
<div class="transactions-index box box-primary">
<div class="box-header with-border">
        <p>
            <?= Html::a('Tambah Item', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'trans_code',
            // 'trans_date',
            // [   'attribute' => 'type_id', 
            //     'filter' => $typeList, 
            //     'label' => 'Transaction Type', 
            //     // 'value' => function ($model, $index, $widget) { return $model->type->name; }
            // ],
            // 'remarks',
            'customer',
            'alamat',
            'no_hp',
            'jenis_produk',
            'pengerjaan',
            'color',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
        </div>
</div>
