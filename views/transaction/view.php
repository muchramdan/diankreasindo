<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Transactions */

$this->title = $model->trans_code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transactions-view box box-primary">
<div class="box-header with-border">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Back to List'), ['index'], ['class' => 'btn btn-warning']) ?>
    </p>
    <div class="col-sm-6 col-md-6 col-lg-6" >
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'customer',
            'alamat',
            'no_hp',
        ],
    ]) ?>
</div>
<div class="col-sm-6 col-md-6 col-lg-6" >
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jenis_produk',
            'pengerjaan',
            'color',
        ],
    ]) ?>
</div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        ],
    ]) ?>
</div>
</div>
<p>
<div class="transactions-view box box-primary">
    <div class="item panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title pull-left"><i class="glyphicon glyphicon-barcode"></i> Detail Bahan Baku</h3>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $details,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // [
                    //     'attribute' => 'item_id',
                    //     'value' => 'item.code',
                    //     'header' => 'Item Code',
                    // ],
                    // [
                    //     'attribute' => 'item_id',
                    //     'value' => 'item.name',
                    //     'header' => 'Item Name',
                    // ],
                    // 'quantity',
                    // 'remarks',
                    'nama_bahan',
                    'jumlah',
                    'harga',
                    'total',
                ],
            ]); ?>
        </div>
    </div>

</div>
