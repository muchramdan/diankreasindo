<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Omset */

$this->title = $model->no_op;
$this->params['breadcrumbs'][] = ['label' => 'Omsets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="omset-view box box-success">

<div class="box-header with-border">
        <p>
            <?= Html::a('<< Back', ['index'], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id_omset], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id_omset], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jenis_pengerjaan',
            // 'id_omset',
            'tanggal',
            'no_op',
            'no_kw',
            'customer',
            'pembelian',
            'saldo_awal',
            'dp_cash',
            'dp_bank',
            'pelunasan',
            'pelunasan_bank',
            'bg_mundur',
            'potongan',
            'saldo_akhir',
            'marketing'
        ],
    ]) ?>
</div>
