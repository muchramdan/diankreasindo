<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OmsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Omset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="omset-index box box-success">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('Tambah Omset', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="glyphicon glyphicon-print"></i> Cetak Laporan', ['/omset/report'], [
                        'class'=>'btn btn-danger', 
                        'target'=>'_blank', 
                        'data-toggle'=>'tooltip', 
                        'title'=>'Will open the generated PDF file in a new window'
            ]); ?>
        </p>

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'showPageSummary' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'jenis_pengerjaan',
                'headerOptions' => ['style' => 'width:150px'],
            ],
            [
                'attribute' => 'tanggal',
                'headerOptions' => ['style' => 'width:150px'],
            ],
            // 'id_omset',
            'no_op',
            'no_kw',
            [
                'attribute' => 'customer',
                'headerOptions' => ['style' => 'width:150px'],
            ],
            [
                'attribute'=> 'pembelian',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            [
                'attribute'=> 'saldo_awal',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            [
                'attribute'=> 'dp_cash',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            [
                'attribute'=> 'dp_bank',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            [
                'attribute'=> 'pelunasan',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            [
                'attribute'=> 'pelunasan_bank',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            'bg_mundur',
            'potongan',
            [
                'attribute'=> 'saldo_akhir',
                'format' => ['decimal'],
                'pageSummary' => true
            ],
            'marketing',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>

    
</div>
