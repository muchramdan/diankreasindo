<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Omset */

$this->title = 'Update Omset: ' . $model->id_omset;
$this->params['breadcrumbs'][] = ['label' => 'Omsets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_omset, 'url' => ['view', 'id' => $model->id_omset]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="omset-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
