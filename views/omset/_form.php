<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Omset */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
//CSS Ini digunakan untuk menampilkan required field (field wajib isi)
?>
<style>
    div.required label.control-label:after {
        content: ' *';
        color: red;
    }
</style>

<?php
//CSS Ini digunakan untuk overide jarak antar form biar tidak terlalu jauh
?>
<style>
    .form-group {
        margin-bottom: 0px;
    }
</style>

<div class="omset-form box box-success">

<div class="box-header with-border">
        <p>
            <?= Html::a('<< Kembali', ['index'], ['class' => 'btn btn-warning']) ?>
        </p>
    </div>
    <div class="box-header with-border">
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            //'action' => ['index1'],
            //'method' => 'get',
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-2',
                    'offset' => 'col-sm-offset-2',
                    'wrapper' => 'col-sm-8',
                ],
            ],
        ]); ?>

    <!-- <?php $form->field($model, 'id_omset')->textInput() ?> -->
    <?php $dataPengerjaan = ['INTERIOR (PENDING)' => 'INTERIOR (PENDING)',
                             'INTERIOR UMUM' => 'INTERIOR UMUM',
                             'PERPINTUAN PROYEK (PENDING)' => 'PERPINTUAN PROYEK (PENDING)',
                             'PERPINTUAN UMUM' => 'PERPINTUAN UMUM',
                             'PERATAPAN + TERALIS UMUM'=>'PERATAPAN + TERALIS UMUM'];
        echo $form->field($model, 'jenis_pengerjaan')->dropDownList($dataPengerjaan,
            ['prompt' => '-Pilih Jenis Pengerjaan-']); ?>
      
    <?= $form->field($model, 'tanggal',['inputOptions' => [
'autocomplete' => 'off']])->widget(
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        'id' => 'assetitemincident-incident_date',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yy',
            'defaultDate' => date('dd/mm/Y'),

        ]
    ]) ?>

    <?= $form->field($model, 'no_op',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_kw',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pembelian',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'saldo_awal',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dp_cash',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dp_bank',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pelunasan',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pelunasan_bank',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bg_mundur',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'potongan',['inputOptions' => [
'autocomplete' => 'off']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'saldo_akhir')->textInput(['maxlength' => true,'readonly'=> true]) ?>

    <?= $form->field($model, 'marketing',
     ['inputOptions' => ['value' => Yii::$app->user->identity->username]])
     ->textInput(['maxlength' => true,'readonly'=> true]) ?>

   
    <div class="box-footer">
            <div class='form-group'>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#omset-dp_bank, #omset-saldo_awal, #omset-dp_cash, #omset-pelunasan, #omset-pelunasan_bank, #omset-potongan").keyup(function() {
            var saldo  = $("#omset-saldo_awal").val();
            var dpbank = $("#omset-dp_bank").val();
            var dpcash = $("#omset-dp_cash").val();
            var pelunasan = $("#omset-pelunasan").val();
            var pelunasanbank = $("#omset-pelunasan_bank").val();
            var potongan = $("#omset-potongan").val();
            var total = saldo - dpcash - dpbank - pelunasan - pelunasanbank - potongan;
            $("#omset-saldo_akhir").val(total);
        });
    });
</script>