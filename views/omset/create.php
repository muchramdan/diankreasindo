<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Omset */

$this->title = 'Tambah Omset';
$this->params['breadcrumbs'][] = ['label' => 'Omsets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="omset-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
