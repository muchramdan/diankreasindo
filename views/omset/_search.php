<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OmsetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="omset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_omset') ?>

    <?= $form->field($model, 'jenis_pengerjaan') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'no_op') ?>

    <?= $form->field($model, 'no_kw') ?>

    <?php // echo $form->field($model, 'customer') ?>

    <?php // echo $form->field($model, 'pembelian') ?>

    <?php // echo $form->field($model, 'saldo_awal') ?>

    <?php // echo $form->field($model, 'dp_cash') ?>

    <?php // echo $form->field($model, 'dp_bank') ?>

    <?php // echo $form->field($model, 'pelunasan') ?>

    <?php // echo $form->field($model, 'pelunasan_bank') ?>

    <?php // echo $form->field($model, 'bg_mundur') ?>

    <?php // echo $form->field($model, 'potongan') ?>

    <?php // echo $form->field($model, 'saldo_akhir') ?>

    <?php // echo $form->field($model, 'marketing') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
