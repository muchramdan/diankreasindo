<?php
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPurchase */
/* @var $form yii\widgets\ActiveForm */
?>

<br>
<?= $form->field($model, 'op_data_ukur_t')->textInput(['maxlength' => true])->label('Ukuran T') ?>

<?= $form->field($model, 'op_data_ukur_l')->textInput(['maxlength' => true])->label('Ukuran L') ?>

<?= $form->field($model, 'op_data_ukur_set')->textInput(['maxlength' => true])->label('Ukuran Set') ?>

<?= $form->field($model, 'op_data_ukur_arah')->textInput(['maxlength' => true])->label('Ukuran Arah') ?>

<?= $form->field($model, 'op_data_gambar')->fileInput(['maxlength' => true]) ?>

<?= $form->field($model, 'op_cp_produk_harga') ?>

<?= $form->field($model, 'op_cp_produk_jumlah')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'op_cp_warna_set')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'op_cp_warna_luas')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'op_cp_warna_harga')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'op_cp_warna_jumlah')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'op_cp_dudukan_set')->textInput(['maxlength' => true]) ?>


<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>