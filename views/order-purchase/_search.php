<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPurchaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-purchase-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_order_purchase') ?>

    <?= $form->field($model, 'op_no_order') ?>

    <?= $form->field($model, 'op_customer_nama') ?>

    <?= $form->field($model, 'op_customer_alamat') ?>

    <?= $form->field($model, 'op_customer_lokasi_ukur') ?>

    <?php // echo $form->field($model, 'op_customer_telp') ?>

    <?php // echo $form->field($model, 'op_tanggal_op') ?>

    <?php // echo $form->field($model, 'op_customer_daerah') ?>

    <?php // echo $form->field($model, 'op_customer_hp') ?>

    <?php // echo $form->field($model, 'op_customer_janji_pasang') ?>

    <?php // echo $form->field($model, 'op_data_ukur_t') ?>

    <?php // echo $form->field($model, 'op_data_ukur_l') ?>

    <?php // echo $form->field($model, 'op_data_ukur_set') ?>

    <?php // echo $form->field($model, 'op_data_ukur_arah') ?>

    <?php // echo $form->field($model, 'op_data_gambar') ?>

    <?php // echo $form->field($model, 'op_cp_produk_set') ?>

    <?php // echo $form->field($model, 'op_cp_produk_luas') ?>

    <?php // echo $form->field($model, 'op_cp_produk_harga') ?>

    <?php // echo $form->field($model, 'op_cp_produk_jumlah') ?>

    <?php // echo $form->field($model, 'op_cp_warna_set') ?>

    <?php // echo $form->field($model, 'op_cp_warna_luas') ?>

    <?php // echo $form->field($model, 'op_cp_warna_harga') ?>

    <?php // echo $form->field($model, 'op_cp_warna_jumlah') ?>

    <?php // echo $form->field($model, 'op_cp_dudukan_set') ?>

    <?php // echo $form->field($model, 'op_cp_dudukan_luas') ?>

    <?php // echo $form->field($model, 'op_cp_dudukan_harga') ?>

    <?php // echo $form->field($model, 'op_cp_dudukan_jumlah') ?>

    <?php // echo $form->field($model, 'op_jumlah') ?>

    <?php // echo $form->field($model, 'op_uang_muka') ?>

    <?php // echo $form->field($model, 'op_sisa') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
