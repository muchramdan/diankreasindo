<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPurchase */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="order-purchase-create box box-primary">
<div class="box-header with-border">
<?php $form = ActiveForm::begin(['id' => 'transactions-form']); ?>
    <div class="col-sm-4 col-md-3">
    <?= $form->field($model, 'op_no_order')->textInput(['maxlength' => true, 'value' => (($model->isNewRecord) ? $model->generateKode_Urut() : $model->no)])->label('No Order') ?>           
    </div>
    <div class="col-sm-4 col-md-3">
    <?= $form->field($model, 'op_tanggal_op')->textInput(['maxlength' => true])->widget( 
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        'id' => 'op-tanggal-op',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yy',
            'defaultDate' => date('dd/mm/Y'),

        ]
    ]) ?>
    </div>
    <div class="col-sm-4 col-md-6">
    <?= $form->field($model, 'op_customer_nama')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-6">
    <?= $form->field($model, 'op_customer_alamat')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="col-sm-6 col-md-6">
    <?= $form->field($model, 'op_customer_lokasi_ukur')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-6">
    <?= $form->field($model, 'op_customer_telp')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-6">
    </div>
    <div class="col-sm-6 col-md-6">
    <?= $form->field($model, 'op_customer_daerah')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-6">
    <?= $form->field($model, 'op_customer_hp')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-6">
    <?= $form->field($model, 'op_customer_janji_pasang')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-6">
    <?php $form->field($model, 'op_data_ukur_t')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    </div>
    <div class="order-purchase-create box box-primary">
   <div class="box-header with-border">
   <div class="col-sm-4 col-md-3">
    <?= $form->field($model, 'op_data_ukur_l')->textInput(['maxlength' => true])->label('Ukuran L') ?>
</div>
<div class="col-sm-4 col-md-3">
<?= $form->field($model, 'op_data_ukur_set')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4 col-md-3">
<?= $form->field($model, 'op_data_ukur_arah')->textInput(['maxlength' => true]) ?>
</div>
<?php $form->field($model, 'op_data_gambar')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_produk_set')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_produk_luas')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_produk_harga')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_produk_jumlah')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_warna_set')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_warna_luas')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_warna_harga')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_warna_jumlah')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_dudukan_set')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_dudukan_luas')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_dudukan_harga')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_cp_dudukan_jumlah')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_jumlah')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_uang_muka')->textInput(['maxlength' => true]) ?>

<?php $form->field($model, 'op_sisa')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>