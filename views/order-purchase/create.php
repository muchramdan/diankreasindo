<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPurchase */

$this->title = 'Create Order Purchase';
$this->params['breadcrumbs'][] = ['label' => 'Order Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-purchase-create">
<div class="box-header with-border">
<?php if (isset($error)): ?>
    	<div class="alert alert-danger" role="alert"><?= Html::encode($error) ?></div>
    <?php endif; ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
