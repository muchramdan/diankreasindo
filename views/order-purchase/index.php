<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderPurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Purchases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-purchase-index box box-primary">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('Tambah OP', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_order_purchase',
            'op_no_order',
            'op_customer_nama',
            'op_customer_alamat',
            'op_customer_lokasi_ukur',
            // 'op_customer_telp',
            'op_tanggal_op',
            'op_customer_daerah',
            'op_customer_hp',
            'op_customer_janji_pasang',
            // 'op_data_ukur_t',
            // 'op_data_ukur_l',
            // 'op_data_ukur_set',
            // 'op_data_ukur_arah',
            // 'op_data_gambar',
            // 'op_cp_produk_set',
            // 'op_cp_produk_luas',
            // 'op_cp_produk_harga',
            // 'op_cp_produk_jumlah',
            // 'op_cp_warna_set',
            // 'op_cp_warna_luas',
            // 'op_cp_warna_harga',
            // 'op_cp_warna_jumlah',
            // 'op_cp_dudukan_set',
            // 'op_cp_dudukan_luas',
            // 'op_cp_dudukan_harga',
            // 'op_cp_dudukan_jumlah',
            // 'op_jumlah',
            // 'op_uang_muka',
            // 'op_sisa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>

    
</div>
