<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPurchase */

$this->title = $model->id_order_purchase;
$this->params['breadcrumbs'][] = ['label' => 'Order Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-purchase-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_order_purchase], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_order_purchase], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_order_purchase',
            'op_no_order',
            'op_customer_nama',
            'op_customer_alamat',
            'op_customer_lokasi_ukur',
            'op_customer_telp',
            'op_tanggal_op',
            'op_customer_daerah',
            'op_customer_hp',
            'op_customer_janji_pasang',
            'op_data_ukur_t',
            'op_data_ukur_l',
            'op_data_ukur_set',
            'op_data_ukur_arah',
            'op_data_gambar',
            'op_cp_produk_set',
            'op_cp_produk_luas',
            'op_cp_produk_harga',
            'op_cp_produk_jumlah',
            'op_cp_warna_set',
            'op_cp_warna_luas',
            'op_cp_warna_harga',
            'op_cp_warna_jumlah',
            'op_cp_dudukan_set',
            'op_cp_dudukan_luas',
            'op_cp_dudukan_harga',
            'op_cp_dudukan_jumlah',
            'op_jumlah',
            'op_uang_muka',
            'op_sisa',
        ],
    ]) ?>

</div>
