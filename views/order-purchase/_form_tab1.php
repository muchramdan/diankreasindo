
   <br> <?= $form->field($model, 'op_no_order')->textInput(['maxlength' => true, 'value' => (($model->isNewRecord) ? $model->generateKode_Urut() : $model->generateKode_Urut()->no)])->label('No Order') ?>           
   
    <?= $form->field($model, 'op_tanggal_op')->textInput(['maxlength' => true])->widget( 
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        'id' => 'op-tanggal-op',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yy',
            'defaultDate' => date('dd/mm/Y'),

        ]
    ]) ?>
   
    <?= $form->field($model, 'op_customer_nama')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'op_customer_alamat')->textInput(['maxlength' => true]) ?>

   
    <?= $form->field($model, 'op_customer_lokasi_ukur')->textInput(['maxlength' => true]) ?>
   
    <?= $form->field($model, 'op_customer_telp')->textInput(['maxlength' => true]) ?>
   
    <?= $form->field($model, 'op_customer_daerah')->textInput(['maxlength' => true]) ?>
   
    <?= $form->field($model, 'op_customer_hp')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'op_customer_janji_pasang')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'op_data_ukur_t')->textInput(['maxlength' => true])->label('Ukuran T') ?>

    <?= $form->field($model, 'op_data_ukur_l')->textInput(['maxlength' => true])->label('Ukuran L') ?>

    <?= $form->field($model, 'op_data_ukur_set')->textInput(['maxlength' => true])->label('Ukuran Set') ?>

    <?= $form->field($model, 'op_data_ukur_arah')->textInput(['maxlength' => true])->label('Ukuran Arah') ?>

    <?= $form->field($model, 'op_data_gambar')->fileInput(['maxlength' => true]) ?>