<?php

/* @var $this yii\web\View */
/* @var $model app\models\Owner */

use app\models\AppVocabularySearch;

$baseName = AppVocabularySearch::getValueByKey('Owner');
$this->title = $baseName;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="owner-create ">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
