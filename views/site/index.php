<?php
use yii\helpers\Html;
use app\models\AppSettingSearch;

/* @var $this yii\web\View */

$this->title = Html::encode(Yii::$app->params['appName']);
$this->title = AppSettingSearch::getValueByKey("APP-NAME-SINGKAT", Yii::$app->params['appName']);
$secondTitle = AppSettingSearch::getValueByKey("APP-NAME", Yii::$app->params['appName']);
?>
<style>
    .img-responsive {
        position: relative;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
    }
</style>
<div class="site-index">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Data Pemasukan
            </h3>
        </div>
        <div class="box-body">
                     <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3> </h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> Pemasukan Hari</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-arrow-graph-up-right"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-blue-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3></h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> Pemasukan Bulan Ini</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-arrow-graph-up-right"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3></h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> Pemasukan Tahun Ini</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-arrow-graph-up-right"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-black-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3></h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> Seluruh Pemasukan</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-arrow-graph-up-right"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        </div>
                        </div>
        <div class="box">
            <div class="box-header with-border">
            <h3 class="box-title">Data Pengeluaran
            </h3>
        </div>
        <div class="box-body">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3> </h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> pengeluaran Hari</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-stats-bars"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3></h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> pengeluaran Bulan Ini</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-stats-bars"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3></h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> pengeluaran Tahun Ini</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-stats-bars"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-black-gradient">
                                <div class="inner">
                                    <!--                    <h3>0</h3>-->
                                    <h3></h3>
                                    <p> Rp. 0 ,-</p>
                                    <p> Seluruh pengeluaran</p>
                                </div>
                                <div class="icon">
                                    <i class="ionicons ion-stats-bars"></i>
                                </div>
                                <a href="" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
			<!-- <?php
			//Get Background Image
			//Ambil Default jika tidak ada
			$imgpath = AppSettingSearch::getValueImageByKey("MAIN-BACKGROUND","images/home.jpg","");
			?>
            <img class="img-responsive" src="<?php echo Yii::$app->request->baseUrl . '/'.$imgpath; ?>"/> -->

        </div>
    </div>

</div>
