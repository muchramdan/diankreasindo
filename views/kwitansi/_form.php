<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kwitansi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kwitansi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_order_purchase')->textInput() ?>
    <?= $form->field($model, 'no_kw')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
